import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import NavBar from './features/navBar/components/navBar';
import Home from './features/home/components/home';
import About from './features/about/components/about';
import Estimation from './features/estimation/containers/estimation';
import { Container, Row, Col } from 'reactstrap';

import './App.css';

const hide = "d-none";

const style = {
  backgroundColor: '#000000'
};

class App extends Component {
  render() {
    return (
      <Container fluid>
        <NavBar />
        <Row className="justify-content-center">
          <Col sm={{ size: 'auto' }} className={(this.props.router.location.pathname === '/') ? hide : undefined} style={style}>
          </Col>
          <Col>
            <div>
              <Route exact path="/" component={Home} />
              <Route exact path="/about-us" component={About} />
              <Route exact path="/estimation" component={Estimation} />
            </div>
          </Col>
        </Row>
      </Container>
    );
  }
}

{/* <p>{this.props.items}</p> */}

const mapStateToProps = (state) => ({
  router: state.router
});

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    // Actions
  }, dispatch
);
export default connect(mapStateToProps, mapDispatchToProps)(App);
