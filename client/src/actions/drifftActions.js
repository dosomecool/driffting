import * as types from '../constants/actionTypes';

export function loadImageUrl(links) {
  return { type: types.LOAD_IMAGE_URL_SUCCESS, payload: links };
}

export function changeActiveStatusSuccess(status) {
  return { type: types.CHANGE_ACTIVE_STATUS_SUCCESS, payload: status };
}
  
export function changePageSuccess(path) {
  return { type: types.CHANGE_PAGE_SUCCESS, path };
}

export function selectWorkflowSuccess(workflow) {
  return { type: types.SELECT_WORKFLOW_SUCCESS, payload: workflow };
}

export function loadNetworkNodesAndEdges(nodesAndEdges) {
  return { type: types.LOAD_NETWORK_NODES_AND_EDGES_SUCCESS, payload: nodesAndEdges };
}

export function loadNetworkError(data) {
  return { type: types.LOAD_NETWORK_ERROR_SUCCESS, payload: data };
}

export function load2dInput(data) {
  return { type: types.LOAD_2D_INPUT_SUCCESS, payload: data };
}

export function load3dInput(data) {
  return { type: types.LOAD_3D_INPUT_SUCCESS, payload: data };
}

export function loadOutputSuccess(data) {
  return { type: types.LOAD_OUTPUT_SUCCESS, payload: data };
}

export function saveNetworkSuccess(model) {
  return { type: types.SAVE_NETWORK_SUCCESS, payload: model };
}

export function changeStatusSuccess(status) {
  return { type: types.UPDATE_STATUS_SUCCESS, payload: status };
}

export function changeStatus(status) {
  return (dispatch) => {
    console.log("dispaych: ", dispatch); 
    dispatch(changeStatusSuccess(status));
  };
}

// export function changeActiveStatus(type, status) {
//   const payload = { type: type, data: status };
//   return (dispatch) => {
//     dispatch(changeActiveStatusSuccess(payload));
//   };
// }
