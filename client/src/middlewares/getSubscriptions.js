import { loadImageUrl as loadImageUrlActual } from '../actions/drifftActions';
import { load2dInput as load2dInputActual } from '../actions/drifftActions';
import { load3dInput as load3dInputActual } from '../actions/drifftActions';
import { loadOutputSuccess as loadOutputSuccessActual } from '../actions/drifftActions';
import { loadNetworkError as loadNetworkErrorActual } from '../actions/drifftActions';
import { loadNetworkNodesAndEdges as loadNetworkNodesAndEdgesActual } from '../actions/drifftActions'; 
import { saveNetworkSuccess as saveNetworkSuccessActual } from '../actions/drifftActions';
import { changeStatusSuccess as changeStatusSuccessActual } from '../actions/drifftActions';

const QUEUE_IMAGE_MESSAGE = '/notification/image';
const QUEUE_VISUALIZE_2DINPUT = '/notification/visualize/2d-input';
const QUEUE_VISUALIZE_3DINPUT = '/notification/visualize/3d-input';
const QUEUE_VISUALIZE_OUTPUT = '/notification/visualize/output';
// const QUEUE_ERROR_MESSAGE = '/notification/error';
const QUEUE_VISUALIZE_ERROR = '/notification/visualize/error';
const QUEUE_VISUALIZE_NETWORK = '/notification/visualize/network';
const QUEUE_SAVE_NETWORK = '/notification/network/actions';
const QUEUE_SERVER_STATUS = '/notification/server/status';

const getSubscriptionsBuilder = (
  loadImageUrl,
  load2dInput,
  load3dInput,
  loadOutputSuccess,
  loadNetworkError,
  loadNetworkNodesAndEdges,
  saveNetworkSuccess,
  changeStatusSuccess
) => (dispatch) => {
  return [
    {
      destination: QUEUE_IMAGE_MESSAGE,
      callback: (msg) => dispatch(loadImageUrl(JSON.parse(msg.body))) 
    },
    {
      destination: QUEUE_VISUALIZE_2DINPUT,
      callback: (msg) => dispatch(load2dInput(JSON.parse(msg.body)))
    },
    {
      destination: QUEUE_VISUALIZE_3DINPUT,
      callback: (msg) => dispatch(load3dInput(JSON.parse(msg.body)))
    },
    {
      destination: QUEUE_VISUALIZE_OUTPUT,
      callback: (msg) => dispatch(loadOutputSuccess(JSON.parse(msg.body)))
    },
    {
      destination: QUEUE_VISUALIZE_ERROR,
      callback: (msg) => dispatch(loadNetworkError(JSON.parse(msg.body)))
    },
    {
      destination: QUEUE_VISUALIZE_NETWORK,
      callback: (msg) => dispatch(loadNetworkNodesAndEdges(JSON.parse(msg.body)))
    },
    {
      destination: QUEUE_SAVE_NETWORK,
      callback: (msg) => console.log(JSON.parse(msg.body))
    },
    {
      destination: QUEUE_SERVER_STATUS,
      callback: (msg) => dispatch(changeStatusSuccess(JSON.parse(msg.body)))
    }
  ];
};

const getSubscriptionsParameters = [
  loadImageUrlActual,
  load2dInputActual,
  load3dInputActual,
  loadOutputSuccessActual,
  loadNetworkErrorActual,
  loadNetworkNodesAndEdgesActual,
  saveNetworkSuccessActual,
  changeStatusSuccessActual
];

const getSubscriptions = getSubscriptionsBuilder(...getSubscriptionsParameters);

export { getSubscriptionsBuilder, getSubscriptions };
