import { default as WebSocketApiActual } from '../apis/webSocketApi';
import { getSubscriptions as getSubscriptionsActual } from './getSubscriptions';

const appInitMiddlewareBuilder = (WebSocketApi, getSubscriptions) => ({ dispatch, getState }) => {
  let initialized;
  return (next) => (action) => {
    const result = next(action);
    if(!initialized) {
      initialized = true;
      WebSocketApi.subscribeToEvents(getSubscriptions(dispatch));
    }
    return result;
  };
};

const appInitMiddlewareParameters = [
  WebSocketApiActual,
  getSubscriptionsActual
];

const appInitMiddleware = appInitMiddlewareBuilder(...appInitMiddlewareParameters);

export { appInitMiddleware };
