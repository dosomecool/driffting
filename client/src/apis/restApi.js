import axios from 'axios';

class RestApi {

  static get API_ROOT() {
    // return 'http://DrifftRestExample-env-1.pftuvpgc4q.us-east-1.elasticbeanstalk.com'; 
    return 'http://localhost:8080'; 
  }

  static submitUserInput(request) {
    return new Promise((resolve, reject) => {
      axios.post(`${RestApi.API_ROOT}/user-input`, request)
           .then((response) => resolve(response.data))
           .catch((error) => reject(error));
    });
  }

  static userAction(request) {
    console.log("reuqest: ", request);
    return new Promise((resolve, reject) => {
      axios.post(`${RestApi.API_ROOT}/user-action`, request)
           .then((response) => resolve(response.data))
           .catch((error) => reject(error));
    });
  }
}

// TODO: Add other methods for data request

export default RestApi;
