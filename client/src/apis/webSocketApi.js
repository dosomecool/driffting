import webstomp from 'webstomp-client';
import SockJS from 'sockjs-client';
import { changeStatusSuccess }  from '../actions/drifftActions'

import store from '../store';

// best client example: https://www.callicoder.com/spring-boot-websocket-chat-example/
// https://stackoverflow.com/questions/24756312/how-can-i-send-a-message-on-connect-event-sockjs-stomp-spring

class WebSocketApi {
  static get WS_SERVER_URL() {
    return 'http://localhost:8080/queue/topic/test';
  }

  static options = {
    debug: false,
    protocols: webstomp.VERSIONS.supportedProtocols()
  };

  static headers = {
    login: 'mylogin',
    passcode: 'mypasscode',
    // additional header
    'client-id': 'my-client-id'
  };

  static subscribeToEvents(subscriptions) {

    const connection = new SockJS(WebSocketApi.WS_SERVER_URL);
    const stompClient = webstomp.over(connection, this.options);
    
    stompClient.connect({}, (frame) => {
      console.log('connecting...', frame); 
    });
    
    connection.onopen = () => {
      subscriptions.forEach((subscription) => {
        stompClient.subscribe(subscription.destination, subscription.callback);
      });
    };

    connection.onerror = () => {
      store.dispatch(changeStatusSuccess(
        {
          status: "warning",
          description: "Connection error"
        }
      ));
    };

    connection.onclose = () => {
      store.dispatch(changeStatusSuccess(
        {
          status: "danger",
          description: "Server is disconnected"
        }
      ));
    };
  }
}

export default WebSocketApi;

// Example: Send message to server
      // JSON.stringify(this.message)

      // Send method here

      // stompClient.send('/app/inbox', {}, JSON.stringify(
      //     {
      //       "color": "maroon",
      //       "type": "crown vic"
      //     }
      //   ));

        // static sendMessage(message) {
  //   console.log('connection is encountered some error');
  //   stompClient.send(message.destination, {}, message.payload); 
  // }

// Example: on event
    // connection.onerror = () => console.log('connection is encountered some error');
    // connection.onmessage = (msg) => {
    //   console.log('message from the server: ', JSON.parse(msg.body)); 
    // };