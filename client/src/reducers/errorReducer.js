import * as types from '../constants/actionTypes';

const errorReducer = (state = [], action) => {
  switch (action.type) {
    case types.SAVE_INPUT_ERRORS_SUCCESS:
      return action.payload
    case types.CLEAR_INPUT_ERRORS_SUCCESS:
      return action.payload
    default:
      return state
  }
}

export default errorReducer;