import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import initialStateReducer from './initialStateReducer';
import { reducer as formReducer } from 'redux-form'
import userInputReducer from './userInputReducer';
import statusReducer from './statusReducer';
import errorReducer from './errorReducer';
import modelReducer from './modelReducer';
import workFlowReducer from './workflowReducer';
import visualizeInputReducer from '../features/estimation/reducers/visualizeInputReducer';
import visualizeOutputReducer from '../features/estimation/reducers/visualizeOutputReducer';
import visualizeErrorReducer from '../features/estimation/reducers/visualizeErrorReducer';
import visualizeNetworkReducer from '../features/estimation/reducers/visualizeNetworkReducer';

const rootReducer = combineReducers({
    router: routerReducer,
    initialState: initialStateReducer,
    form: formReducer,
    userInput: userInputReducer,
    status: statusReducer,
    errors: errorReducer,
    model: modelReducer,
    workflow: workFlowReducer,
    visualizeInput: visualizeInputReducer,
    visualizeOutput: visualizeOutputReducer,
    visualizeError: visualizeErrorReducer,
    visualizeNetwork: visualizeNetworkReducer
});

export default rootReducer;
