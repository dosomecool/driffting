import * as types from '../constants/actionTypes';

const userInputReducer = (state = {}, action) => {
  switch (action.type) {
    case types.LOAD_INPUT_VALUES_SUCCESS:
      return {
        ...action.payload
      }
    default:
      return state
  }
}

export default userInputReducer;
