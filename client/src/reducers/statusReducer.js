import * as types from '../constants/actionTypes';

const statusReducer = (state = {}, action) => {
  switch (action.type) {
    case types.UPDATE_STATUS_SUCCESS:
    console.log("status update")
      return action.payload
    default:
      return state
  }
}

export default statusReducer;