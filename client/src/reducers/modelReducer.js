import * as types from '../constants/actionTypes';
import unavailable from '../not-available.jpg';

const root = 'https://s3.amazonaws.com/drifftspace.com/';

const model = [
  {
    name: 'DIFFUSION',
    isActive: false,
    link: unavailable
  },
  {
    name: 'KILLING',
    isActive: false,
    link: unavailable
  }
]; 

const drifftModelReducer = (state = model, action) => {
  switch (action.type) {
    case types.LOAD_IMAGE_URL_SUCCESS:
      state.forEach((item) => {
        if(item.name === action.payload.type) {
          item.link = root + action.payload.data;
        }
      });
      return state;
    case types.CHANGE_ACTIVE_STATUS_SUCCESS:
      state.forEach((item) => {
        if(item.name === action.payload.type) {
          item.isActive = true;
        }
      });
    default:
      return state;
  }
};

export default drifftModelReducer;
