import * as types from '../constants/actionTypes';

const workflowReducer = (state = {}, action) => {
  switch (action.type) {
    case types.SELECT_WORKFLOW_SUCCESS:
      return action.payload;
    default:
      return state;
  }
};

export default workflowReducer;