import React from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { Field, FieldArray, reduxForm } from 'redux-form'
import { Container, Row, Col, Button } from 'reactstrap';
import { loadInputValues } from '../actions/userInputActions';

import DropdownList from 'react-widgets/lib/DropdownList';

import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';

import '../styles/formStyle.css';

library.add(faTrashAlt);

// https://medium.com/dailyjs/why-build-your-forms-with-redux-form-bcacbedc9e8
// https://redux-form.com/7.1.1/docs/faq/handlevson.md/

// Regex
// https://www.w3schools.com/Jsref/jsref_obj_regexp.asp

// Clear form 
// https://redux-form.com/6.5.0/examples/simple/

// drop down
// https://stackoverflow.com/questions/40075281/how-to-create-custom-dropdown-field-component-with-redux-form-v6

const required = value => (value ? undefined : 'Value is required');
const floatRequired = value => (!isNaN(parseFloat(value)) ? undefined : 'Float value is required');
const number = value => (value && isNaN(Number(value)) ? 'Value must  be a number' : undefined);

const equation = (value) => (value && /(?=(?!pow))(?=[^\s*x\s*t\s*(\s*)\s**\s*-\s*+\s*,\s*.\s*])(?=[^0-9])/i.test(value) ? 'Not a valid equation' : undefined);

const alphaNumeric = value =>
  value && /[^a-zA-Z0-9 ]/i.test(value)
    ? 'Only alphanumeric characters'
    : undefined

const renderField = ({
  input,
  label,
  type,
  disabled,
  meta: { touched, error, warning }
}) => (
    <Container>
      <Row>
        <Col sm="6">
          <label className="input-label">{label}</label>
        </Col>
        <Col sm="6">
          <Row>
            <input className="input-textfield" {...input} placeholder='' type={type} disabled={disabled}/>
          </Row>
          <Row>
            {touched && ((error && <span className="input-error">{error}</span>) || 
            (warning && <span className="input-warning">{warning}</span>))}
          </Row>
        </Col>
      </Row>
    </Container>
)

const renderDropDownField = ({ input, label, values}) => (
  <Container>
    <Row>
      <Col sm="6">
        <label className="input-label">{label}</label>
      </Col>
      <Col sm="6">
        <Row>
          <select className="dropdown-list" {...input} >
            {values.map((value) => (
              <option key={value} value={value}>{value}</option>
            ))}
          </select>
        </Row>
      </Col>
    </Row>
  </Container>
)

const renderHiddenLayers = ({ fields, activation, meta: { error, submitFailed } }) => (
  <ul className="form-ul">
    {fields.map((hiddenLayer, index) => (
      <li key={index}>
        <Row>
          <Col sm="6"><h6>Hidden Layer #{index + 1}</h6></Col>
          <Col sm="6"><FontAwesomeIcon className={"form-svg-inline"} icon={faTrashAlt} title="Remove Layer" onClick={() => fields.remove(index)} /></Col>
        </Row>
        <Field
          name={`${hiddenLayer}.size`}
          type="text"
          component={renderField}
          label="NODES [ NUMBER ]:"
        />
        <Field
          name={`${hiddenLayer}.activation`}
          component={renderDropDownField}
          label="ACTIVATION [ FUNCTION ]:"
          values={activation}
        />
      </li>  
    ))}
    <li className={"mx-auto w-50"}>
      <br/>
      <Button block color="secondary" size="sm" outline onClick={() => fields.push({})}>
        Add Hidden Layer
      </Button>
      {submitFailed && error && <span>{error}</span>}
    </li>
  </ul>
)

let UserInputForm = (props) => {
  const { handleSubmit, load, pristine, reset, submitting} = props;
  return (
    <form autoComplete="off" autoCorrect="off" spellCheck="off" onSubmit={handleSubmit}>
      <div 
        key="id-section" 
        className="text-center font-weight-bold" 
        ref={(props.target==='id-section') ? props.reference : null}
        ><h6>Simulation</h6>
      </div>
      <Field
        name="id"
        type="text"
        component={renderField}
        label="ID [ UUID ]:"
        // warn={}
        props={{ disabled: true }}
      />
      <br/>
      <div 
        key="diffusion-section" 
        className="text-center font-weight-bold" 
        ref={(props.target==='diffusion-section') ? props.reference : null}
        ><h6>Diffusion</h6>
      </div>
      <Field
        name="drift"
        type="text"
        component={renderField}
        label="DRIFT [ μ(X(t),t) ]:"
        validate={[required]}
        // warn={}
        props={{ disabled: false }}
      />
      <Field 
        name="diffusion"
        type="text"
        component={renderField}
        label="DIFFUSION [ σ(X(t),t) ]:"
        validate={[required]}
        // warn={}
      />
      <Field
        name="initialState"
        type="text"
        component={renderField}
        label="INITIAL STATE [ X(0) ]:"
        validate={[required, floatRequired, number]}
        // warn={}
      />
      <br/>
      <Field
        name="t0"
        type="text"
        component={renderField}
        label="START TIME [ T-MIN ]:"
        validate={[required, floatRequired, number]}
        // warn={}
      />
      <Field
        name="tf"
        type="text"
        component={renderField}
        label="FINAL TIME [ T-MAX ]:"
        validate={[required, floatRequired, number]}
        // warn={}
      />
      <Field
        name="dt"
        type="text"
        component={renderField}
        label="DELTA TIME [ Δt ]:"
        validate={[required, floatRequired, number]}
        // warn={alphaNumeric}
      />
      <br/>
      <Field
        name="x0"
        type="text"
        component={renderField}
        label="START STATE [ X-MIN ]:"
        validate={[required, floatRequired, number]}
        // warn={}
      />
      <Field
        name="xf"
        type="text"
        component={renderField}
        label="FINAL STATE [ X-MAX ]:"
        validate={[required, floatRequired, number]}
        // warn={}
      />
      <Field
        name="dx"
        type="text"
        component={renderField}
        label="DELTA X [ Δx ]:"
        validate={[required, floatRequired, number]}
        // warn={alphaNumeric}
      />
      <br/>
      <Field
        name="paths"
        type="text"
        component={renderField}
        label="NUMBER OF PATHS [ NUMBER ]:"
        validate={[required, floatRequired, number]}
        // warn={}
      />
      <Field
        name="seed"
        type="text"
        component={renderField}
        label="SEED [ FIX ]:"
        validate={[required, floatRequired, number]}
        // warn={}
      />
      <br></br>
      <div 
        key="killing-section" 
        className="text-center font-weight-bold" 
        ref={(props.target==='killing-section') ? props.reference : null}
        ><h6> Killing Function</h6>
      </div>
      <Field
        name="killing"
        type="text"
        component={renderField}
        label="KILLING FUNCTION [ k(x,t) ]:"
        // input={{ disabled: (props.workflow=='approximate') ? true : false }}
        // input={{ disabled: false }}
        validate={[required]}
        // warn={}
      />
      <br></br>
      <div 
        key="network-section" 
        className="text-center font-weight-bold" 
        ref={(props.target==='network-section') ? props.reference : null}
        ><h6>Neural Network</h6>
      </div>
      <Field
        name="input"
        component={renderDropDownField}
        label="INPUT [ DATA ]:"
        values={props.listOptions.input}
      />
      <FieldArray 
        name="hiddenLayers" 
        component={renderHiddenLayers} 
        activation={props.listOptions.activationFunctions}  
      /> 
      <Field
        name="output"
        component={renderDropDownField}
        label="OUTPUT [ DATA ]:"
        values={props.listOptions.output}
      />
      <Field
        name="outputActivation"
        component={renderDropDownField}
        label="OUTPUT ACTIVATION [ FUNCTION ]:"
        values={props.listOptions.activationFunctions}
      />
      <Field
        name="costFunctions"
        component={renderDropDownField}
        label="COST [ FUNCTION ]:"
        values={props.listOptions.costFunctions}
      />
      <Field
        name="optimization"
        component={renderDropDownField}
        label="OPTIMIZATION [ ALGORITHM ]:"
        values={props.listOptions.optimization}
      />
      <Field
        name="batch"
        type="text"
        component={renderField}
        label="BATCH [ SIZE ]:"
        validate={[required, floatRequired, number]}
        // warn={}
      />
      <Field
        name="variance"
        type="text"
        component={renderField}
        label="WEIGHT VARIANCE [ 0-10 ]:"
        validate={[required, floatRequired, number]}
        // warn={}
      />
      <Field
        name="shuffle"
        component={renderDropDownField}
        label="SHUFFLE [ DATA ]:"
        values={props.listOptions.shuffle}
      /> 
      <Field
        name="networkSeed"
        type="text"
        component={renderField}
        label="SEED [ FIX ]:"
        validate={[required, floatRequired, number]}
        // warn={}
      />
    </form>
  )
}

UserInputForm = reduxForm({
  form: 'user-input-form' // a unique identifier for this form,
})(UserInputForm);

const mapStateToProps = (state, props) => ({
  enableReinitialize: true,
  keepDirtyOnReinitialize: false,
  initialValues: props.defaultValues
})

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      load: loadInputValues
    }, dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(UserInputForm);
