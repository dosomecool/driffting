import React from 'react';
import { connect } from 'react-redux';
import { submit } from 'redux-form';
import { Button } from 'reactstrap';

const RemoteSubmitButton = ({ dispatch }) => (
  <Button className={"mx-0"} size="sm" color="primary" block onClick={() => dispatch(submit('user-input-form'))}>Apply</Button>
)

export default connect()(RemoteSubmitButton)