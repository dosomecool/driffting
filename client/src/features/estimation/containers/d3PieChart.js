import React from 'react';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { PieChart, d3 } from 'react-d3-components';
import { Row, Col } from 'reactstrap';
import { Card, CardText, CardBody } from 'reactstrap';
import InputConfiguration from "../containers/configuration";

import '../styles/d3Style.css';

// Github
// https://github.com/codesuki/react-d3-components#other-charts

class D3PieChart extends React.Component {
  constructor(props) {
    super(props);


    this.state = {
    };

    this.data = {
      label: 'somethingA',
      values: [{ x: 'Iterations', y: 10 }, { x: 'Not complete', y: 4 }]
    };

    this.sort = d3.descending; //d3.ascending;
  }


  
  render() {
    return (
      <Col sm="4">
        <Row>
          <Col sm="8">
            {/* header */}
            <p className="text-left font-weight-bold mb-0">Neural Network Graph</p>
          </Col>
          <Col sm="4">
            <InputConfiguration target={'killing-section'} />
          </Col>
        </Row>
        <Card>
          <div className="bg-light">
            <PieChart
              data={this.data}
              width={318}
              height={180}
              margin={{ top: 10, bottom: 10, left: 100, right: 100 }}
              sort={this.sort}
            />
          </div>
          <CardBody>
            <CardText>{this.props.text}</CardText>
          </CardBody>
        </Card>
      </Col>
    );
  }
}

const mapStateToProps = (state) => ({
  // prop: state.<name>
});

const mapDispatchToProps = (dispatch) => bindActionCreators(
  { 
    changePage: (page) => push(page) 
  }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(D3PieChart);
