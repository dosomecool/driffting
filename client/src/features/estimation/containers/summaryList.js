import React from 'react';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Card, CardText, CardBody, ListGroup, ListGroupItem, Badge, ListGroupItemText } from 'reactstrap';
import { Row, Col } from 'reactstrap';
import { Scrollbars } from 'react-custom-scrollbars';
import InputConfiguration from "../containers/configuration";

class SummaryList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
    };

    this.getListGroupItem = this.getListGroupItem.bind(this);

    this.topics = [
      "Input Layer",
      "Hidden Layer", 
      "Output Layer",
      "Cost Function",
      "Initial Weight Distribution",
      "Optimization Algorithm",
      "Batch", 
      "Epoch"
    ];
  }

  getListGroupItem(topic, pill, description) {
    return (
      <ListGroupItem key={topic.toString()} className="justify-content-between">{topic}<Badge>{pill}</Badge>
        <ListGroupItemText>
          {description}
      </ListGroupItemText>
      </ListGroupItem>
    );
  }

  render() {
    const topicList = [];
    this.topics.forEach( topic => {
      topicList.push(this.getListGroupItem(topic));
    })

    return (
      <Col sm={this.props.size}>
        <Row>
          <Col sm="8">
            {/* header */}
            <p className="text-left font-weight-bold mb-0">{this.props.title}</p>
          </Col>
          <Col sm="4">
            <InputConfiguration target={'killing-section'} />
          </Col>
        </Row>
        <Card>
          <div className="bg-light" ref={this.widthRef}>
            <ListGroup >
              {topicList}
            </ListGroup>
          </div>
          {/* <CardBody>
            <CardText>{this.props.text}</CardText>
          </CardBody> */}
        </Card>
      </Col>
    );
  }
}

const mapStateToProps = (state) => ({
  // prop: state.<name>
});

const mapDispatchToProps = (dispatch) => bindActionCreators(
  { 
    changePage: (page) => push(page) 
  }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(SummaryList);
