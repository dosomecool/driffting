import React from 'react';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { LineChart, d3 } from 'react-d3-components';
import { Card, CardText, CardBody } from 'reactstrap';
import { Row, Col } from 'reactstrap';
import InputConfiguration from "../containers/configuration";

import '../styles/d3Style.css';

// Github
// https://github.com/codesuki/react-d3-components#other-charts

// Responsive
// https://gist.github.com/devholic/08853562e4e1723d0a13ed6aeb2cf4a8

// Read this
// https://github.com/codesuki/react-d3-components/issues/9

class ErrorChart extends React.Component {
  constructor(props) {
    super(props);

    this.dashFunc = this.dashFunc.bind(this);
    this.dashFunc = this.dashFunc.bind(this);
    this.linecapFunc = this.linecapFunc.bind(this);
    this.tooltipLine = this.tooltipLine.bind(this);

    this.updateDimensions = this.updateDimensions.bind(this);

    this.state = {
      isFull: false,
      width: 350,
      height: 180,
      data: [
        {
          label: 'output',
          values: [{
            x: 0.0,
            y: 0.0
          }]
        }
      ]
    };

    // this.data = this.data.bind(this);

    // Create a width parent reference for the chart width
    this.widthRef = React.createRef();
  }

  updateDimensions() {
    const update_width = this.widthRef.current.clientWidth;
    this.setState({ width: update_width });
  }

  componentDidMount() {
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions);
  }

  dashFunc(label) {
    if (label === "killing") {
      return "4 4 4";
    } 
    if (label === "approximationError") {
      return "4 3 4";
    }
  }

  widthFunc(label) {
    if (label === "killing") {
      return "2";
    }
    if (label === "approximationError") {
      return "3";
    } 
  }

  linecapFunc(label) {
    if (label === "killing") {
      return "round";
    }
  };

  tooltipLine(path, value) {
    return "[" + value.x.toFixed(2) + ", " + value.y.toFixed(2) + "]";
  };

  // data() {
  //   if(this.props.data) {
  //     return this.props.data
  //   } else {
  //     return { label: 'init', values: [{ x: 0.0, y: 0.0 }]}; 
  //   }
  // };

  static getDerivedStateFromProps(nextProps, prevState) {

    if (nextProps.data.length !== 0) {
      return {
        data: nextProps.data
      };
    } else return null; 
  }

  render() {
    return (
      <Col sm={this.props.size}>
        <Row>
          <Col sm="8">
            <p className="text-left font-weight-bold mb-0">{this.props.title}</p>
          </Col>
          <Col sm="4">
            <InputConfiguration target={this.props.target} />
          </Col>
        </Row>
        <Card>
          <div className="bg-light" ref={this.widthRef}>
            <LineChart styles={this.style}
              data={this.state.data}
              barPadding={0.3}
              width={this.state.width}
              height={this.state.height}
              margin={{ top: 10, bottom: 20, left: 50, right: 10 }}
              tooltipHtml={this.tooltipLine}
              tooltipContained={false}
              tooltipMode={'mouse'}
              xAxis={{ tickArguments: [10], label: this.props.xLabel }}
              yAxis={{ tickArguments: [5], label: this.props.yLabel }}
              shapeColor={"red"}
              stroke={{ strokeDasharray: this.dashFunc, strokeWidth: this.widthFunc, strokeLinecap: this.linecapFunc}} /> 
          </div>
          <CardBody>
            <CardText>{this.props.text}</CardText>
          </CardBody>
        </Card>
      </Col>
    );
  }
}

const mapStateToProps = (state) => ({
  // prop: state.<name>
});

const mapDispatchToProps = (dispatch) => bindActionCreators(
  { 
    changePage: (page) => push(page) 
  }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(ErrorChart);
