import React from 'react';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Container, Row, Col } from 'reactstrap';
import { Button } from 'reactstrap';
import InputConfiguration from "../containers/configuration";
import D3LineChart from '../containers/d3LineChart';
import D3PieChart from '../containers/d3PieChart';
import Vis3dChart from '../containers/vis3dChart';
import NetworkGraph from '../containers/networkGraph';
import SummaryList from '../containers/summaryList';
import Controls from '../containers/controls';
import * as actions from '../actions/supportedFunctions';
import * as userInputActions from '../actions/userInputActions';


// TODO: https://fontawesome.com/how-to-use/on-the-web/using-with/react

class Estimation extends React.Component {
  constructor(props) {
    super(props);

    const diffusion = this.props.actions.diffusion(0.0, 0.5, 0.95);
    const killing = this.props.actions.killing(diffusion, (x) => actions.quadraticKilling(0.25, x));

    this.state = {
      diffusion: diffusion,
      killing: killing
    };

    this.target = this.target.bind(this); 
  }

  static getDerivedStateFromProps(nextProps, prevState) {
     return null;
  }

  target() {

    // if(this.props.input.target)

    return 
  }

  render() {

    const req = {test: "cancellllllll"};

    return (
      <Container className="justify-content-center">
        <Row>
          <Col sm="8">
            <Vis3dChart
              size={12}
              title={"Network Input"}
              text={"Kolmogorov solution percentiles."}
              data={this.props.input} />
          {/* <D3LineChart
            size={4}
            title={"Input"}
            xLabel={"t"}
            yLabel={"X(t)"}
            data={this.state.diffusion}
            text={"Simulated diffusion model."}
            target={"diffusion-section"} /> */}

            {/* <NetworkGraph text={"Vis network experimental."}/> */}
            <D3LineChart
              size={12}
              title={"Network Output"}
              xLabel={"t"}
              yLabel={"H(t)"}
              data={this.props.output}
              text={"Cumulative hazard from observed data."}
              target={"diffusion-section"} />
            <D3LineChart
              size={12}
              title={"Network Error"}
              xLabel={"Epochs"}
              yLabel={"Error"}
              data={this.props.error}
              text={"Approximation error"}
              target={"diffusion-section"} />
          </Col>
          {/* <Col sm="8">

          </Col> */}
          <Col sm="4">
            <Controls
              title={"Action Center"}
              text={"Actions"}
            />
            {/* <Button onClick={()=>this.props.cancelRequest(req)}>Cancel Request</Button> */}
            <SummaryList 
              title={"Model Summary"}
            />
          </Col>
        </Row>
        {/* <Row>
          <D3LineChart
            size={12}
            title={"Error"}
            xLabel={"Iterations"}
            yLabel={"Error"}
            data={this.props.error} 
            text={"Approximation error"}
            target={"diffusion-section"} />
        </Row> */}
        {/* <Row>
          <SummaryList/>
          <D3PieChart 
            text={"Neural network controls."} />
        </Row> */}
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  // input: state.input,
  // model: state.model,
  input: state.visualizeInput,
  output: state.visualizeOutput,
  error: state.visualizeError,
  actions: actions
});

const mapDispatchToProps = (dispatch) => 
bindActionCreators(
  { 
    // changePage: (page) => {push(page)},
    ...userInputActions
  }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(Estimation);
