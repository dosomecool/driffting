import React from 'react';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Card, CardText, CardBody } from 'reactstrap';
import Fullscreen from "react-full-screen";
import { Row, Col } from 'reactstrap';
import { Network } from 'vis';
import InputConfiguration from "../containers/configuration";
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExpand } from '@fortawesome/free-solid-svg-icons';
library.add(faExpand);

class NetworkGraph extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isFull: false,
      width: 350,
      height: 180
    };

    this.changeChosenEdgeFromArrow = (values, id, selected, hovering) => {
      values.fromArrow = true;
    }

    // specify options
    this.options = {
      width: this.state.width.toString() + 'px',
      height: this.state.height.toString() + 'px',
      autoResize: true,
      nodes: {
        shape: 'circle',
        size: 100,
        scaling: {
          label: {
              enabled: true,
              min: 50,
              max: 50
          }
      },
      value: 1,
        font: {
          size: 32,
          color: '#000000'
        },
        borderWidth: 2
      },
      edges: {
        width: 2
      },
      physics: false,
      interaction:{ hover:true }, 
      
    };

    this.resize = this.resize.bind(this);
    }

  componentWillReceiveProps(nextProps) {
    if (this.props.data !== nextProps.data) {

      // do something with location
      this.network.setData({
        nodes: nextProps.data.nodes,
        edges: nextProps.data.edges.map((edge) => {
           edge['font'] = {align: 'bottom', size: 36, strokeWidth: 3};
           edge['chosen'] = { label: false, edge: this.changeChosenEdgeFromArrow };
           return edge;
        })
      })
    }
  }

  componentDidMount() {

    this.data = {
        nodes: [],
        edges: []
    };
    
    this.container = document.getElementById('network');
    this.network = new Network(this.container, this.data, this.options); 
  }

  goFull = () => {
    this.setState({ isFull: true });
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.isFull) {
      this.resize(window.innerWidth, window.innerHeight);
    } else {
      this.resize(this.state.width, this.state.height);
    }
    return true;
  }

  resize(width, height) {
    this.network.setSize(width + 'px', height + 'px');
    // this.network.redraw();
    this.network.fit();
  }
  
  render() {
    return (
      <Col sm="4">
        <Row>
          <Col sm="8">
            {/* header */}
            <p className="text-left font-weight-bold mb-0">Neural Network Graph</p>
          </Col>
          <Col sm="4">
            <InputConfiguration target={'killing-section'} />
            <FontAwesomeIcon icon={faExpand} onClick={this.goFull}/>
          </Col>
        </Row>
        <Card> 
          <div className="bg-light">
            <Fullscreen
              enabled={this.state.isFull}
              onChange={isFull => this.setState({ isFull })}>
              <graph id="network" />
            </Fullscreen>
          </div>
          <CardBody>
            <CardText>Neural Network Graph</CardText>
          </CardBody>
        </Card>
      </Col>
    );
  }
}

const mapStateToProps = (state) => ({
  data: state.visualizeNetwork
});

const mapDispatchToProps = (dispatch) => bindActionCreators(
  { 
    changePage: (page) => push(page) 
  }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(NetworkGraph);
