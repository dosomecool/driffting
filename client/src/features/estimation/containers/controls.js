import React from 'react';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Card, CardText, CardBody, ListGroup, ListGroupItem, Badge, ListGroupItemText } from 'reactstrap';
import { Row, Col } from 'reactstrap';
import InputConfiguration from "../containers/configuration";
import * as userInputActions from '../actions/userInputActions';
import Status from '../../status/containers/status'
import { Tooltip } from 'reactstrap';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlay, faStop, faDownload, faUpload } from '@fortawesome/free-solid-svg-icons';

library.add(faPlay, faStop, faDownload, faUpload);

class Controls extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.download = this.download.bind(this);
    this.show = this.show.bind(this);

    this.state = {
      tooltipOpen: false,
      linkReady: false,
      data: "",
      status: 'Closed',
      collapse: false
    };

    this.onEntering = this.onEntering.bind(this);
    this.onEntered = this.onEntered.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
    this.toggle = this.toggle.bind(this);
  }

  onEntering() {
    this.setState({ status: 'Opening...' });
  }

  onEntered() {
    this.setState({ status: 'Opened' });
  }

  onExiting() {
    this.setState({ status: 'Closing...' });
  }

  onExited() {
    this.setState({ status: 'Closed' });
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  show() {
    return (
      <a href={`data: ${this.state.data}`} download="data.json">download JSON</a>
    );
  }

  download() {
    this.props.downloadRequest({action: "download"}).then(
      (obj) => {
        console.log('obj: ', JSON.stringify(obj));
        this.setState({
          linkReady: true,
          // data: "text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(obj))
          data: "text/json;charset=utf-8," + JSON.stringify(obj)
        });
      }
    );
  }

  render() {
    return (
      <Col sm={this.props.size}>
        <Row>
          <Col sm="8">
            {/* header */}
            <p className="text-left font-weight-bold mb-0">{this.props.title}</p>
          </Col>
          <Col sm="4">
            <InputConfiguration target={"killing-section"} />
          </Col>
        </Row>
        <Card>
          <Status size={12}/>
          { this.state.linkReady ? this.show() : null }
          <CardBody>
          {/* <Row> */}
              <FontAwesomeIcon className={"svg-inline--fa-left"} icon={faPlay} />
              <FontAwesomeIcon className={"svg-inline--fa-left"} icon={faStop} onClick={()=>this.props.downloadRequest({action: "stop"})}/>
              <FontAwesomeIcon icon={faDownload} onClick={()=>this.download()}/>
              {/* <div>
                {
                  this.state.linkReady ?
                    <a href={`data: ${this.state.data}`} download="data1.json">Download JSON</a>
                    : <button onClick={this.download}> Prepare link </button>
                }
              </div> */}
              {/* right here see what happenes */}
            {/* </Row> */}
            {/* <CardText>{this.props.text}</CardText> */}
          </CardBody>
        </Card>
      </Col>
    );
  }
}

const mapStateToProps = (state) => ({
  // prop: state.<name>
});

const mapDispatchToProps = (dispatch) => bindActionCreators(
  { 
    // changePage: (page) => push(page) 
    ...userInputActions
  }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(Controls);
