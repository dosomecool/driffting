import React from 'react';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Card, CardText, CardBody } from 'reactstrap';
import Fullscreen from "react-full-screen";
import { Row, Col } from 'reactstrap';
import InputConfiguration from "../containers/configuration";
import { DataSet, Graph3d } from 'vis';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExpand, faCog } from '@fortawesome/free-solid-svg-icons';
import DropdownList from '../../commons/components/dropdown';
import CollapsePanel from '../../commons/components/collapse';

library.add(faExpand, faCog);

class Vis3dChart extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isFull: false,
      height: "350px",
      data: {},
      style: "surface",
      show: false
    };

    this.styles = [
      "Surface",
      "Bar",
      "Dot"
    ];

    this.data = new DataSet();
    this.data.add(
      {
        id: 0,
        x: 0,
        y: 0,
        z: 0,
        style: 0
      });

    // specify options
    this.options = {
      width: "100%",
      height: "100%",
      style: this.state.style,
      xBarWidth: 0.01,
      yBarWidth: 0.01,
      showPerspective: true,
      showGrid: true,
      showShadow: false,
      keepAspectRatio: true,
      verticalRatio: 0.5
    };

    this.container = null;
    this.updateDimensions = this.updateDimensions.bind(this);
    this.onStyleChange = this.onStyleChange.bind(this);
    this.settings = this.settings.bind(this);

    // Create a width parent reference for the chart width
    this.widthRef = React.createRef();

    this.settingsRef = React.createRef();
  }

  updateDimensions() {
    const update_width = this.widthRef.current.clientWidth.toString() + 'px';
    this.graph.setSize(update_width, this.state.height);
  }
  
  componentDidMount() {
    this.container = document.getElementById('3d-chart');
    this.graph = new Graph3d(this.container, this.data, this.options);
    window.addEventListener("resize", this.updateDimensions);
    this.updateDimensions();
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions);
  }

  goFull = () => {
    this.setState({ isFull: true });
  }

  toggle = () => {
    this.settingsRef.current.toggle();
  }

  // shouldComponentUpdate(nextProps, nextState) {
   
  //   if (nextState.isFull) {
  //     const update_width = window.innerWidth.toString() + 'px';
  //     const update_height = window.innerHeight.toString() + 'px';
  //     this.graph.setSize(update_width, update_height);
  //   } else {
  //     this.updateDimensions();
  //   }

  //   // if(nextState.data) {
  //   //   this.graph.setData(nextProps.data.values);
  //   // }
  //   return true;
  // }

  componentDidUpdate(prevProps, prevState) {
  
    if (this.state.isFull) {
      const update_width = window.innerWidth.toString() + 'px';
      const update_height = window.innerHeight.toString() + 'px';
      this.graph.setSize(update_width, update_height);
    }

    if(this.state.data !== prevState.data) {
      this.graph.setData(this.state.data.values);
    }

    if(this.state.style !== prevState.style) {

      this.options = {
        style: this.state.style,
        xBarWidth: 0.01,
        yBarWidth: 0.01,
        showPerspective: true,
        showGrid: true,
        showShadow: false,
        keepAspectRatio: true,
        verticalRatio: 0.5
      };

      this.data = new DataSet();

      this.state.data.values.forEach(point => {
        this.data.add(point);
      });
      
      this.graph = new Graph3d(this.container, this.data, this.options);
      this.updateDimensions();
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    return {
      data: nextProps.data
    };
  }

  onStyleChange = (e) => {
    e.persist();
    if (e.target.innerText === "Surface") {
      this.setState({
        style: "surface"
      });
      return "Surface";
    } else if (e.target.innerText === "Bar") {
      this.setState({
        style: "bar"
      });
      return "Bar";
    } else if (e.target.innerText === "Dot") {
      this.setState({
        style: "dot"
      });
      return "Dot";
    }
  }

  settings() {
    return (
      <Row>
        <Col className={"text-right"} sm="6">
          Style
        </Col>
        <Col className={"text-left"} sm="6">
          <DropdownList
            items={this.styles}
            selected={this.styles[0]}
            ref={this.styleRef}
            onStyleChange={this.onStyleChange} />
        </Col>
      </Row>
    );
  }

  render() {
    return (
      <Col sm={this.props.size}>
        <Row>
          <Col sm="8">
            {/* header */}
            <p className="text-left font-weight-bold mb-0">{this.props.title}</p>
          </Col>
          <Col sm="4">
            <InputConfiguration target={'killing-section'} />
            <FontAwesomeIcon icon={faExpand} onClick={this.goFull} />
            <FontAwesomeIcon icon={faCog} onClick={this.toggle}/>
          </Col>
        </Row>
        <Card> 
          <div className="bg-light"  ref={this.widthRef}>
          <Fullscreen
              enabled={this.state.isFull}
              onChange={isFull => this.setState({ isFull })}>
              <graph id="3d-chart" />
            </Fullscreen>
          </div>
          <CardBody>
            <CardText>{this.props.text}</CardText>
            <CollapsePanel 
              ref={this.settingsRef}
              buttonName={"Settings"}
              component={this.settings()}/>
          </CardBody>
        </Card>
      </Col>
    );
  }
}

const mapStateToProps = (state) => ({
  // prop: state.<name>
});

const mapDispatchToProps = (dispatch) => bindActionCreators(
  { 
    changePage: (page) => push(page) 
  }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(Vis3dChart);
