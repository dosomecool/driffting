import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { submit } from 'redux-form';
import { Container, Row, Col, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import * as userInputActions from '../actions/userInputActions';
import { Scrollbars } from 'react-custom-scrollbars';
import { SpringSystem, MathUtil } from 'rebound';
import RemoteSubmitButton from '../components/remoteSubmitButton';
import { UncontrolledAlert } from 'reactstrap';
import UserInputForm from '../components/userInputForm';

import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCogs } from '@fortawesome/free-solid-svg-icons';

library.add(faCogs);

class Configuration extends React.Component {
  constructor(props, ...rest) {
    super(props, ...rest);

    this.state = {
      modal: false,
      workflow: this.props.workflow,
      errors: [],
      target: this.props.target,
      status: 'error',
      defaultValues: {
        drift: "0.05",
        diffusion: "0.5",
        initialState: "0.0",
        t0: "0.0",
        tf: "5.0",
        dt: "0.01",
        x0: "-5.0",
        xf: "5.0",
        dx: "0.01",
        paths: "10000",
        seed: "123",
        killing: "0.25 * pow(x, 2)",
        input: "diffusion-relative-frequency",
        hiddenLayers: [
          { 
           size: "10",
           activation: "elu" 
          }
        ],
        output: "mean-survival",
        outputActivation: "linear",
        costFunctions: "mse",
        optimization: "vanilla",
        epochs: "25",
        batch: "5",
        variance: "0.2",
        shuffle: "false",
        networkSeed: '456'
      }
    };

    this.toggle = this.toggle.bind(this);
    this.handleSpringUpdate = this.handleSpringUpdate.bind(this);
    this.handleClick= this.handleClick.bind(this);
    this.alertExample = this.alertExample.bind(this);

    this.section = React.createRef();

    this.options = {
      input: ["diffusion-relative-frequency"],
      output: ["mean-survival", "cumulative-hazard", "density"],
      activationFunctions: ["relu", "leaky-relu", "elu", "sigmoid", "tanh", "gaussian", "linear"],
      costFunctions: ["mad", "mse"],
      optimization: ["vanilla"],
      shuffle: ["true", "false"]
    };
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

  componentDidMount() {
    this.springSystem = new SpringSystem();
    this.spring = this.springSystem.createSpring();
    this.spring.addListener({ onSpringUpdate: this.handleSpringUpdate });
  }

  componentDidUpdate() {
    (this.state.modal) ? this.handleClick() : null;
  }

  componentWillUnmount() {
    this.springSystem.deregisterSpring(this.spring);
    this.springSystem.removeAllListeners();
    this.springSystem = undefined;
    this.spring.destroy();
    this.spring = undefined;
  }

  getScrollTop() {
    return this.refs.scrollbars.getScrollTop();
  }

  getScrollHeight() {
    return this.refs.scrollbars.getScrollHeight();
  }

  getHeight() {
    return this.refs.scrollbars.getHeight();
  }

  scrollTop(top) {
    const { scrollbars } = this.refs;
    const scrollTop = scrollbars.getScrollTop();
    const scrollHeight = scrollbars.getScrollHeight();
    const val = MathUtil.mapValueInRange(top, 0, scrollHeight, scrollHeight * 0.2, scrollHeight * 0.8);
    this.spring.setCurrentValue(scrollTop).setAtRest();
    this.spring.setEndValue(val);
  }

  handleSpringUpdate(spring) {
    const { scrollbars } = this.refs;
    const val = spring.getCurrentValue();
    scrollbars.scrollTop(val);
  }

  handleClick() {
    const { scrollbars } = this.refs;
    // const scrollHeight = scrollbars.getScrollHeight();
    scrollbars.scrollTop(this.section.current.offsetTop);
  }

  alertExample() {
    return (
      <UncontrolledAlert color={this.getColor()}>
        I am an alert and I can be dismissed!
      </UncontrolledAlert>
    );
  }

  getColor() {
    if(this.state.status==='submitting') {
      return 'info';
    } else if(this.state.status==='error') {
      return 'danger';
    }
    return 'success';
  }

  render() {
    return (
      <div>
        <FontAwesomeIcon icon={faCogs} onClick={this.toggle} />
        <Modal size="lg" isOpen={this.state.modal} toggle={this.toggle}>
          <ModalHeader toggle={this.toggle}>
            Configuration
          </ModalHeader>
          <ModalBody>
            {(true) ? this.alertExample() : null}
            <div className="row justify-content-center">
              <Scrollbars ref="scrollbars" style={{ width: 770, height: 450 } }>
              <div className={"mx-4"}>
              <UserInputForm
                defaultValues={this.state.defaultValues}
                onSubmit={(values) => {
                  Object.assign(values, { workflow: 'estimate' });
                  this.props.submitRequest(values);
                  this.setState({
                    defaultValues: values
                  });
                }}
                listOptions={this.options} 
                reference={this.section}
                target={this.state.target}/>
                </div>
              </Scrollbars>
            </div>
          </ModalBody>
          <ModalFooter className={"flex-container"}>
            <RemoteSubmitButton>>Apply</RemoteSubmitButton>
            <Button className={"mx-0"} size="sm" color="secondary" block onClick={this.toggle}>Close</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  workflow: state.workflow,
  errors: state.errors
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      ...userInputActions
    }, dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Configuration);