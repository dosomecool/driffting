import * as types from '../../../constants/actionTypes';

const init = {
  label: "approximationError",
  values: [{
    x: 0,
    y: 0.0
  }]
};

const visualizeErrorReducer = (state = init, action) => {
  switch (action.type) {
    case types.LOAD_NETWORK_ERROR_SUCCESS:
      return action.payload;
    default:
      return state;
  }
};

export default visualizeErrorReducer;
