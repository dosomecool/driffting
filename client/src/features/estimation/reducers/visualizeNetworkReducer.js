import * as types from '../../../constants/actionTypes';

const visualizeNetworkReducer = (state = {}, action) => {
  switch (action.type) {
    case types.LOAD_NETWORK_NODES_AND_EDGES_SUCCESS:
      return action.payload;
    default:
      return state;
  }
};

export default visualizeNetworkReducer;
