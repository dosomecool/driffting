import * as types from '../../../constants/actionTypes';

const visualizeInputReducer = (state = {}, action) => {
  switch (action.type) {
    case types.LOAD_2D_INPUT_SUCCESS:
      return action.payload;
    case types.LOAD_3D_INPUT_SUCCESS:
      return action.payload;
    default:
      return state;
  }
};

export default visualizeInputReducer;