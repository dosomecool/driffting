import * as types from '../../../constants/actionTypes';


const visualizeOutputReducer = (state = [], action) => {
  switch (action.type) {
    case types.LOAD_OUTPUT_SUCCESS:

      if(action.payload.label === "output") {

        state.forEach( (series, index) => {
          
          if(series.label === "output") {
            state.splice(index, 1);
          } 
        })
      }
      return [...state, action.payload];
    default:
      return state;
  }
};

export default visualizeOutputReducer;
