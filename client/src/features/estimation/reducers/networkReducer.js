import * as types from '../../../constants/drifftActionTypes';

const networkReducer = (state = {}, action) => {
  switch (action.type) {
    case types.SAVE_NETWORK_SUCCESS:
      return action.payload
    default:
      return state
  }
}

export default networkReducer;