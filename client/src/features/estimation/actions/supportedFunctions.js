import { POINT_CONVERSION_COMPRESSED } from "constants";

export function quadraticKilling(lambda, x) {
  return lambda * x * x; 
}

export function nextGaussion() {
    var x1, x2, rad, y1;
    do {
      x1 = 2 * Math.random() - 1;
      x2 = 2 * Math.random() - 1;
      rad = x1 * x1 + x2 * x2;
    } while(rad >= 1 || rad == 0);
    var c = Math.sqrt(-2 * Math.log(rad) / rad);
    return x1 * c;
}

export function diffusion(x0, mu, sigma) {
  const m = 100;
  const dt = 0.01;
  const path = 10;

  var data = [];
  var x = 0;

  for (var index = 0; index < path; index++) {
    x = x0;
    var series = [];
    series.push(
      {
        x: 0,
        y: x
      });

    for (var i = 1; i < m; i++) {
      x = x + mu * dt + sigma * Math.sqrt(dt) * nextGaussion();
      series.push(
        {
          x: i * dt,
          y: x
        });
    }
    data.push(
      { label: index.toString(), values: series }
    );
  }
  return data;
}

export function killing(diffusion, kill) {
  var data = [];
  diffusion.forEach((item) => {

    var list = item.values.map(value => {
      return {
        x: value.x,
        y: kill(value.y)
      }
    })

    data.push(
      {
        label: item.label,
        values: list
      }
    );
  });
  return data;
}