import { push } from 'react-router-redux';
import RestApi from '../../../apis/restApi';
import { reset, change, arrayPush, autofill } from 'redux-form';
import { changePageSuccess } from '../../../actions/drifftActions';
import * as types from '../../../constants/actionTypes';

export function beginAjaxCall() {
  return { type: types.BEGIN_AJAX_CALL };
}

export function ajaxCallError() {
  return { type: types.AJAX_CALL_ERROR };
}

export function loadInputValuesSuccess(values) {
  return { type: types.LOAD_INPUT_VALUES_SUCCESS, payload: values };
}

export function handleSubmitSuccess(values) {
  return { type: types.HANDLE_SUBMIT_SUCCESS, values };
}

export function saveInputErrorsSuccess(errors) {
  return { type: types.SAVE_INPUT_ERRORS_SUCCESS, payload: errors };
}

export function clearInputErrorsSuccess() {
  return { type: types.CLEAR_INPUT_ERRORS_SUCCESS, payload: []};
}

export function saveInputValues(values) {
  return (dispatch) => {
    dispatch(loadInputValuesSuccess(values));
  };
}

export function saveInputErrors(errors) {
  return (dispatch) => {
    dispatch(saveInputErrorsSuccess(errors));
  };
}

export function loadInputValues(values) {
  return (dispatch) => {
    Object.keys(values).forEach((key) => {
      dispatch(change('user-input-form', key, values[key]));
    });
  };
}

export function clearInputErrors() {
  return (dispatch) => {
      dispatch(clearInputErrorsSuccess());
  };
}

// TODO
// var p = new Promise(function(resolve, reject) {  
  
//   // do something here
//   const x = 5;

//   if (x > 5) {
//      resolve(/* value */);  // fulfilled successfully
//   }
//   else {
//      reject(/* reason */);  // error, rejected
//   }
// });

// dispatch(push('/results'));
// dispatch(changePageSuccess('/results'));

// https://www.datchley.name/es6-promises/

export function submitRequest(values) {
  console.log('submit request is being called: ', values);
  return (dispatch) => {
    dispatch(beginAjaxCall());
    return RestApi.submitUserInput(values)
      .then((errors) => {
        if (Array.isArray(errors) || errors.length) {
          dispatch(saveInputErrors(errors));
        } else {
          dispatch(saveInputValues(values));
          // dispatch(push('/results'));
        }
      })
      .catch((error) => {
        console.log('ajax error: ', error);
        dispatch(ajaxCallError());
      });
  };
}

export function cancelRequest(values) {
  console.log('cancel request!!');
  return (dispatch) => {
    dispatch(beginAjaxCall());
    return RestApi.userAction(values)
      .then((response) => {
          console.log("Erros: ", response);
      })
      .catch((error) => {
        console.log('ajax error: ', error);
        dispatch(ajaxCallError());
      });
  };
}

export function downloadRequest(values) {
  return (dispatch) => {
    dispatch(beginAjaxCall());
    return RestApi.userAction(values)
      .then((response) => {
          return response; 
      })
      .catch((error) => {
        console.log('ajax error: ', error);
        dispatch(ajaxCallError());
      });
  };
}

export function changePage(path) {
  return (dispatch) => {
    dispatch(changePageSuccess(path));
  };
}
