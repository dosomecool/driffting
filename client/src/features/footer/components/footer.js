import React from 'react';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faReact from '@fortawesome/fontawesome-free-brands/faReact';

const Footer = () => (
  <div className="row align-items-center justify-content-center">
    <p>
    <FontAwesomeIcon icon={faReact}/>
      Built with React JS and love
    </p>
  </div>
);

export default Footer;
