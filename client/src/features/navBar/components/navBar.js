import React from 'react';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

const addModel = '/estimation'
const homePage = '/';
const aboutPage = '/about-us';

// tmp
const estimation = '/estimation';

const NavBar = (props) => (  
  <div>
    <nav className="navbar navbar-expand-lg navbar-light" style={{ backgroundColor: '#ffffff'}}>
      <a className="navbar-brand font-weight-light">DRIFFT {'   '} | </a>
      <form className="form-inline">
        <button className="btn btn-sm btn-outline-secondary" type="button" onClick={() => props.changePage(addModel)}>Add Model</button>
      </form>
      <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div className="navbar-nav ml-auto">
          <a className="nav-item nav-link" onClick={() => props.changePage(homePage)}>Home </a>
          <a className="nav-item nav-link">Tutorials</a>
          <a className="nav-item nav-link" onClick={() => props.changePage(aboutPage)}>About</a>
        </div>
      </div>
    </nav>
  </div>
);

const mapStateToProps = state => ({
  // prop: state.<name>
});

const mapDispatchToProps = dispatch => 
bindActionCreators(
  { changePage: (page) => push(page) }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(NavBar);
