import React from 'react';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

export default class DropdownList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      dropdownOpen: false,
      items: [],
      selected: "Select"
    };

    this.toggle = this.toggle.bind(this);
    this.getItem = this.getItem.bind(this);
    this.changeValue = this.changeValue.bind(this);
  }

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  changeValue(e) {
    this.setState({
      selected: this.props.onStyleChange(e)
    });
  }

  getItem(item) {
    return (
      <DropdownItem key={item} onClick={this.changeValue}>{item}</DropdownItem>
    );
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    return {
      // selected: nextProps.selected, 
      items: nextProps.items
    };
  }

  render() {
    const itemList = [];
    this.state.items.forEach( item => {
      itemList.push(this.getItem(item));
    })

    return (
      <Dropdown 
        isOpen={this.state.dropdownOpen} toggle={this.toggle}>
        <DropdownToggle caret size="sm" outline block color="secondary">
          {this.state.selected}
        </DropdownToggle>
        <DropdownMenu>
          {itemList}
        </DropdownMenu>
      </Dropdown>
    );
  }
}