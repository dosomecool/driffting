import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import { Collapse, Button, CardBody, Card, DropdownItem } from 'reactstrap';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCaretUp } from '@fortawesome/free-solid-svg-icons';

library.add(faCaretUp);

export default class CollapsePanel extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = { collapse: false };
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  render() {
    return (
      <div>
        <Collapse isOpen={this.state.collapse}>
          <Button block color={"secondary"} size="sm" onClick={this.toggle}>
            {this.props.buttonName}<FontAwesomeIcon icon={faCaretUp} />
          </Button>
          <DropdownItem divider />
          {this.props.component}
        </Collapse>
      </div>
    );
  }
}