import React from 'react';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ListGroup, ListGroupItem, ListGroupItemText, Badge } from 'reactstrap';
import { Row, Col } from 'reactstrap';
import { Scrollbars } from 'react-custom-scrollbars';

class Status extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      status: "success",
      description: "App init",
      information: "Configure the workflow to get started.",
      history: []
    };

    // this.historyList = this.historyList.bind(this);
  }

  // historyList(item, index) {
  //   return (
  //     <ListGroupItem key={index} className="justify-content-between">{item.description}<Badge color={item.status}>{item.status}</Badge>
  //       <ListGroupItemText>
  //         {item.information}
  //     </ListGroupItemText>
  //     </ListGroupItem>
  //   );
  // }

  static getDerivedStateFromProps(nextProps, prevState) {

    if (Object.keys(nextProps.status).length !== 0) {
      return {
        status: nextProps.status.status,
        description: nextProps.status.description,
        history: [...prevState.history,
        {
          status: prevState.status,
          description: prevState.description,
          information: prevState.information
        }]
      }
    }
    return null;
  }

  render() {
    // const statusList = [];
    // this.state.history.forEach((item, index) => {
    //   statusList.push(this.historyList(item, index));
    // })

    return (
      
      <div>
        <ListGroupItem key={this.state.history.length} className="justify-content-between, list-group-item-noborder">
          <strong> {this.state.description}<Badge color={this.state.status}>{this.state.status}</Badge></strong>
        </ListGroupItem>
        {/* <div className="bg-light" ref={this.widthRef}>
          <ListGroup >
            {statusList}
          </ListGroup>
        </div> */}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  status: state.status
});

const mapDispatchToProps = (dispatch) => bindActionCreators(
  { 
    changePage: (page) => push(page) 
  }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(Status);
