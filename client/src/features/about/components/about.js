import React from 'react';
import { connect } from 'react-redux';

const About = (props) => {
  const {greeting} = props;
  return (
    <div className="row align-items-center justify-content-center">
      <p>This is the about page.</p>
      <br></br>
      <p>You should see a greeting: { greeting }</p>
    </div>
  );
};

const mapStateToProps = (state) => { 
  return { greeting: state.initialState.greeting };
};

export default connect(mapStateToProps)(About);

