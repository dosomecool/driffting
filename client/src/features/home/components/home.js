import React from 'react';
import { Card, Button, CardImg, CardTitle, CardText, CardDeck,
 CardSubtitle, CardBody } from 'reactstrap';
import { Jumbotron, Container } from 'reactstrap';
import logo from '../../../drifft.svg';
import diffusion from '../../../resources/diffusion.png';
import killing from '../../../resources/killing.png';
import survival from '../../../resources/survival.png';
import density from '../../../resources/density.png';
import hazard from '../../../resources/hazard.png';
import conditioned from '../../../resources/conditioned.png';
import survivalPde from '../../../resources/survival-pde.png';
import deepNeuralNet from '../../../resources/deep-neural-net.png';

import './home.css';

const Home = (props) => {
  return (
    <div>
        <Jumbotron fluid className="justify-content-center">
          <Container >
            {/* <br/> */}
            {/* className="img-fluid mx-auto d-block"  */}
            <img className={"logo"} src={logo}/>
            {/* <br/> */}
            <hr className="my-2" />
            {/* <br/> */}
            <h1 className="display-6">Stochastic Differential Equations and Machine Learning</h1>
            <p className="lead">Drifft application is a statistical tool for simulating stochastic differential equations with focus on survival analysis.</p>
          </Container>
        </Jumbotron>
      <Container>
    <CardDeck>
      <Card>
        <CardImg top width="100%" src={diffusion} alt="Card image cap" />
        <CardBody>
          <CardTitle>Diffusion Model</CardTitle>
          <CardSubtitle>Coming Soon</CardSubtitle>
          <CardText>Simulate any diffusion model.</CardText>
          <Button disabled={ true }>Learn more</Button>
        </CardBody>
      </Card>
      <Card>
        <CardImg top width="100%" src={killing} alt="Card image cap" />
        <CardBody>
          <CardTitle>Killing Function</CardTitle>
          <CardSubtitle>Coming Soon</CardSubtitle>
          <CardText>Calculate the diffusion Killing function.</CardText>
          <Button disabled={ true }>Learn more</Button>
        </CardBody>
      </Card>
      <Card>
        <CardImg top width="100%" src={survival} alt="Card image cap" />
        <CardBody>
          <CardTitle>Survival Function</CardTitle>
          <CardSubtitle>Coming Soon</CardSubtitle>
          <CardText>Derive the diffusion Survival from the killing function.</CardText>
          <Button disabled={ true }>Learn more</Button>
        </CardBody>
      </Card>
      <Card>
        <CardImg top width="100%" src={density} alt="Card image cap" />
        <CardBody>
          <CardTitle>Density Function</CardTitle>
          <CardSubtitle>Coming Soon</CardSubtitle>
          <CardText>Measure the density distribution of the killed diffusion.</CardText>
          <Button disabled={ true }>Learn more</Button>
        </CardBody>
      </Card>
    </CardDeck>
        <CardDeck>
        <Card>
          <CardImg top width="100%" src={hazard} alt="Card image cap" />
          <CardBody>
            <CardTitle>Hazard Function</CardTitle>
            <CardSubtitle>Coming Soon</CardSubtitle>
            <CardText>Measure the Hazard rate of the killed diffusion.</CardText>
            <Button disabled={ true }>Learn more</Button>
          </CardBody>
        </Card>
        <Card>
          <CardImg top width="100%" src={conditioned} alt="Card image cap" />
          <CardBody>
            <CardTitle>Conditioned Diffusion</CardTitle>
            <CardSubtitle>Coming Soon</CardSubtitle>
            <CardText>Simulate the Conditioned diffusion given the killing function.</CardText>
            <Button disabled={ true }>Learn more</Button>
          </CardBody>
        </Card>
        <Card>
          <CardImg top width="100%" src={survivalPde} alt="Card image cap" />
          <CardBody>
            <CardTitle>Survival PDE</CardTitle>
            <CardSubtitle>Coming Soon</CardSubtitle>
            <CardText>Solve the survival partial differential equation for each diffusion initial state.</CardText>
            <Button disabled={ true }>Learn more</Button>
          </CardBody>
        </Card>
        <Card>
          <CardImg top width="100%" src={deepNeuralNet} alt="Card image cap" />
          <CardBody>
            <CardTitle>Neural Networks</CardTitle>
            <CardSubtitle>Coming Soon</CardSubtitle>
            <CardText>Train a Neural Network to solve various regression models.</CardText>
            <Button disabled={ true }>Learn more</Button>
          </CardBody>
        </Card>
      </CardDeck>
      </Container>
    </div>
  );
};

export default Home;
