import React from 'react';
import { push as Menu } from 'react-burger-menu'

class BurgerMenu extends React.Component {
  constructor(props) {
    super(props);
    this.styles = {
      bmBurgerButton: {
      position: 'fixed',
      width: '36px',
      height: '30px',
      left: '25px',
      top: '15px'
      },
      bmBurgerBars: {
        background: '#373a47'
      },
      bmCrossButton: {
        height: '24px',
        width: '24px'
      },
      bmCross: {
        background: '#000000'
      },
      bmMenu: {
        background: '#ffffff',
        padding: '2.5em 0.0em 0',
        fontSize: '1.15em'
      },
      bmMorphShape: {
        fill: '#000000'
      },
      bmItemList: {
        color: '#000000',
        padding: '0.0em'
      },
      bmOverlay: {
        background: 'rgba(0, 0, 0, 0.3)'
      }
    }
  }

  showSettings (event) {
    event.preventDefault();
  }
  
  render () {
    return (
      <Menu pageWrapId={'page-wrap'} isOpen={ false } styles={ this.styles }>
        <a className="list-group-item list-group-item-action">Function 0</a>
        <a className="list-group-item list-group-item-action">Function 1</a>
        <a className="list-group-item list-group-item-action">Function 2</a>
        <a className="list-group-item list-group-item-action">Function 3</a>
        <a onClick={ this.showSettings } className="list-group-item list-group-item-action">Settings</a>
      </Menu>
    );
  }
}

export default BurgerMenu;
