package com.drifft.domain;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Graph3DData {

	private String label;
	private List<Graph3DValue> values;
	
    public Graph3DData() {
    }
    
    public Graph3DData(String label, List<Graph3DValue> values) {
    	this.label = label;
    	this.values = values;
    }

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public List<Graph3DValue> getValues() {
		return values;
	}

	public void setValues(List<Graph3DValue> values) {
		this.values = values;
	}
    
	@Override 
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("label: ").append(label);
		sb.append("\nvalues: ").append(values);
		
		String jsonStr = null; 
		ObjectMapper mapper = new ObjectMapper();
		
        try {
            jsonStr = mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
		return jsonStr;
	}
}
