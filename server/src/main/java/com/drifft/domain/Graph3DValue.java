package com.drifft.domain;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Graph3DValue {

	private double x;
	private double y;
	private double z;
	
	public Graph3DValue() {
	}
	
	public Graph3DValue(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
	
	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}

	@Override 
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("x: ").append(x);
		sb.append("\ny: ").append(y);
		sb.append("\nz: ").append(z);
		
		String jsonStr = null; 
		ObjectMapper mapper = new ObjectMapper();
		
        try {
            jsonStr = mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
		return jsonStr;
	}
}
