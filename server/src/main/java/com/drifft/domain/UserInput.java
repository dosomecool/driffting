package com.drifft.domain;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class UserInput implements Cloneable {

	private UUID id;
	private String workflow;
	
	private String drift;
	private String diffusion;
	private String initialState;
	
	private double t0;
	private double tf;
	private double dt;
	
	private double x0;
	private double xf;
	private double dx;
	
	private int paths; 
	private long seed;
	
	private String killing; 
	
	private String input;
	private List<HiddenLayer> hiddenLayers;
	public String output;
	public String outputActivation;
	
	private String costFunction;
	private String optimization;
	private int epochs; 
	private int batch; 
	
	private double variance;
	private boolean shuffle;
	private long networkSeed;
	
	public UserInput() {		
	}

	public UserInput(String workflow, String drift, String diffusion, String initialState, 
			double t0, double tf, double dt, double x0, double xf, double dx, int paths, 
			long seed, String killing, String input, List<HiddenLayer> hiddenLayers, String output, 
			String outputActivation, String costFunction, String optimization, int epochs, int batch, 
			double variance, boolean shuffle, long networkSeed) {
		this.drift = drift;
		this.diffusion = diffusion;
		this.initialState = initialState;
		this.t0 = t0;
		this.tf = tf;
		this.dt = dt;
		this.x0 = x0;
		this.xf = xf;
		this.dx = dx;
		this.paths = paths;
		this.seed = seed;
		this.killing = killing;
		this.input = input;
		this.hiddenLayers = hiddenLayers;
		this.output = output;
		this.outputActivation = outputActivation;
		this.costFunction = costFunction; 
		this.optimization = optimization;
		this.epochs = epochs; 
		this.batch = batch;
		this.variance = variance;
		this.shuffle = shuffle;
		this.networkSeed = networkSeed;
	}
	
	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getWorkflow() {
		return workflow;
	}

	public void setWorkflow(String workflow) {
		this.workflow = workflow;
	}

	public String getDrift() {
		return drift;
	}

	public void setDrift(String drift) {
		this.drift = drift;
	}

	public String getDiffusion() {
		return diffusion;
	}

	public void setDiffusion(String diffusion) {
		this.diffusion = diffusion;
	}

	public String getInitialState() {
		return initialState;
	}

	public void setInitialState(String initialState) {
		this.initialState = initialState;
	}

	public double getT0() {
		return t0;
	}

	public void setT0(double t0) {
		this.t0 = t0;
	}

	public double getTf() {
		return tf;
	}

	public void setTf(double tf) {
		this.tf = tf;
	}

	public double getDt() {
		return dt;
	}

	public void setDt(double dt) {
		this.dt = dt;
	}
	
	public double getX0() {
		return x0;
	}

	public void setX0(double x0) {
		this.x0 = x0;
	}

	public double getXf() {
		return xf;
	}

	public void setXf(double xf) {
		this.xf = xf;
	}

	public double getDx() {
		return dx;
	}

	public void setDx(double dx) {
		this.dx = dx;
	}

	public int getPaths() {
		return paths;
	}

	public void setPaths(int paths) {
		this.paths = paths;
	}

	public long getSeed() {
		return seed;
	}

	public void setSeed(long seed) {
		this.seed = seed;
	}

	public String getKilling() {
		return killing;
	}

	public void setKilling(String killing) {
		this.killing = killing;
	}
	
	public int getM() {
		Double tempM = new Double( ( (this.tf - this.t0) / this.dt) );
	    return tempM.intValue();
	}
	
	public int getN() {
		Double tempN = new Double( ( (this.xf - this.x0) / this.dx) );
	    return tempN.intValue();
	}
	

	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = input;
	}
	
	public List<HiddenLayer> getHiddenLayers() {
		return hiddenLayers;
	}

	public void setHiddenLayers(List<HiddenLayer> hiddenLayers) {
		this.hiddenLayers = hiddenLayers;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}
	
	public String getOutputActivation() {
		return outputActivation;
	}

	public void setOutputActivation(String outputActivation) {
		this.outputActivation = outputActivation;
	}
	
	public String getCostFunction() {
		return costFunction;
	}

	public void setCostFunction(String costFunction) {
		this.costFunction = costFunction;
	}

	public String getOptimization() {
		return optimization;
	}

	public void setOptimization(String optimization) {
		this.optimization = optimization;
	}
	
	public int getEpochs() {
		return epochs;
	}

	public void setEpochs(int epochs) {
		this.epochs = epochs;
	}

	public int getBatch() {
		return batch;
	}

	public void setBatch(int batch) {
		this.batch = batch;
	}
	
	public double getVariance() {
		return variance;
	}

	public void setVariance(double variance) {
		this.variance = variance;
	}

	public boolean isShuffle() {
		return shuffle;
	}

	public void setShuffle(boolean shuffle) {
		this.shuffle = shuffle;
	}

	public long getNetworkSeed() {
		return networkSeed;
	}

	public void setNetworkSeed(long networkSeed) {
		this.networkSeed = networkSeed;
	}
	
	@Override 
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("id: ").append(id);
		sb.append("\nworkflow: ").append(workflow);
		sb.append("\ndrift: ").append(drift);
		sb.append("\ndiffusion: ").append(diffusion);
		sb.append("\ninitialState: ").append(initialState);
		sb.append("\nt0: ").append(t0);
		sb.append("\ntf: ").append(tf);
		sb.append("\ndt: ").append(dt);
		sb.append("\nx0: ").append(x0);
		sb.append("\nxf: ").append(xf);
		sb.append("\ndx: ").append(dx);
		sb.append("\npaths: ").append(paths);
		sb.append("\nseed: ").append(seed);
		sb.append("\nkilling: ").append(killing);	
		sb.append("\ninput: ").append(input);
		sb.append("\nhiddenLayers: ").append(hiddenLayers);
		sb.append("\noutput: ").append(output);
		sb.append("\noutputActivation: ").append(outputActivation);
		sb.append("\ncostFunction: ").append(costFunction);
		sb.append("\noptimization: ").append(optimization);
		sb.append("\nepochs: ").append(epochs);
		sb.append("\nbatch: ").append(batch);
		sb.append("\nvariance: ").append(variance);
		sb.append("\nshuffle: ").append(shuffle);
		sb.append("\nnetworkSeed: ").append(networkSeed);
		
		String jsonStr = null; 
		ObjectMapper mapper = new ObjectMapper();
		
        try {
            jsonStr = mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
		return jsonStr;
	}
	
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	static class HiddenLayer {
		
		private int size; 
		private String activation;
		
		public HiddenLayer() {
		}
		
		public HiddenLayer(int size, String activation) {
			this.size = size;
			this.activation = activation;
		}
		
		public int getSize() {
			return size;
		}

		public void setSize(int size) {
			this.size = size;
		}

		public String getActivation() {
			return activation;
		}

		public void setActivation(String activation) {
			this.activation = activation;
		}

		@Override 
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append("size: ").append(size);
			sb.append("\nactivation: ").append(activation);
			
			String jsonStr = null; 
			ObjectMapper mapper = new ObjectMapper();
			
	        try {
	            jsonStr = mapper.writeValueAsString(this);
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
			return jsonStr;
		}
	}
}
