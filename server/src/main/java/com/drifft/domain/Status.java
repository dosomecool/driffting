package com.drifft.domain;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Status {

	private String status = ""; 
	private String description = "";
	private String information = ""; 
	
	public Status() {
	}
	
	public Status(String status, String description) {
		this.status = status; 
		this.description = description;
	}
	
	public Status(String status, String description, String information) {
		this.status = status; 
		this.description = description;
		this.information = information;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	@Override 
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("status: ").append(status);
		sb.append("\ndescription: ").append(description);
		sb.append("\ninformation: ").append(information);

		String jsonStr = null; 
		ObjectMapper mapper = new ObjectMapper();
		
        try {
            jsonStr = mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
		return jsonStr;
	}
}
