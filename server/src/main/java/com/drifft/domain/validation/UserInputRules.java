package com.drifft.domain.validation;

public interface UserInputRules {
	
	public String validate(String field);
}
