package com.drifft.domain.validation;

import java.util.regex.Pattern;

public class EquationValidationRule implements UserInputRules {

	 private static final String EQUATION_PATTERN = 
	 "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
	 + "+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	 
	 private final Pattern pattern = Pattern.compile(EQUATION_PATTERN);

	 @Override
	 public String validate(String field) {
		 
		 String error = null;
		 
		 if ( !pattern.matcher(field).matches()) {
			 error = "Field contains unrecognized variables! Only variables x and t are allowed.";
			 throw new IllegalArgumentException("Field contains unrecognized variables! Only variables x and t are allowed.");
		 }
		 return error; 
	 }
}
