package com.drifft.domain.validation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.drifft.domain.UserInput;
import com.drifft.service.dynamic.DynamicModel;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Validator {

	private UserInput userInput;
	private List<Error> errors = new ArrayList<>();
	private boolean failed = false;
	
	public Validator(UserInput userInput) {
		this.userInput = userInput;
	}
	
	public Validator check() {
		
		UserInput test = null;
		DynamicModel dataGenerator;
		
		try {
			test = (UserInput) userInput.clone();
			test.setT0(0);
			test.setTf(0.1);
			test.setDt(0.01);
			test.setPaths(1);
			dataGenerator = new DynamicModel(test);
			
			Double[] time = dataGenerator.getTime();
			Double[][] wiener = dataGenerator.getWiener();
			Double[][] diffusion = dataGenerator.getDiffusion(time, wiener);

//			System.out.println(Arrays.deepToString(diffusion));

		} catch (CloneNotSupportedException e) {
			System.out.println("errors: " + e.getMessage());
			failed = true;
		} 

		// others
		
		return this;
	}
	
	public boolean isFailed() {
		return failed;
	}
	
	public String getErrors() {
		
		String jsonStr = null; 
		ObjectMapper mapper = new ObjectMapper();
		
        try {
            jsonStr = mapper.writeValueAsString(errors);
        } catch (IOException e) {
            e.printStackTrace();
        }
		return jsonStr;
	}
	
	private Map<String, String> getEquations() {
		Map<String, String> equations = new HashMap<>();
		equations.put("drift", userInput.getDrift());
		equations.put("diffusion", userInput.getDiffusion());
		equations.put("killing", userInput.getKilling());
		return equations;
	}
	
	private Map<String, Double> getDoubles() {
		Map<String, Double> doubles = new HashMap<>();
		doubles.put("t0", userInput.getT0());
		doubles.put("tf", userInput.getTf());
		doubles.put("dt", userInput.getDt());
		return doubles;
	}
}
