package com.drifft.domain;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Graph2DData {

	private String label;
	private List<Graph2DValue> values;
	
    public Graph2DData() {
    }
    
    public Graph2DData(String label, List<Graph2DValue> values) {
    	this.label = label;
    	this.values = values;
    }

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public List<Graph2DValue> getValues() {
		return values;
	}

	public void setValues(List<Graph2DValue> values) {
		this.values = values;
	}
    
	@Override 
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("label: ").append(label);
		sb.append("\nvalues: ").append(values);
		
		String jsonStr = null; 
		ObjectMapper mapper = new ObjectMapper();
		
        try {
            jsonStr = mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
		return jsonStr;
	}
}
