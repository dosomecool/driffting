package com.drifft.domain;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Graph2DValue {

	private double x;
	private double y;
	
	public Graph2DValue() {
	}
	
	public Graph2DValue(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
	
	@Override 
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("x: ").append(x);
		sb.append("\ny: ").append(y);
		
		String jsonStr = null; 
		ObjectMapper mapper = new ObjectMapper();
		
        try {
            jsonStr = mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
		return jsonStr;
	}
}
