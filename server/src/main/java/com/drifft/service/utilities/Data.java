package com.drifft.service.utilities;

import java.util.UUID;

public class Data<T> {
	
	private UUID uuid;
	private String name; 
	private final Class<T> type;
	private Object data; 
	
	public Data(UUID uuid, String name, Class<T> type, T instance) {
		this.uuid = uuid;
		this.name = name;
		this.type = type; 
		this.data = instance.getClass().cast(instance);
	}
		
	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Class<T> getType() {
		return type;
	}

	public T cast() {
		return data == null ? null : type.cast(data);
	}
}
