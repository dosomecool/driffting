package com.drifft.service.utilities;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.AnchorPane;
import javafx.scene.transform.Transform;

public class ImageUtility {
	
	private static final AnchorPane root = new AnchorPane();
	private static final Scene scene = new Scene(root);
	
//	https://<region>.amazonaws.com/<bucket-name>/<key>
	
	public static boolean savePngToFile(Parent node, File file) {
		
		boolean success = true;
	
		scene.setRoot(node);
		WritableImage image = node.snapshot(new SnapshotParameters(), null);
		
		try {
			ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
			
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
			success = false;
		}
		return success;
	}
	
	public static boolean savePngToFile(Parent node, File file, 
			int width, int height, int scaleX, int scaleY) {
		
		boolean success = true;
	
		scene.setRoot(node);
		WritableImage image = new WritableImage(width, height);
		SnapshotParameters param = new SnapshotParameters();
		
		param.setTransform(Transform.scale(scaleX, scaleY));
	    node.snapshot(param, image);
		
		try {
			ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
			
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
			success = false;
		}
		return success;
	}
	
	public static Object[] savePngAsStream(Parent node) {

        boolean success = true;

        scene.setRoot(node);
		WritableImage image = node.snapshot(new SnapshotParameters(), null);

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		InputStream inputStream = null; 

		try {
			ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", outputStream); 
			inputStream = new ByteArrayInputStream(outputStream.toByteArray());

		} catch (IOException ex) {
			System.out.println(ex.getMessage());
			success = false;
			
		} finally {
			
			try {
				outputStream.close();
				inputStream.close();
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return new Object[] {success, inputStream};
	}
	
	public static Object[] savePngAsStream(Parent node, int width, int height, int scaleX, int scaleY) {

        boolean success = true;

        scene.setRoot(node);		    
		WritableImage image = node.snapshot(new SnapshotParameters(), null);

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		InputStream inputStream = null; 

		try {
			ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", outputStream); 
			inputStream = new ByteArrayInputStream(outputStream.toByteArray());

		} catch (IOException ex) {
			System.out.println(ex.getMessage());
			success = false;
			
		} finally {
			
			try {
				outputStream.close();
				inputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return new Object[] {success, inputStream};
	}
}
