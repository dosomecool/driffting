package com.drifft.service.utilities;

import java.util.concurrent.Callable;

import net.openhft.compiler.CompilerUtils;

public class FTCSParameters {

//	private Configuration config;

	private String _m;
	private String _n;
	private String _mu;
	private String _sigma;
	private String x_min;
	private String x_max;
	private String t_min;
	private String t_max;

	private String _kill;

	private String ftcsJavaCode;

	// CONSTRUCTOR
	public FTCSParameters() {

//		config = Configuration.getInstance();

		getValues();
		setFTCSJavaCode();
		loadDynamicParameters();
		getDynamicArrays();
	}

	public void getValues(){

//		_m        = config.getConfig("temporal_intervals",        "m");
//		_n        = config.getConfig("spatial_intervals",         "n");
//		x_min     = config.getConfig("limits",                 "xmin");
//		x_max     = config.getConfig("limits",                 "xmax");
//		t_min     = config.getConfig("limits",                 "tmin");
//		t_max     = config.getConfig("limits",                 "tmax");
//		_mu       = config.getConfig("drift",                    "mu");
//		_sigma    = config.getConfig("diffusion",             "sigma");
//		_kill     = config.getConfig("kill",               "function");
	}

	// DYNAMIC FTCS CLASS
	String dynamicFTCSclass = "application.prototypes.DynamicFTCSClass";

	public void setFTCSJavaCode(){

		ftcsJavaCode = "package application.prototypes; \n\n"

                 + "import static java.lang.Math.*; \n"
                 + "import java.util.concurrent.Callable; \n\n"

				 + "public class DynamicFTCSClass implements Callable<Object[]> { \n\n"

				 + "private double t; \n"
				 + "private double x; \n\n"

				 + "private int n; \n"
				 + "private int m; \n\n"

			     + "private double k; \n"
			     + "private double h; \n\n"

				 + "private double[] xIndex; \n"
				 + "private double[] tIndex; \n\n"

				 + "private double[][] mu; \n"
				 + "private double[][] sigma; \n"
				 + "private double[][] kill; \n\n"

				 + "private double xmin; \n"
				 + "private double xmax; \n\n"

				 + "private double tmin; \n"
				 + "private double tmax; \n\n"

				 // Constructor
				 + "public DynamicFTCSClass(){ \n\n"

				 // INSERTED VALUE - *** n, m ***
				 + "n = " + _n + "; \n"
				 + "m = " + _m + "; \n\n"

				 + "mu = new double[n+1][m+1]; \n"
				 + "sigma = new double[n+1][m+1]; \n\n"

				 + "xIndex = new double[n+1]; \n"
				 + "tIndex = new double[m+1]; \n\n"

				 + "kill = new double[n+1][m+1]; \n\n"

				 // INSERTED VALUE - *** xmin, xmax ***
				 + "xmin = " + x_min + "; \n"
				 + "xmax = " + x_max + "; \n\n"

				 // INSERTED VALUE - *** tmin, tmax ***
				 + "tmin = " + t_min + "; \n"
				 + "tmax = " + t_max + "; \n\n"

				 + "h = ( tmax-tmin ) / m ; \n"
				 + "k = ( xmax-xmin ) / n ; \n\n"

				 + "buildIndex(); \n"
				 + "setMu(); \n"
				 + "setSigma(); \n"
				 + "setKilling(); \n"

				 + "} \n\n"

				 + "public void buildIndex() \n"
				 + "{ \n"
				 + "for(int j=0; j<=m; j++) \n"
				 + "{ \n"
				 + "tIndex[j] = tmin + j * h; \n"
				 + "} \n\n"
				 + "for(int i=0; i<=n; i++) \n"
				 + "{ \n"
				 + "xIndex[i] = xmin + i * k; \n"
				 + "} \n"
				 + "} \n\n"

				 + "public void setMu() \n"
				 + "{ \n"
				 + "for(int j=0; j<=m; j++) \n"
				 + "{ \n"
				 + "for(int i=0; i<=n; i++) \n"
				 + "{ \n"

				 + "t = tIndex[j]; \n"
				 + "x = xIndex[i]; \n"
				 // INSERTED VALUE - *** mu ***
				 + "mu[i][j] = " + _mu + "; \n"

				 + "} \n"
				 + "} \n"
				 + "} \n\n"

				 + "public void setSigma() \n"
				 + "{ \n"
				 + "for(int j=0; j<=m; j++) \n"
				 + "{ \n"
				 + "for(int i=0; i<=n; i++) \n"
				 + "{ \n"

				 + "t = tIndex[j]; \n"
				 + "x = xIndex[i]; \n"
				 // INSERTED VALUE - *** sigma ***
				 + "sigma[i][j] = " + _sigma + "; \n"

				 + "} \n"
				 + "} \n"
				 + "} \n\n"

				 + "public void setKilling() \n"
				 + "{ \n"

				 + "for(int j=0; j<=m; j++) \n"
				 + "{ \n"
				 + "for(int i=0; i<=n; i++) \n"
				 + "{ \n"

				 + "t = tIndex[j]; \n"
				 + "x = xIndex[i]; \n"
				 // INSERTED VALUE - *** kill ***
				 + "kill[i][j] = " + _kill + "; \n"

				 + "} \n"
				 + "} \n"
				 + "} \n\n"

				 + "@Override \n"
				 + "public Object[] call(){ \n"
				 + "return new Object[]{m, n, h, k, mu, sigma, kill, tIndex, xIndex, tmin, tmax, xmin, xmax}; \n"
				 + "} \n"

				 + "} \n\n";
	}

	@SuppressWarnings("rawtypes")
	private Class ftcsClass;
	private Callable<Object[]> callFTCS;
	private ClassLoader ftcsClassloader;
	private Object[] ftcsArray;

	@SuppressWarnings("unchecked")
	public void loadDynamicParameters(){

		ftcsClassloader = new ClassLoader() {
		};

		try {
			ftcsClass = CompilerUtils.CACHED_COMPILER.loadFromJava(ftcsClassloader, dynamicFTCSclass, ftcsJavaCode);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			callFTCS = (Callable<Object[]>) ftcsClass.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void getDynamicArrays(){

		try {
			ftcsArray = (Object[]) callFTCS.call();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		};
	}

	// GETTERS // m, n, h, k, mu, sigma, kill, tIndex, xIndex, tmin, tmax, xmin, xmax

	public int getM(){
		return (int) ftcsArray[0];
	}

	public int getN(){
		return (int) ftcsArray[1];
	}

	public double getH(){
		return (double) ftcsArray[2];
	}

	public double getK(){
		return (double) ftcsArray[3];
	}

	public double[][] getDrift(){
		return (double[][]) ftcsArray[4];
	}

	public double[][] getDiffusion(){
		return (double[][]) ftcsArray[5];
	}

	public double[][] getKilling(){
		return (double[][]) ftcsArray[6];
	}

	public double[] getT(){
		return (double[]) ftcsArray[7];
	}

	public double[] getX(){
		return (double[]) ftcsArray[8];
	}

	public double getMinT(){
		return (double) ftcsArray[9];
	}

	public double getMaxT(){
		return (double) ftcsArray[10];
	}

	public double getMinX(){
		return (double) ftcsArray[11];
	}

	public double getMaxX(){
		return (double) ftcsArray[12];
	}
}
