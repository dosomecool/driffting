package com.drifft.service.utilities;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.drifft.constant.Constants;

// Source: https://javatutorial.net/java-s3-example

public class S3BucketUtility {

	private static final String SUFFIX = "/";
	
	// Credentials object identifying user for authentication
	// User must have AWSConnector and AmazonS3FullAccess 
	private static final BasicAWSCredentials credentials = 
			new BasicAWSCredentials(Constants.ACCESS_KEY_ID, Constants.SECRET_ACESS_KEY);
	
	// Create a client connection based on credentials
	private static final AmazonS3 s3client = 
			AmazonS3ClientBuilder.standard()
                                 .withRegion(Regions.US_EAST_1)
                                 .withCredentials(new AWSStaticCredentialsProvider(credentials))
                                 .build();

	public List<String> getBucketNames() {
		List<String> bucketNames = new ArrayList<>();
		
		for (Bucket bucket : s3client.listBuckets()) {
			bucketNames.add(bucket.getName());
		}
		return bucketNames;
	}
	
	public static boolean stream(String bucketName, String key, InputStream input, String contentType) {

		boolean success = true;
		
		try {
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentLength(input.available());
			metadata.setContentType(contentType);

			s3client.putObject(new PutObjectRequest(bucketName, key, input, metadata)
					.withCannedAcl(CannedAccessControlList.PublicRead));

		} catch (IOException | AmazonClientException e) {
			e.printStackTrace();
			success = false;			
		}
		return success; 
	}
	
	public static void upload(String bucketName, String folderName) {
		
		// upload file to folder and set it to public
		String fileName = folderName + SUFFIX + "smile.png";
		s3client.putObject(new PutObjectRequest(bucketName, fileName, 
				new File("C:\\Users\\BQ\\Desktop\\smile.png")) // original location 
				.withCannedAcl(CannedAccessControlList.PublicRead));
	
		PutObjectRequest test = new PutObjectRequest(fileName, fileName, null, null);
	}
	
	public static void createFolder(String bucketName, String folderName, AmazonS3 client) {
		
		// create meta-data for your folder and set content-length to 0
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(0);
		
		// create empty content
		InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
		// create a PutObjectRequest passing the folder name suffixed by /
		
		PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, folderName + SUFFIX, emptyContent, metadata);
		
		// send request to S3 to create folder
		client.putObject(putObjectRequest);
	}
	
	/**
	 * This method first deletes all the files in given folder and than the
	 * folder itself
	 */
	public static void deleteFolder(String bucketName, String folderName, AmazonS3 client) {
		List<S3ObjectSummary> fileList = client.listObjects(bucketName, folderName).getObjectSummaries();
		
		for (S3ObjectSummary file : fileList) {
			client.deleteObject(bucketName, file.getKey());
		}
		client.deleteObject(bucketName, folderName);
	}
	
	// Deletes bucket
	public void deleteBucket(String bucketName) {
		s3client.deleteBucket(bucketName);
	}
}
