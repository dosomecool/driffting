package com.drifft.service.utilities;

public class MathUtility {

	public static Double[] getMean(int numPath, int m, Double[][] survivalData) {
		
		Double[] mean = new Double[m];
		
		for(int j=0; j<m; j++) {
			
			double meanSum = 0;
			
			for(int index = 0; index < numPath; index++) {
			
				meanSum = meanSum + survivalData[index][j];
			}	
			mean[j] = ( meanSum / numPath );
		}
		return mean;
	}
	
	public static Double[] getStandardDeviation(int numPath, int m, Double[][] mean, Double[][] data) {
		
		Double[] sd = new Double[m];
		
		for(int j=0; j<m; j++) {
			
			double stdDevSum = 0;
			
			for(int index = 0; index < numPath; index++ ) {
			
				stdDevSum = stdDevSum + 
						( ( data[index][j] - mean[0][j] ) * ( data[index][j] - mean[0][j] ) );
			}
			sd[j] = Math.sqrt( stdDevSum / numPath );
		}
		return sd;
	}
}
