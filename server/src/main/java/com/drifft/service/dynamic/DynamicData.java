package com.drifft.service.dynamic;

public interface DynamicData {
	
	public Double[] getTime();
	public Double[] getState();
	public Double[][] getWiener();
	public Double[][] getDiffusion(Double[] time, Double[][] wiener);
	public Double[][] getKilling(Double[] time, Double[][] diffusion);
	public Double[][] getSurvival(Double[][] killing);
	public Double[] getDensity(Double[][] survival);
}
