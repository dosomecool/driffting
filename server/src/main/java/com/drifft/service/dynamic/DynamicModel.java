package com.drifft.service.dynamic;

import com.drifft.domain.UserInput;

public class DynamicModel extends DynamicLoader implements DynamicData {

	private String javaCode;
	private String className;
	
	private UserInput input;
	
	public DynamicModel(UserInput xInput){		
		this.input = xInput;
		super.compile(setClassName(), setJavaCode());
	}
	
	// *** DYNAMIC CLASS ***
	
	public String setClassName(){
		
		className = "com.drifft.service.dynamic.DataGenerator";
		return className;
	}
	
	public String setJavaCode() {
		
		javaCode = "package com.drifft.service.dynamic; \n\n"
			
				 + "import com.drifft.service.dynamic.DynamicData; \n\n" 
				 + "import java.util.Random; \n"
                 + "import static java.lang.Math.*; \n\n"
				 
		    	 + "public class DataGenerator implements DynamicData { \n\n"

		    	 // initialState
		    	 + "private Double initialState; \n"
		    	 
		    	 // numPath
		    	 + "private int numPaths; \n"

		    	 // tMin
		    	 + "private Double tMin; \n"

		    	 // tMax
		    	 + "private Double tMax; \n"
		    	 
		    	 // dt
		    	 + "private Double dt; \n"  
		    	 
		    	 // xMin
		    	 + "private Double xMin; \n"

		    	 // xMax
		    	 + "private Double xMax; \n"

 		    	 // dx
		    	 + "private Double dx; \n" 
		    	 
		    	 // n - state index number
		    	 + "private int n; \n"

		    	 // m - time index number
		    	 + "private int m; \n"                    

		    	 // SEED
		    	 + "private long seed; \n\n"
	    	 		  
		    	 // *** BEGIN CONSTRUCTOR ***
		    	 + "public DataGenerator() { \n\n"
		    	
				 // INSERTED VALUE - *** num_paths ***
				 + "numPaths = " + input.getPaths() + "; \n"
				 
				 // INSERTED VALUE - *** intial_state ***
				 + "initialState = " + input.getInitialState() + "; \n"

				 // INSERTED VALUE - *** t_min, t_max ***
				 + "tMin = " + input.getT0() + "; \n"
				 + "tMax = " + input.getTf() + "; \n"
				 
				 // INSERTED VALUE - *** dt ***
				 + "dt = " + input.getDt() + "; \n"
				 
				 // INSERTED VALUE - ***x_min, x_max ***
				 + "xMin = " + input.getX0()+ "; \n"
				 + "xMax = " + input.getXf() + "; \n"

				 // INSERTED VALUE - *** _h ***
				 + "dx = " + input.getDx() + "; \n"

				 // INSERTED VALUE - *** _seed ***
				 + "seed = " + input.getSeed() + "L; \n\n"

				 // CALCULATES *** n *** 
				 + "Double tempN = new Double( ( (xMax-xMin) / dx) ); \n"
				 + "n = tempN.intValue(); \n\n"
				 
				 // CALCULATES *** m ***
				 + "Double tempM = new Double( ( (tMax-tMin) / dt) ); \n"
				 + "m = tempM.intValue(); \n"

				 + "} \n\n"  // END CONSTRUCTOR
				 
				 // TIME
				 + "@Override \n" 
				 + "public Double[] getTime() { \n"
						
				 + "Double[] time = new Double[m]; \n\n" 
						
				 + "for(int j=0; j<m; j++) { \n\n" 
							
				 + "time[j] = ( tMin + j * dt ); \n" 
				 + "} \n" 
				 + "return time; \n"
				 + "} \n\n"
				 
				 // STATE
				 + "@Override \n" 
				 + "public Double[] getState() { \n"
						
				 + "Double[] state = new Double[n]; \n\n" 
						
				 + "for(int i=0; i<n; i++) { \n\n" 
							
				 + "state[i] = ( xMin + i * dx ); \n" 
				 + "} \n" 
				 + "return state; \n"
				 + "} \n\n"
					
				 // WIENER
				 + "@Override \n" 
				 + "public Double[][] getWiener() { \n\n"
						
				 + "Double[][] wiener = new Double[numPaths][m]; \n" 
				 + "Random random = new Random(seed); \n\n" 
						
				 + "for(int index = 0; index < numPaths; index++) { \n\n" 
							
				 + "for(int j=0; j<m; j++) { \n\n" 

				 + "wiener[index][j] = random.nextGaussian(); \n" 
	             + "} \n" 
				 + "} \n"
				 + "return wiener; \n"
				 + "} \n\n"
					
				 // DIFFUSION
				 + "@Override \n" 
				 + "public Double[][] getDiffusion(Double[] time, Double[][] wiener) { \n\n"
						
				 + "Double[][] diffusion = new Double[numPaths][m]; \n\n"
						
				 + "for(int index = 0; index < numPaths; index++) { \n\n" 

				 + "diffusion[index][0] = initialState; \n\n" 

				 + "for(int j=0; j<m-1 ; j++){ \n\n" 

				 + "Double t = time[j]; \n"
				 + "Double w = wiener[index][j]; \n" 
				 + "Double x = diffusion[index][j]; \n" 
				 
				 // INSERTED VALUE - *** _seed ***
				 + "diffusion[index][j+1] = ( x + (" + input.getDrift() + ") * dt + (" + input.getDiffusion() + ") * Math.sqrt(dt) * w ); \n" 
				 + "} \n" 
				 + "} \n"
				 + "return diffusion; \n" 
				 + "} \n\n"
				 
				 // KILLING
				 + "@Override \n" 
				 + "public Double[][] getKilling(Double[] time, Double[][] diffusion) { \n\n"
				 
                 + "Double[][] killing = new Double[numPaths][m]; \n\n"
				 
				 + "for(int index = 0; index < numPaths; index++) { \n"
				  
				 + "for(int j=0; j<m ; j++) { \n"
						  
				 // INSERTED VALUE - *** kill ***
				 + "Double t = time[j]; \n"
                 + "Double x = diffusion[index][j]; \n" 
                 + "killing[index][j] = (" + input.getKilling() + "); \n"
                 
				 + "} \n" 	
				 + "} \n"
				 + "return killing; \n" 
				 + "} \n\n"
				 
				 // SURVIVAL
				 + "@Override \n" 
				 + "public Double[][] getSurvival(Double[][] killing) { \n\n"
				 
                 + "Double[][] survival = new Double[numPaths][m]; \n\n"

				 + "for(int index = 0; index < numPaths; index++) { \n"
				 
                 + "survival[index][0] = 1.0; \n\n" 
				  
				 + "for(int j=0; j<m-1 ; j++) { \n"
				 
                 + "survival[index][j+1] = ( (1 - killing[index][j] * dt) * survival[index][j] ); \n" 
				 
				 + "} \n" 	
				 + "} \n"
				 + "return survival; \n" 
				 + "} \n\n"
				 
				 // DENSITY
				 + "@Override \n" 
				 + "public Double[] getDensity(Double[][] survival) { \n\n"
				 
                 + "Double[] density = new Double[m]; \n"
                 + "density[0] = 0.0; \n\n" 
                 
                 + "for(int j=1; j<m-1 ; j++) { \n"
                 
                 + "Double sumLeft = 0.0; \n\n"
                 + "Double sumRight = 0.0; \n\n"
                 
                 + "for(int index = 0; index < numPaths; index++) { \n"
                 
                 + "sumLeft = sumLeft + survival[index][j]; \n"
                 + "sumRight = sumRight + survival[index][j+1]; \n\n"
                 
                 + "} \n"

                 + "density[j+1] = ( ( (sumLeft/numPaths) - (sumRight/numPaths) ) / (2 * dt) ); \n"
	
	             + "} \n"
	             + "return density; \n" 
	             + "} \n\n"

				 + "} \n"; // end
		
//		System.out.println(javaCode);
		return javaCode;
	}
	
	public Double[] getTime() {
		return super.getCallClass().getTime();
	}
	
	public Double[] getState() {
		return super.getCallClass().getState();
	}
	
	public Double[][] getWiener() { 
		return super.getCallClass().getWiener();
	}
	
	public Double[][] getDiffusion(Double[] timeData, Double[][] wienerData) { 
		return super.getCallClass().getDiffusion(timeData, wienerData);
	}
	
	public Double[][] getKilling(Double[] time, Double[][] diffusion) { 
		return super.getCallClass().getKilling(time, diffusion);
	}
	
	public Double[][] getSurvival(Double[][] killing) {
		return super.getCallClass().getSurvival(killing);
	}

	public Double[] getDensity(Double[][] survival) {
		return super.getCallClass().getDensity(survival);
	}
}
