package com.drifft.service.dynamic;

import java.util.UUID;
import java.util.concurrent.BlockingQueue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.drifft.constant.Constants.Request;
import com.drifft.domain.Status;
import com.drifft.domain.UserInput;
import com.drifft.service.neuralnetwork.regression.NeuralNetwork;
import com.drifft.service.neuralnetwork.utilities.Action;
import com.drifft.service.neuralnetwork.utilities.Histogram;
import com.drifft.service.utilities.Data;
import com.drifft.service.utilities.MathUtility;
import com.drifft.websockets.WebSocketController;

@Service
public class Simulator  { //implements Runnable

	@Autowired
	private WebSocketController webSocketController;
	
	private UserInput userInput;
	private BlockingQueue<Action> netQueue;
	private BlockingQueue<Action> plotQueue;
	private BlockingQueue<Action> dataQueue;
	
	private boolean initialized = false;
	
	public Simulator setInput(UserInput userInput) {
		this.userInput = userInput;
		return this;
	}

	public Simulator setNetQueue(BlockingQueue<Action> netQueue) {
		this.netQueue = netQueue;
		return this;
	}

	public Simulator setPlotQueue(BlockingQueue<Action> plotQueue) {
		this.plotQueue = plotQueue;
		return this;
	}
	
	public Simulator setDataQueue(BlockingQueue<Action> dataQueue) {
		this.dataQueue = dataQueue;
		return this;
	}

	// Data created by dynamic model
	private Double[] time;
	private Double[] state;
	private Double[][] wiener;
	private Double[][] diffusion;
	private Double[][] killing;
	private Double[][] survival;
	private Double[] density; 

	// Action carries request and data
	private Action action;
	
//	@Override
	public void run() {

		try{
			while((action = dataQueue.take()).getRequest() != Request.EXIT) {

				switch (action.getRequest()) {
				case INITIALIZE: 
					initialize(action);
					break;

				default:
					break;
				}
			}

		} catch(InterruptedException e) {
			e.printStackTrace(); 
		} finally {

		}	
	}
	
	public void initialize(Action action) {
		
		webSocketController.sendMessage(new Status("info", "Generating data... please wait!"));

		DynamicModel dataGenerator = new DynamicModel(userInput);

		time = dataGenerator.getTime();
		state = dataGenerator.getState();
		wiener = dataGenerator.getWiener();
		diffusion = dataGenerator.getDiffusion(time, wiener);
		killing = dataGenerator.getKilling(time, diffusion);
		survival = dataGenerator.getSurvival(killing);
		density = dataGenerator.getDensity(survival);
		
		// TODO: need drift[]
		// TODO: need diffusion[]
	}
	
	//	@Override
	public void execute() {
		
		double tMin = userInput.getT0();
		double tMax = userInput.getTf();
		double dt = userInput.getDt();

		double xMin = userInput.getX0();
		double xMax = userInput.getXf();
		double dx = userInput.getDx();

		webSocketController.sendMessage(new Status("info", "Data generation complete!"));
		
		if(userInput.getOutput().equals("cumulative-hazard")) {

			Histogram histogram = new Histogram(tMin, tMax, dt, xMin, xMax, dx, diffusion);

		} else if(userInput.getOutput().equals("mean-survival")) {

	        Double[][] diffusionFrequencyData = new Histogram(tMin, tMax, dt, xMin, xMax, dx, diffusion).getHistogram(); 
			Double[] meanSurvivalData = MathUtility.getMean(userInput.getPaths(), userInput.getM(), survival);
			
			
			submit(new Action(Request.TRAIN)
					.putData(time)  
					.putData(Catalog.NEURAL_NETWORK.stateType(), state)   
					.putData(Catalog.NEURAL_NETWORK.inputType(), diffusionFrequencyData)
					.putData(Catalog.NEURAL_NETWORK.outputType(), meanSurvivalData), netQueue);  
			
		} else if(userInput.getOutput().equals("density")) {

			Histogram histogram = new Histogram(tMin, tMax, dt, xMin, xMax, dx, diffusion);
             
		}
		webSocketController.sendMessage("success", "Data generation complete");
	}

	private void submit(Action action, BlockingQueue<Action> queue) { 
		try {	
			webSocketController.sendMessage(new Status("info", "Trying to submit!"));
			queue.put(action);
			webSocketController.sendMessage(new Status("info", "Mean survival case submitted to: " + queue.getClass().getName() ));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}	
}
