package com.drifft.service.dynamic;

import java.util.ArrayList;
import java.util.List;

import net.openhft.compiler.CompilerUtils;

public abstract class DynamicLoader {
	
	private String className;
	private String javaCode;    

	@SuppressWarnings("rawtypes")
	private Class dynamicClass;
	private ClassLoader classLoader;
		
	private DynamicData callClass;
	
//	private List<Error> errors = new ArrayList<>();
	
//	https://github.com/OpenHFT/Java-Runtime-Compiler
	
	private void loadClass(){
		
		classLoader = new ClassLoader() {
		};
		
		try {
			dynamicClass = CompilerUtils.CACHED_COMPILER.loadFromJava(classLoader, className, javaCode);
			
		} catch (ClassNotFoundException e1) {
//			errors.add(new Error("Compiler", ""));

		}

		try {			
			callClass = (DynamicData) dynamicClass.newInstance();
		} catch (InstantiationException e) {
			// TODO: Send message to client
//			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO: Send message to client 
//			e.printStackTrace();
		} catch (NullPointerException e){
			// TODO: Send message to client
//			e.printStackTrace();
		}
	}
		
	public void compile(String xName, String xCode) {
		
		this.className = xName;
		this.javaCode = xCode;
		loadClass();
	}

	public DynamicData getCallClass() {
		return callClass;
	}
}
