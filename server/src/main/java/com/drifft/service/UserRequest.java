package com.drifft.service;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

public class UserRequest {

	private String request;
		
	public UserRequest() {		
	}

	public UserRequest(String request) {
		this.request = request;
	}
	
	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	@Override 
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("request: ").append(request);
		
		String jsonStr = null; 
		ObjectMapper mapper = new ObjectMapper();
		
        try {
            jsonStr = mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
		return jsonStr;
	}
}
