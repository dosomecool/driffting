package com.drifft.service.plot;

import java.util.concurrent.BlockingQueue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.drifft.constant.Constants.Functionality;
import com.drifft.service.neuralnetwork.utilities.Action;
import com.drifft.websockets.WebSocketController;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;

@Service
public class Plotter implements Runnable {

	private BlockingQueue<Action> dataQueue;
	Action action;
	
	@Autowired
	private WebSocketController webSocketController;
	
	public Plotter setDataQueue(BlockingQueue<Action> dataQueue) {
		this.dataQueue = dataQueue;
		return this;
	}
	
	@Override
	public void run() {

		com.sun.javafx.application.PlatformImpl.startup(()->{});
		
		try{
			while((action = dataQueue.take()).getType() != Functionality.EXIT) {

				// TODO: Implement plot factory
				
				ThreeLayerItoPlot plot = new ThreeLayerItoPlot(action);
				Task<Void> plotTask = plot.getTask();  
				setTaskListener(plotTask, plot, action.getType()); 
								
				Platform.runLater(new Runnable() {

					@Override
					public void run() {
						plotTask.run();
					}
				});
			}

		} catch(InterruptedException e) {
			e.printStackTrace(); 
		} finally {
			// Exit JavaFx
//	     	com.sun.javafx.application.PlatformImpl.exit();
		}	
	}
	
	private void setTaskListener(Task<Void> task, ThreeLayerItoPlot plot, Functionality type) {
		
		task.stateProperty().addListener((obs, oldState, newState) -> {

			if (newState == Worker.State.SUCCEEDED) {
				
//				InputStream inputStream = plot.getStream();
//				String key = Constants.DATA_FOLDER + type + "-" + UUID.randomUUID().toString() + ".png";
//				boolean test = S3BucketUtility.stream(Constants.PRIMARY_BUCKET, key, inputStream, "image/png");
//				webSocketController.sendMessage(type.toString(), key);
				
				System.out.println("Producing new graph");
				
			} else if (newState == Worker.State.FAILED) {
				task.getException();
				
				webSocketController.sendMessage(type.toString(), new Object[]{"failed"});
			}
		});
	}
}
