package com.drifft.service.plot;

import java.io.IOException;
import java.io.InputStream;

import com.drifft.constant.Constants;
import com.drifft.domain.UserInput;
import com.drifft.service.neuralnetwork.utilities.Action;
import com.drifft.service.utilities.ImageUtility;
import com.drifft.service.utilities.MathUtility;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class ThreeLayerItoPlot extends AnchorPane {

	// DIFFUSION 
    @FXML
    private AnchorPane plotRoot;

    @FXML
    private VBox plotVBox;

    @FXML
    private StackPane plotStack;
    
    // DATA ATTRIBUTES
    private int numPaths;
    private int m;
    private double h;
    
    // AXIS ATTRIBUTES
    private String tAxisTitle = "TIME [ t ]";
    private String xAxisTitle;
    
    private double tmin;
    private double tmax;
    
    private double dt = 1.0;
    
    private double xmin = 0.0;
    private double xmax = 0.0;
    
    private double dx   = 0.5;
    
    // CHART ATTRIBUTES
    private int max = 100;
    
    private double width = 800;
    private double height = 450;
    
    // AXES
    private NumberAxis tAxis;
    private NumberAxis xAxis;
    
    // CHARTS
	private LineChart<Number,Number> pathChart;
	private LineChart<Number,Number> meanChart;
	private LineChart<Number,Number> sdLineChart;
	private AreaChart<Number,Number> sdAreaChart;
	
	// SERIES
	private ObservableList<XYChart.Series<Number, Number>> pathCollection = FXCollections.observableArrayList();
	private ObservableList<XYChart.Series<Number, Number>> meanCollection = FXCollections.observableArrayList();
	private ObservableList<XYChart.Series<Number, Number>> sdLineCollection = FXCollections.observableArrayList();
	private ObservableList<XYChart.Series<Number, Number>> sdAreaCollection = FXCollections.observableArrayList();
  
	private UserInput input;
	
	double[][] timeArray; // time array
	double[][] dataArray;
	double[][] meanArray; // mean array
	double[][] sdArray; // sd array
	
	InputStream inputStream = null; 
	
	public ThreeLayerItoPlot(Action action) {
		
		this.input = action.getInput();
	
		try {
			FXMLLoader loader = new FXMLLoader(getClass()
					.getResource(Constants.FXML_RESOURCE_PATH + "ItoGraph.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// SETS THE STYLE OF THIS PAGE
//		this.getStylesheets().addAll(getClass()
//	    		.getResource(Constants.CSS_RESOURCE_PATH + "UserInputStyle.css").toExternalForm());
		
		this.tmin = this.input.getT0();
		this.tmax = this.input.getTf();
		this.h = this.input.getDt();
		
		Double tempM = new Double( ( (tmax-tmin) / h) );
	  	this.m = tempM.intValue();
		
	  	this.numPaths = input.getPaths();

	  	// Data
	  	timeArray = action.getTime();
	  	dataArray = action.getData();
	  	meanArray = MathUtility.getMean(numPaths, m, dataArray);
	  	sdArray = MathUtility.getStandardDeviation(numPaths, m, meanArray, dataArray);
	  	
	  	listen();
	}
	
	private int getMax(){
		
	    if (numPaths < max){
	    	max = numPaths;
	    }
		return max;
	}
		
	private void initLayerOneChart() {
		
	    pathChart = new LineChart<Number,Number>(tAxis, xAxis);
	    
	    // PATH CHART PROPERTIES
	    pathChart.setLegendVisible(false);
	    pathChart.setAnimated(false);
	    pathChart.setCreateSymbols(false);
	    
	    pathChart.setHorizontalGridLinesVisible(false);
	    pathChart.setHorizontalZeroLineVisible(false);
	    
	    pathChart.setVerticalGridLinesVisible(false);
	    pathChart.setVerticalZeroLineVisible(false);
	    
	    pathChart.getStylesheets().addAll(getClass()
	    		.getResource(Constants.CSS_RESOURCE_PATH + "PathLineStyle.css").toExternalForm());
	    
	    pathChart.setMinSize(width, height);
	    pathChart.setMaxSize(width, height);
	    pathChart.setAnimated(false);
	    
	    
	    // ADD SERIES TO CHART
        pathChart.getData().addAll(pathCollection);
	}
	
	private void initLayerTwoChart() {
		
	    meanChart = new LineChart<Number,Number>(tAxis, xAxis);
	    
	    // MEAN CHART POPERTIES
	    meanChart.setLegendVisible(false);
	    meanChart.setAnimated(false);
	    meanChart.setCreateSymbols(false);
	    
	    meanChart.setHorizontalGridLinesVisible(false);
	    meanChart.setHorizontalZeroLineVisible(false);
	    
	    meanChart.setVerticalGridLinesVisible(false);
	    meanChart.setVerticalZeroLineVisible(false);
	    
	    meanChart.getStylesheets().addAll(getClass()
	    		.getResource(Constants.CSS_RESOURCE_PATH + "MeanLineChartStyle.css").toExternalForm());
        
	    meanChart.setMinSize(width, height);
	    meanChart.setMaxSize(width, height);
	    meanChart.setAnimated(false);
	    
	    // ADD SERIES TO CHART
        meanChart.getData().addAll(meanCollection);
	}
	
	private void initLayerThreeLineChart(){
		
		// *** LINE CHART ***
		
		// VARIANCE LINE CHART
        sdLineChart = new LineChart<Number,Number>(tAxis, xAxis);
        
        // VARIANCE LINE CHART PROPERTIES
        sdLineChart.setLegendVisible(false);
	    sdLineChart.setAnimated(false);
	    sdLineChart.setCreateSymbols(false);
	    
	    sdLineChart.setHorizontalGridLinesVisible(false);
	    sdLineChart.setHorizontalZeroLineVisible(false);
	    
	    sdLineChart.setVerticalGridLinesVisible(false);
	    sdLineChart.setVerticalZeroLineVisible(false);
	    
	    sdLineChart.getStylesheets().addAll(getClass()
	    		.getResource(Constants.CSS_RESOURCE_PATH + "SDLineChartStyle.css").toExternalForm());
	    
	    sdLineChart.setMinSize(width, height);
	    sdLineChart.setMaxSize(width, height);
	    sdLineChart.setAnimated(false);
	    
	    // ADD SERIES TO CHART
        sdLineChart.getData().addAll(sdLineCollection);
	}
	
	
	public void initLayerThreeAreaChart() {
		
	    // *** AREA CHART ***
	    
        // VARIANCE AREA CHART
	    sdAreaChart = new AreaChart<Number,Number>(tAxis,xAxis);
	    
	    // VARIANCE AREA CHART PROPERTIES
	    sdAreaChart.setLegendVisible(false);
	    sdAreaChart.setAnimated(false);
	    sdAreaChart.setCreateSymbols(false);
	   
	    sdAreaChart.setHorizontalGridLinesVisible(false);
	    sdAreaChart.setHorizontalZeroLineVisible(false);
	    
	    sdAreaChart.setVerticalGridLinesVisible(false);
	    sdAreaChart.setVerticalZeroLineVisible(false);
	    
	    sdAreaChart.getStylesheets().addAll(getClass()
	    		.getResource(Constants.CSS_RESOURCE_PATH + "SDAreaChartStyle.css").toExternalForm());
        
	    sdAreaChart.setMinSize(width, height);
	    sdAreaChart.setMaxSize(width, height);
	    sdAreaChart.setAnimated(false);
	    
        // ADD SERIES TO CHART
        sdAreaChart.getData().addAll(sdAreaCollection);
	}
	
	private void stackPlots(){
		plotStack.getChildren().addAll(sdAreaChart, pathChart, sdLineChart, meanChart);
	}
			
	// DATA TASK
	Task<Void> plotTask = new Task<Void>() {
		
		@Override 
		public Void call() {
			
			
		    // PATH CHART SERIES
		    for (int n=0; n<getMax(); n++)
			{
		    	pathCollection.add(new Series<Number, Number>());
			}
			
			for (int n=0; n<numPaths; n++) {
				
				for(int i=0; i<=m; i++) {
					
					// PATH DATA
					pathCollection.get(n).getData().add(new XYChart.Data<Number, Number>( 
							timeArray[0][i],   // time
							dataArray[n][i])); // value
				}
			}
			
	        // MEAN CHART SERIES
	        meanCollection.add(new Series<Number, Number>());
	        		    
		    // STD DEV LINE CHART SERIES - TWO SERIES TOTAL #0 and #1
		    sdLineCollection.add(new Series<Number, Number>());
		    sdLineCollection.add(new Series<Number, Number>());
		    
			// STD DEV AREA CHART SERIES - TWO SERIES TOTAL #0 and #1
		    sdAreaCollection.add(new Series<Number, Number>());
		    sdAreaCollection.add(new Series<Number, Number>());
		    
		    double upper;
		    double lower;
		    
			for(int i=0; i<=m; i++) {

				// MEAN DATA
				meanCollection.get(0).getData().add(new XYChart.Data<Number, Number>( 
						timeArray[0][i], 
						meanArray[0][i]));
				
				upper = meanArray[0][i] + sdArray[0][i];
				lower = meanArray[0][i] - sdArray[0][i];
				
				
				// STD DEV LINE DATA
				sdLineCollection.get(0).getData().add(new XYChart.Data<Number, Number>( 
						timeArray[0][i], 
						upper));
				
				// STD DEV LINE DATA
				sdLineCollection.get(1).getData().add(new XYChart.Data<Number, Number>( 
						timeArray[0][i], 
						lower));
				
				
				// STD DEV AREA DATA
				sdAreaCollection.get(0).getData().add(new XYChart.Data<Number, Number>( 
						timeArray[0][i], 
						upper));
			
				// STD DEV AREA DATA
				sdAreaCollection.get(1).getData().add(new XYChart.Data<Number, Number>( 
						timeArray[0][i], 
						lower));
				
				if ( upper > xmax ){
					xmax = upper;
				}
				
				if ( lower < xmin ){
					xmin = lower;
				}
			}
			
			tAxis = new NumberAxis(tAxisTitle, tmin , tmax,  dt);
			xAxis = new NumberAxis(xAxisTitle, xmin, xmax, dx); 

			// --- GENERATE CHARTS ---
			
			initLayerOneChart();
			initLayerTwoChart();
			initLayerThreeLineChart();
			initLayerThreeAreaChart();
			
			stackPlots();
			
			boolean success = (boolean) ImageUtility.savePngAsStream(plotRoot)[0];
			
			if(success) {
				inputStream = (InputStream) ImageUtility.savePngAsStream(plotRoot)[1];
			} else {
				super.failed();
				updateMessage("System failed to generate image stream!");
			}
			return null;
		}
		
		@Override 
		protected void succeeded() {
	        super.succeeded();
	        updateMessage("Done!");
	    }
	};
	
	// For troubleshooting
	public void listen() {

		plotTask.exceptionProperty().addListener((observable, oldValue, newValue) ->  {
			if(newValue != null) {
				Exception ex = (Exception) newValue;
				ex.printStackTrace();
			}
		});
	}
    	
    public Task<Void> getTask(){
    	return plotTask;
    }
    
    public InputStream getStream() {
    	return this.inputStream;
    }
}
