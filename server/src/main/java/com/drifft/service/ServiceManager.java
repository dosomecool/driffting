package com.drifft.service;

import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.TransferQueue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.drifft.constant.Constants.Request;
import com.drifft.domain.Status;
import com.drifft.domain.UserInput;
import com.drifft.service.dynamic.Simulator;
import com.drifft.service.neuralnetwork.regression.NeuralNetwork;
import com.drifft.service.neuralnetwork.utilities.Action;
import com.drifft.service.plot.Plotter;
import com.drifft.websockets.WebSocketController;

@Component
public class ServiceManager {

	private UserInput input;
	final BlockingQueue<Action> plotQueue = new LinkedBlockingQueue<Action>();
	final BlockingQueue<Action> netQueue = new LinkedBlockingQueue<Action>();
	final BlockingQueue<Action> dataQueue = new LinkedBlockingQueue<Action>();

	@Autowired
	private Simulator simulator;

	@Autowired
	private Plotter plotter;

	@Autowired
	private NeuralNetwork network;

	@Autowired
	private WebSocketController webSocketController;
	

	//	http://winterbe.com/posts/2015/04/07/java8-concurrency-tutorial-thread-executor-examples/

	// input

	public void process(Request request) {
		
        switch (request) {
		case INITIALIZE: 
			input.setId(UUID.randomUUID());
			webSocketController.sendMessage("uuid", input.getId());
			webSocketController.sendMessage(new Status("info", "Input ready to train!"));
			break;
			
		case GENERATE:
			
			break;
		
		case TRAIN: 
			break;

		case VALIDATE: 
			break;

		case PREDICT: 
			break;

		case STOP: 
			break;

		case CHECK_GRADIENT:
			break;

		case DOWNLOAD:
			break;

		case EXIT:
			break;

		default:
			break;
		}


		webSocketController.sendMessage(new Status("info", "Submitting input... please wait!"));

		netexec.submit(network.setInput(input).setNetQueue(netQueue));

		// Future
		//			https://www.baeldung.com/java-runnable-callable

		simulator.setInput(input).setNetQueue(netQueue).setPlotQueue(plotQueue);
		simulator.run();

		network.setInput(input).setNetQueue(netQueue);

		submitAction(new Action(Request.INITIALIZE));





		//		simexec.submit(simulator.setInput(input).setQueue(dataQueue));
		//		plotexec.submit(plotter.setDataQueue(dataQueue));

		//		simexec.shutdown();
		//		plotexec.shutdown();

	}

	public ServiceManager setInput(UserInput input) {
		this.input = input;
		return this;
	}

	private void submitAction(Action action) { 
		try {
			dataQueue.put(action);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	// Simulator executor
	private ExecutorService simexec = Executors.newSingleThreadExecutor( r -> {

		Thread t = new Thread(r, "data-generator-thread");
		t.setDaemon(true);
		return t;
	});

	// Plotter
	private ExecutorService plotexec = Executors.newSingleThreadExecutor( r -> {

		Thread t = new Thread(r, "data-plotter-thread");
		t.setDaemon(true);
		return t;
	});

	// Network executor
	private ExecutorService netexec = Executors.newSingleThreadExecutor( r -> {

		Thread t = new Thread(r, "neural-network-thread");
		t.setDaemon(true);
		return t;
	});
}
