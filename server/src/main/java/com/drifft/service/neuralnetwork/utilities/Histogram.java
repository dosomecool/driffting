package com.drifft.service.neuralnetwork.utilities;

public class Histogram {
	
	private double tMin;
	private double tMax;
	private double dt;
	
	private double xMin; 
	private double xMax; 
	private double dx;
	
	private int rows; 
	
	private Double[][] data;
	private Double[][] histogram;
	
	private int n;    // space index 
	private int m;    // time  
	
	public Histogram(double tMin, double tMax, double dt, double xMin, double xMax, double dx, Double[][] diffusionData) {
		this.tMin = tMin;
		this.tMax = tMax;
		this.dt = dt;
		this.xMin = xMin;
		this.xMax = xMax;
		this.dx = dx;
		this.data = diffusionData;
		
		this.rows = diffusionData.length;
			
		Double tempM = new Double( ( (this.tMax - this.tMin) / this.dt) );
		this.m = tempM.intValue();
		
		Double tempN = new Double( ( (this.xMax - this.xMin) / this.dx) );
		this.n = tempN.intValue();
		
		this.histogram = new Double[m][n];
		initZeroArray();
	}
	
	public Double[][] getHistogram() {
		
		for(int j=0; j<m; j++) {
			
			double xLower = xMin;
			double xUpper = xLower + dx;

			for(int i=0; i<n; i++) {

				histogram[j][i] = setScores(xLower, xUpper, j);
			    xLower = xUpper;
				xUpper = xUpper + dx;
			}
		}
		return histogram;
	}
	
	private double setScores(double xLower, double xUpper, int tIndex) {
		double sum = 0.0;
		
		// path
		for(int i=0; i<rows; i++) {

			double d = data[i][tIndex];

			if(d >= xLower && d < xUpper) {
				sum = sum + 1.0;
			}
		}
		return (sum / rows);
	}
	
	private void initZeroArray() {
		// time
		for(int j=0; j<m; j++) {
			
			// path
			for(int i=0; i<n; i++) {
				histogram[j][i] = 0.0;
			}
		}
	}
}
