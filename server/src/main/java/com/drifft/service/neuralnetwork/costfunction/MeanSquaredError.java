package com.drifft.service.neuralnetwork.costfunction;

import com.drifft.service.neuralnetwork.network.Types.CostFunctionType;

public class MeanSquaredError implements CostFunction {

	public MeanSquaredError() {
	}
	
	@Override
	public double getErrorValue(double yhat, double y) {	
		return Math.pow((y - yhat), 2);
	}
		
	@Override
	public double getErrorDerivativeWrtActivatedValue(double yhat, double y) {
		return -2.0 * getErrorValue(yhat, y);
	}

	@Override
	public CostFunctionType getCostFunction() {
		return CostFunctionType.MSE;
	}
}
