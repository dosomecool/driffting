package com.drifft.service.neuralnetwork.visualization;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

public class VisEdge {

	private int from;
	private int to;
	private String label;
	
	public VisEdge() {
	}
	
	public VisEdge(int from, int to, String label) {
		this.from = from;
		this.to = to;
		this.label = label;
	}
	
	public int getFrom() {
		return from;
	}

	public void setFrom(int from) {
		this.from = from;
	}

	public int getTo() {
		return to;
	}

	public void setTo(int to) {
		this.to = to;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override 
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("from: ").append(from);
		sb.append("\nto: ").append(to);
		sb.append("\nlabel: ").append(label);
		
		String jsonStr = null; 
		ObjectMapper mapper = new ObjectMapper();
		
        try {
            jsonStr = mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
		return jsonStr;
	}
}
