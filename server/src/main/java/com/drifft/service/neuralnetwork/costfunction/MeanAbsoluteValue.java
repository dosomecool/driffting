package com.drifft.service.neuralnetwork.costfunction;

import com.drifft.service.neuralnetwork.network.Types.CostFunctionType;

public class MeanAbsoluteValue implements CostFunction {

	public MeanAbsoluteValue() {
	}
	
	@Override
	public double getErrorValue(double yhat, double y) {
		
		System.out.println("y: " + y + " yhat: " + yhat + " error: " + Math.abs(y - yhat) );
		
		return Math.abs( y - yhat );
	}

	@Override
	public double getErrorDerivativeWrtActivatedValue(double yhat, double y) {
		return (yhat - y) / getErrorValue(yhat, y);
	}

	@Override
	public CostFunctionType getCostFunction() {
		return CostFunctionType.MAD;
	}
}
