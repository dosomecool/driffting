package com.drifft.service.neuralnetwork.activation;

import com.drifft.service.neuralnetwork.network.Types.ActivationType;

public class Gaussian implements Function {

	public Gaussian() {
	}
	
	@Override
	public double getFunctionValue(double x) {
		return Math.exp(-1.0 * Math.pow(x, 2));
	}

	@Override
	public double getFunctionDerivativeValue(double x) {
		return -2.0 * x * getFunctionValue(x);
	}

	@Override
	public ActivationType getActivation() {
		return ActivationType.GAUSSIAN;
	}
}
