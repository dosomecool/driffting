package com.drifft.service.neuralnetwork.optimization;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NesterovMomentum implements Optimizer {

	private double alpha = 0.02;
	private double beta = 0.99;
	
	// Holds the velocity values
	Map<String, Double> history = new HashMap<>();

	Map<String, List<Double>> gradients = new HashMap<>();
	private boolean update = false;
	
	public void setConfiguration(Double alpha, Double beta) {
		this.alpha = alpha;
		this.beta = beta;
	}
	
	// source: https://distill.pub/2017/momentum/
	// velocity = beta * velocity + gradient
	// weight = weight - alpha * velocity
	public double updateParameter(String id, double weight, double gradient) {
		
		if(Math.abs(gradient) > 2.0) gradient = 2.0 * (gradient / Math.abs(gradient)); 
		
		if(!gradients.containsKey(id)) {
			gradients.put(id, new ArrayList<>());
			gradients.get(id).add(gradient);
		} else {
			gradients.get(id).add(gradient);
		}
		
		if(update) {
			double sum = 0.0; 
			
			for(double g : gradients.get(id)) {
				sum = sum + g;
			}
			Double velocity = history.get(id);
			velocity = (velocity != null) ? beta * velocity + (sum / gradients.get(id).size()) : (sum / gradients.get(id).size());
			weight = weight - alpha * velocity;
			gradients.get(id).clear();
		}
		return weight;
	}

	@Override
	public void update(boolean update) {
		this.update = update;
	}

}
