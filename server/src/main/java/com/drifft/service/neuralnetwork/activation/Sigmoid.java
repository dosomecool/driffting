package com.drifft.service.neuralnetwork.activation;

import com.drifft.service.neuralnetwork.network.Types.ActivationType;

public class Sigmoid implements Function{

	public Sigmoid() {
	}
	
	@Override
	public double getFunctionValue(double x) {
		return ( 1 / ( 1 + Math.exp(-x)) );
	}
	
	@Override
	public double getFunctionDerivativeValue(double x) {
		return ( getFunctionValue(x) * ( 1.0 - getFunctionValue(x)) );
	}

	@Override
	public ActivationType getActivation() {
		return ActivationType.SIGMOID;
	}
}
