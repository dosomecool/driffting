package com.drifft.service.neuralnetwork.activation;

import com.drifft.service.neuralnetwork.network.Types.ActivationType;

public class LeakyRelu implements Function{

	double alpha = 0.33;
	
	public LeakyRelu() {
	}
	
	@Override
	public double getFunctionValue(double x) {
		if( x > 0 ) return x;
		return alpha * x;
	}

	@Override
	public double getFunctionDerivativeValue(double x) {	
		if( x > 0 ) return 1.0;
		return alpha;
	}

	@Override
	public ActivationType getActivation() {
		return ActivationType.LEAKY_RELU;
	}
}
