package com.drifft.service.neuralnetwork.kolmogorov;

public class KolmogorovSolution {

	private double mu;
	private double sigma;
	private double x0;
	
	public KolmogorovSolution(double mu, double sigma, double x0) {
		this.mu = mu;
		this.sigma = sigma;
		this.x0 = x0;
	}
	
	public double integrate(double a, double b, double t) {
	      int N = 10000;                    // precision parameter
	      double h = (b - a) / (N - 1);     // step size
	 
	      // 1/3 terms
	      double sum = 1.0 / 3.0 * (pdf(a, t) + pdf(b, t));

	      // 4/3 terms
	      for (int i = 1; i < N - 1; i += 2) {
	         double x = a + h * i;
	         sum += 4.0 / 3.0 * pdf(x, t);
	      }

	      // 2/3 terms
	      for (int i = 2; i < N - 1; i += 2) {
	         double x = a + h * i;
	         sum += 2.0 / 3.0 * pdf(x, t);
	      }
	      return sum * h;
	   }
	
	private double pdf(double x, double t) {
		return Math.exp( -1.0 * Math.pow((x - mu * t - x0), 2) / ( 2 * Math.pow(sigma, 2) * t )) / sigma * Math.sqrt(2 * Math.PI * t);
	}
}
