package com.drifft.service.neuralnetwork.activation;

import com.drifft.service.neuralnetwork.network.Types.ActivationType;

public class ActivationFunctionFactory {

	public Function getActivation(ActivationType activation){

		if(activation == null){
			return null;
		}	
		if(activation == ActivationType.ELU){
			return new Elu();
			
		} else if(activation == ActivationType.LEAKY_RELU){
			return new LeakyRelu();
			
		} else if(activation == ActivationType.RELU){
			return new Relu();
			
		} else if(activation == ActivationType.TANH){
			return new Tanh();
			
		} else if(activation == ActivationType.SIGMOID){
			return new Sigmoid();

		} else if (activation == ActivationType.LINEAR) {
			return new Linear();
			
		} else if (activation == ActivationType.GAUSSIAN) {
			return new Gaussian();
		}
		return null;
	}
}
