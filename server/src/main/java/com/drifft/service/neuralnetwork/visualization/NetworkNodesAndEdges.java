package com.drifft.service.neuralnetwork.visualization;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

public class NetworkNodesAndEdges {

	private List<VisNode> nodes;
	private List<VisEdge> edges;
	
	public NetworkNodesAndEdges() {
	}
	
	public NetworkNodesAndEdges(List<VisNode> nodes, List<VisEdge> edges) {
		this.nodes = nodes;
		this.edges = edges; 
	}
	
	public List<VisNode> getNodes() {
		return nodes;
	}

	public void setNodes(List<VisNode> nodes) {
		this.nodes = nodes;
	}

	public List<VisEdge> getEdges() {
		return edges;
	}

	public void setEdges(List<VisEdge> edges) {
		this.edges = edges;
	}

	@Override 
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("nodes: ").append(nodes);
		sb.append("\nedges: ").append(edges);
		
		String jsonStr = null; 
		ObjectMapper mapper = new ObjectMapper();
		
        try {
            jsonStr = mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
		return jsonStr;
	}	
}
