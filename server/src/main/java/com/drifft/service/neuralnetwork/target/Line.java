package com.drifft.service.neuralnetwork.target;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.drifft.service.neuralnetwork.network.Types.TargetFunctionType;

public class Line implements TargetFunction {

	// y = mx + b
	private final TargetFunctionType type = TargetFunctionType.LINE;
	
	private double m = 1.0;
	private double b = 0.0;
	
	private final Map<String, Double> params = new HashMap<>();
	
	public Line() {
		params.put("m", m);
		params.put("b", b);
	}
	
	@Override
	public double getFunctionValue(double x) {
		return m * x + b;
	}
			
	@Override
	public double getFunctionDerivativeValue(double x, String param) {
		
		if(param.equals("m")) {
			
			if(m == 0.0) return 0.0;
			return (m / Math.abs(m)) * x;
			
		} else if (param.equals("b")) {
			
			if(b == 0.0) return 0.0;
			return (b /Math.abs(b));
		} 
		return 1.0; 
	}
		
	@Override
	public Map<String, Double> getParams() {
		return params;
	}
	
	@Override
	public List<String> getParameterNames() {
		return new ArrayList<String>(params.keySet());
	}

	@Override
	public TargetFunctionType getType() {
		return this.type;
	}
}
