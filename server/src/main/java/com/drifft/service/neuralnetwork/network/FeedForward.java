package com.drifft.service.neuralnetwork.network;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.drifft.service.neuralnetwork.backpropagation.MultiThreadBackpropagation;
import com.drifft.service.neuralnetwork.costfunction.CostFunction;
import com.drifft.service.neuralnetwork.costfunction.CostFunctionFactory;
import com.drifft.service.neuralnetwork.network.Types.ActivationType;
import com.drifft.service.neuralnetwork.network.Types.ConnectionType;
import com.drifft.service.neuralnetwork.network.Types.CostFunctionType;
import com.drifft.service.neuralnetwork.network.Types.NodeType;
import com.drifft.service.neuralnetwork.network.Types.OptimizationAlgorithmType;
import com.drifft.service.neuralnetwork.optimization.OptimizationAlgorithmFactory;
import com.drifft.service.neuralnetwork.optimization.Optimizer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;

public class FeedForward {
	
	private String id; 
	private long seed = 123L; 
	
	@JsonIgnore
	private final Random random = new Random(seed);
		
	private int batch = 1;
	private double variance = 0.2;
	private CostFunction costFunction = new CostFunctionFactory().get(CostFunctionType.MSE);
	public Optimizer optimizer;
	
	@JsonIgnore
	private Layer inputLayer = new Layer(NodeType.INPUT);	
	private ArrayList<Layer> hiddenLayersList = new ArrayList<Layer>();
	private Layer outputLayer;
	
	@JsonIgnore
	private double error;
	
	// *** Initialize ***
    
	// Create input layer
	public FeedForward createInputLayer(Integer inputNodes) {

		for(int i=0; i<inputNodes; i++) {
			inputLayer.getNodeList().add(new Node(i));
		}
		return this;
	}

	// Create hidden layer
	public FeedForward createHiddenLayer(int hiddenNodes, ActivationType activation) {
        
		// Add new empty layer
		hiddenLayersList.add(new Layer(NodeType.HIDDEN, activation));

		// Set layer index
		int lastlayerIndex = hiddenLayersList.size()-1;
		hiddenLayersList.get(lastlayerIndex).setIndex(lastlayerIndex);
		
		// Add nodes to layer
		for(int i=0; i<hiddenNodes; i++) {
			hiddenLayersList.get(lastlayerIndex).getNodeList().add(new Node(i));
		} 
		return this;
	}

	// Create output layer for classification
	public FeedForward createOutputLayer(int outputNodes, ActivationType activation) {

		outputLayer = new Layer(NodeType.OUTPUT, activation);

		for(int i=0; i<outputNodes; i++) {
			outputLayer.getNodeList().add(new Node(i));
		}
		return this;
	}
	
	// Sample from a Normal distribution
	private double getRandomWeight() {
		return random.nextGaussian() * variance;
	}

	public FeedForward createConnections() {

		// Input-Hidden
		for(int i=0; i<inputLayer.getNodeList().size(); i++) {

			int from = inputLayer.getNodeList().get(i).getIndex();
			
			for(int j=0; j<hiddenLayersList.get(0).getNodeList().size(); j++) {
				
				int to = hiddenLayersList.get(0).getNodeList().get(j).getIndex();
				Connection c = new Connection(ConnectionType.TO_HIDDEN_NODE, from, to, getRandomWeight());
				hiddenLayersList.get(0).getNodeList().get(j).getConnectionList().add(c);
			}
		}

		// Hidden-Hidden
		for (int k=0; k<hiddenLayersList.size()-1; k++) {

			for(int i=0; i<hiddenLayersList.get(k).getNodeList().size(); i++) {

				int from = hiddenLayersList.get(k).getNodeList().get(i).getIndex();
				
				for(int j=0; j<hiddenLayersList.get(k+1).getNodeList().size(); j++) { 

					int to = hiddenLayersList.get(k+1).getNodeList().get(j).getIndex();
					Connection c = new Connection(ConnectionType.TO_HIDDEN_NODE, from, to, getRandomWeight());
					hiddenLayersList.get(k+1).getNodeList().get(j).getConnectionList().add(c);
				}
			}
		}

		// Bias-Hidden
		for (int i=0; i<hiddenLayersList.size(); i++) {

			int from = hiddenLayersList.get(i).getNodeList().size();
			
			for (int j=0; j<hiddenLayersList.get(i).getNodeList().size(); j++) {
				
				int to = hiddenLayersList.get(i).getNodeList().get(j).getIndex();
				Connection c = new Connection(ConnectionType.TO_BIAS_NODE, from, to, getRandomWeight());
				hiddenLayersList.get(i).getNodeList().get(j).setBiasConnection(c);
			}
		}

		// Hidden-Output
		for(int i=0; i<hiddenLayersList.get(hiddenLayersList.size()-1).getNodeList().size(); i++) {

			int from = hiddenLayersList.get(hiddenLayersList.size()-1).getNodeList().get(i).getIndex();
					
			for(int j=0; j<outputLayer.getNodeList().size(); j++) {
				
				int to = outputLayer.getNodeList().get(j).getIndex();
				Connection c = new Connection(ConnectionType.TO_BIAS_NODE, from, to, getRandomWeight());
				outputLayer.getNodeList().get(j).getConnectionList().add(c);
			}
		}
		
		int from = outputLayer.getNodeList().size();

		// Bias-Output
		for(int j=0; j<outputLayer.getNodeList().size(); j++) {
			
			int to = outputLayer.getNodeList().get(j).getIndex();
			Connection c = new Connection(ConnectionType.TO_BIAS_NODE, from, to, getRandomWeight());
			outputLayer.getNodeList().get(j).setBiasConnection(c);
		}
		return this;
	}
	
	public List<Double> getWeights(boolean isGradient) {

		List<Double> weightList = new ArrayList<>(); 
		List<Double> gradientList = new ArrayList<>(); 

		// Input-Hidden
		for(int i=0; i<inputLayer.getNodeList().size(); i++) {

			for(int j=0; j<hiddenLayersList.get(0).getNodeList().size(); j++) {
				weightList.add( hiddenLayersList.get(0).getNodeList().get(j).getConnectionList().get(i).getWeight() );
				gradientList.add( hiddenLayersList.get(0).getNodeList().get(j).getConnectionList().get(i).getGradient() );
			}
		}

		// Hidden-Hidden
		for (int k=0; k<hiddenLayersList.size()-1; k++) {

			for(int i=0; i<hiddenLayersList.get(k).getNodeList().size(); i++) {

				for(int j=0; j<hiddenLayersList.get(k+1).getNodeList().size(); j++) {
					weightList.add( hiddenLayersList.get(k+1).getNodeList().get(j).getConnectionList().get(i).getWeight() );
					gradientList.add( hiddenLayersList.get(k+1).getNodeList().get(j).getConnectionList().get(i).getGradient());
				}
			}
		}

		// Bias-Hidden
		for (int i=0; i<hiddenLayersList.size(); i++){

			for (int j=0; j<hiddenLayersList.get(i).getNodeList().size(); j++){
				weightList.add( hiddenLayersList.get(i).getNodeList().get(j).getBiasConnection().getWeight() );
				gradientList.add( hiddenLayersList.get(i).getNodeList().get(j).getBiasConnection().getGradient() );
			}
		}

		// Hidden-Output
		for(int i=0; i<hiddenLayersList.get(hiddenLayersList.size()-1).getNodeList().size(); i++) {

			for(int j=0; j<outputLayer.getNodeList().size(); j++) {
				weightList.add( outputLayer.getNodeList().get(j).getConnectionList().get(i).getWeight() );
				gradientList.add( outputLayer.getNodeList().get(j).getConnectionList().get(i).getGradient() );
			}
		}

		// Bias-Output
		for(int j=0; j<outputLayer.getNodeList().size(); j++){
			weightList.add( outputLayer.getNodeList().get(j).getBiasConnection().getWeight() );
			gradientList.add( outputLayer.getNodeList().get(j).getBiasConnection().getGradient() );
		}
		if(isGradient) return gradientList; 
		return weightList;
	}

	public List<Double> setWeights(List<Double> weightList) { 

		int index = 0;

		// Input-Hidden
		for(int i=0; i<inputLayer.getNodeList().size(); i++) {

			for(int j=0; j<hiddenLayersList.get(0).getNodeList().size(); j++) {
				hiddenLayersList.get(0).getNodeList().get(j).getConnectionList().get(i).setWeight( weightList.get(index) );
				index = index + 1;
			}
		}

		// Hidden-Hidden
		for (int k=0; k<hiddenLayersList.size()-1; k++) {

			for(int i=0; i<hiddenLayersList.get(k).getNodeList().size(); i++) {

				for(int j=0; j<hiddenLayersList.get(k+1).getNodeList().size(); j++){
					hiddenLayersList.get(k+1).getNodeList().get(j).getConnectionList().get(i).setWeight( weightList.get(index) );
					index = index + 1;
				}
			}
		}

		// Bias-Hidden
		for (int i=0; i<hiddenLayersList.size(); i++) {

			for (int j=0; j<hiddenLayersList.get(i).getNodeList().size(); j++){
				hiddenLayersList.get(i).getNodeList().get(j).getBiasConnection().setWeight( weightList.get(index) );
				index = index + 1;
			}
		}

		// Hidden-Output
		for(int i=0; i<hiddenLayersList.get(hiddenLayersList.size()-1).getNodeList().size(); i++) {

			for(int j=0; j<outputLayer.getNodeList().size(); j++) {
				outputLayer.getNodeList().get(j).getConnectionList().get(i).setWeight( weightList.get(index) ) ;
			}
		}

		// Bias-Output
		for(int j=0; j<outputLayer.getNodeList().size(); j++){
			outputLayer.getNodeList().get(j).getBiasConnection().setWeight( weightList.get(index) );
			index = index + 1;
		}
		return weightList;
	}
	
	// *** Feed Forward *** 

	// Set input node values
	protected FeedForward setInputNodeValues(List<Double> data) { 
	
		inputLayer.getNodeList().forEach( inputNode ->{			
			inputNode.setValue( data.get(inputNode.getIndex()) );
		});
		return this;
	}

	// Set hidden node values
	// Activate hidden node values
	public FeedForward setHiddenNodeValues() {

		// First hidden layer
		hiddenLayersList.get(0).getNodeList().forEach( hiddenNode ->{
			// Pass input layer to first hidden layer
			hiddenNode.calculateValue(inputLayer);
			hiddenNode.setActivatedValue( hiddenLayersList.get(0).getActivation().getFunctionValue( hiddenNode.getValue() ));
		});

		// Deeper hidden layers
		for(int i=1; i<hiddenLayersList.size(); i++) {

			int index = i;
			hiddenLayersList.get(i).getNodeList().forEach( hiddenNode ->{
				// Pass previous hidden layer to current hidden layer
				hiddenNode.calculateValue(hiddenLayersList.get(index-1));
				hiddenNode.setActivatedValue( hiddenLayersList.get(index).getActivation().getFunctionValue(hiddenNode.getValue()) );
			});
		}
		return this;
	}

	// Set output node values
	public FeedForward setOutputNodeValues() {

		outputLayer.getNodeList().forEach( outputNode -> {
			// Pass last hidden layer to output layer
			outputNode.calculateValue(hiddenLayersList.get(hiddenLayersList.size()-1));
			outputNode.setActivatedValue( outputLayer.getActivation().getFunctionValue( outputNode.getValue() ));
		});
		return this;
	}
	
	// Set desired output node values
	public FeedForward setExpectedOutputNodeValues(List<Double> data){

		outputLayer.getNodeList().forEach( outputNode -> {			
			outputNode.setDesiredValue( data.get(outputNode.getIndex()) );
		});
		return this;
	}
	
	// Calculate error for each output node 
	public FeedForward calculateOutputLayerError() {

		outputLayer.getNodeList().forEach( outputNode -> {
			outputNode.calculateError();
		});
		
		double sum = 0.0;
		for(Node outputNode : outputLayer.getNodeList()) {
			sum = sum + outputNode.getError();
		}
		
		// Total error is not used in calculations
		this.error = ( sum / outputLayer.getNodeList().size() ); 
		return this;
	}
		
	public FeedForward setCostFunction(CostFunctionType type) {
		this.costFunction = new CostFunctionFactory().get(type);
		
		outputLayer.getNodeList().forEach( outputNode -> {
			outputNode.setCostFunction(costFunction);
		});
		return this; 
	}
	
	public FeedForward setOptimizer(OptimizationAlgorithmType type) {
		optimizer = new OptimizationAlgorithmFactory().get(type);
		return this;
	}
		
	public void feedForward(List<Double> inputRow, List<Double> outputRow) {			
		setInputNodeValues( inputRow )
		.setHiddenNodeValues()
		.setOutputNodeValues()
		.setExpectedOutputNodeValues( outputRow )
		.calculateOutputLayerError();
	}
	
	public List<Double> feedForward(List<Double> inputRow) {			
		setInputNodeValues( inputRow )
		.setHiddenNodeValues()
		.setOutputNodeValues();
		
		List<Double> output = new ArrayList<>();
		
		outputLayer.getNodeList().forEach( outputNode -> {
			output.add(outputNode.getActivatedValue());
		});
		return output;
	}
		
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public long getSeed() {
		return seed;
	}

	public void setSeed(long seed) {
		this.seed = seed;
	}
	
	public int getBatch() {
		return batch;
	}
	
	public FeedForward setBatch(int batch) {
		this.batch = batch;
		return this;
	}
	
	public double getVariance() {
		return variance;
	}

	public FeedForward setVariance(double variance) {
		this.variance = variance;		
		return this;
	}
	
	public CostFunction getCostFunction() {
		return costFunction;
	}
	
	public void setCostFunction(CostFunction costFunction) {
		this.costFunction = costFunction;
	}
	
	public Optimizer getOptimizer() {
		return optimizer;
	}
		
	public void setOptimizer(Optimizer optimizer) {
		this.optimizer = optimizer;
	}

	public Layer getInputLayer() {
		return inputLayer;
	}
	
	public void setInputLayer(Layer inputLayer) {
		this.inputLayer = inputLayer;
	}

	public ArrayList<Layer> getHiddenLayersList() {
		return hiddenLayersList;
	}
	
	public void setHiddenLayersList(ArrayList<Layer> hiddenLayersList) {
		this.hiddenLayersList = hiddenLayersList;
	}

	public Layer getOutputLayer() {
		return outputLayer;
	}
	
	public void setOutputLayer(Layer outputLayer) {
		this.outputLayer = outputLayer;
	}

	public double getError() {
		return error;
	}
		
	@Override 
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("id: ").append(id);
		sb.append("\nseed: ").append(seed);
		sb.append("\nbatch: ").append(batch);
		sb.append("\nvariance: ").append(variance);
		sb.append("\ncostFunction: ").append(costFunction);
		sb.append("\noptimizer: ").append(optimizer);
		sb.append("\ninputLayer: ").append(inputLayer);
		sb.append("\nhiddenLayersList: ").append(hiddenLayersList);
		sb.append("\noutputLayer: ").append(outputLayer);
		
		String jsonStr = null; 
		ObjectMapper mapper = new ObjectMapper();
		
        try {
            jsonStr = mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
		return jsonStr;
	}
}
