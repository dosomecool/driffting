package com.drifft.service.neuralnetwork.utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class DataUtility {
	
	private Double[][] input;
	private Double[][] output;
	
	private Double[] time;
	private Double[] state;
	
	private int rows;
	
	private long seed = new Random().nextLong();
	private double percent = 0.75;
	private boolean shuffle = false;
	
	private List<Integer> sequence = new ArrayList<>();
	
	private List<Integer> trainingSequence = new ArrayList<>();
	private List<Integer> validationSequence = new ArrayList<>();
	
	private boolean valid = true;
	
	
	public DataUtility(Double[] timeData, Double[] stateData, Double[][] input, Double[][] outputData) throws Exception {
		
		this.time = timeData;
		this.state = stateData;
		this.input = input; 
		this.output = outputData;
		
//		if(this.input == null && this.input.length == 0) this.valid = false;
//		if(this.output == null && this.output.length == 0) this.valid = false;
//		if(this.input.length == this.output.length) {
//			this.rows = this.input.length;
//		} else {
//			this.valid = false;
//		}

		System.out.println("this.input.length: " + this.input.length);
		System.out.println("this.output.length: " + this.output.length);
		System.out.println("isValid: " + this.valid);
		
//		if(valid) {

		this.rows = this.input.length;
			for(int i=5; i<rows; i++) {
				sequence.add(i);
			}
			setSequence();
//		}
	}
	
	public DataUtility(Double[] timeData, Double[] stateData, Double[][] input, Double[] outputData) throws Exception {
		
		this.time = timeData;
		this.state = stateData;
		this.input = input;
		System.out.println("Length of data: " + outputData.length);
		this.output = new Double[1][outputData.length];
		this.output[0] = outputData;
		
//		if(this.input == null && this.input.length == 0) this.valid = false;
//		if(this.output == null && this.output.length == 0) this.valid = false;
//		if(this.input.length == this.output.length) {
//			this.rows = this.input.length;
//		} else {
//			this.valid = false;
//		}

		System.out.println("this.input.length: " + this.input.length);
		System.out.println("this.output.length: " + this.output.length);
		System.out.println("isValid: " + this.valid);
		
//		if(valid) {

		this.rows = this.input.length;
			for(int i=5; i<rows; i++) {
				sequence.add(i);
			}
			setSequence();
//		}
	}
	
	private Double[][] transpose(Double[][] data) {
		int rows = data.length;       //index 
		int cols = data[0].length;    //time
		Double[][] transpose = new Double[cols][rows];
		
        for (int j=0; j<cols; j++) {
            for (int i=0; i<rows; i++) {
            	transpose[j][i] = data[i][j];
            }
        }
        return transpose;
	}
	
	public boolean isValid() {
		return this.valid;
	}
	
	public DataUtility setTrainingPercent(Double percent) {
		if(percent != null && percent >= 0.0 && percent <= 1.0) this.percent = percent;
		return this;
	}
	
	public DataUtility setShuffle(Boolean shuffle, Long seed) {
		if(shuffle != null && shuffle == true) this.shuffle = shuffle;
		if(seed != null) this.seed = seed;
		return this;
	}
	
	public DataUtility apply() {
//		shuffle();
		return this;
	}
	
	private void shuffle() {
		if (shuffle) { 
			Collections.shuffle(sequence, new Random(seed));
			setSequence();
		}
	}

	private void setSequence() {
		int length = (int) Math.rint( percent * sequence.size() );		
		Integer[] tmp = sequence.toArray(new Integer[0]);
		
		// TODO: for mini batch split into more pieces
//		trainingSequence = Arrays.asList(Arrays.copyOfRange(tmp, 0, length));
//		validationSequence = Arrays.asList(Arrays.copyOfRange(tmp, length, sequence.size()));
	}
	
	public List<Integer> getSequence() {
		return sequence;
	}
	
	// TODO: fix this
	public List<Integer> getTrainingSequence() {
		return sequence;
//		return trainingSequence;
	}
	
	public List<Integer> getValidationSequence() {
		return validationSequence;
	}
	
	public List<Double> getInputRow(int index) {
        return Arrays.asList(input[index]);
	}
	
	public List<Double> getOutputRow(int index) {
        return Arrays.asList(output[index]);
	}
	
	public int getInputSize() {
		return input[0].length;
	}
	
	public int getOutputSize() {
		return output[0].length;
	}

	public Double[] getTime() {
		return time;
	}

	public void setTime(Double[] time) {
		this.time = time;
	}

	public Double[] getState() {
		return state;
	}

	public void setState(double[] space) {
		this.state = state;
	}
}
