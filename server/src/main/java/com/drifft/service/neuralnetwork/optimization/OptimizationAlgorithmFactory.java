package com.drifft.service.neuralnetwork.optimization;

import com.drifft.service.neuralnetwork.network.Types.OptimizationAlgorithmType;

public class OptimizationAlgorithmFactory {
	
   
	public OptimizationAlgorithmFactory() {
	}
	
	public Optimizer get(OptimizationAlgorithmType type){

		if(type == null) {
			return null;
		}

		if(type.equals(OptimizationAlgorithmType.VANILLA)) {			
			return new VanillaGradientDecent();

		} else if(type.equals(OptimizationAlgorithmType.MOMENTUM)) {
			return new NesterovMomentum();     
		} 
		return null;
	}
}
