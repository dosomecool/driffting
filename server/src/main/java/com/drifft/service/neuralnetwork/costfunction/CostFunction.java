package com.drifft.service.neuralnetwork.costfunction;

import com.drifft.service.neuralnetwork.network.Types.CostFunctionType;

public interface CostFunction {
	public double getErrorValue(double yhat, double y);
	public double getErrorDerivativeWrtActivatedValue(double yhat, double y);
	public CostFunctionType getCostFunction();
}
