package com.drifft.service.neuralnetwork.network;

public class Types {

	public enum ConnectionType {
	    TO_HIDDEN_NODE, TO_OUTPUT_NODE, TO_BIAS_NODE
	}
	
	public enum NodeType {
	    INPUT, HIDDEN, OUTPUT, BIAS
	}

	public enum ActivationType {
		ELU, RELU, LEAKY_RELU, TANH, SIGMOID, LINEAR, GAUSSIAN
	}
	
	public enum OptimizationAlgorithmType {
		VANILLA, MOMENTUM
	}
	
	public enum CostFunctionType {
		MSE, MAD, CROSS_ENTROPY
	}
	
	public enum Objective {
		REGRESSION, CLASSIFICATION
	}
	
	public enum TargetFunctionType {
		LINE, QUADRATIC
	}
	
	public enum TrainingAlgorithmType {
		BACKPROPAGATION
	}
}
