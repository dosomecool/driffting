package com.drifft.service.neuralnetwork.visualization;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

public class VisNode {

	private int id;
	private String label;
	private int group;
	private double x;
	private double y;
	
	public VisNode() {
	}
	
	public VisNode(int id, String label, int group, double x, double y) {
		this.id = id;
		this.label = label;
		this.group = group;
		this.x = x;
		this.y = y;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getGroup() {
		return group;
	}

	public void setGroup(int group) {
		this.group = group;
	}
	
	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	@Override 
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("id: ").append(id);
		sb.append("\nlabel: ").append(label);
		sb.append("\ngroup: ").append(group);
		sb.append("\nx: ").append(x);
		sb.append("\ny: ").append(y);
		
		String jsonStr = null; 
		ObjectMapper mapper = new ObjectMapper();
		
        try {
            jsonStr = mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
		return jsonStr;
	}
}
