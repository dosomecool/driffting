package com.drifft.service.neuralnetwork.exceptions;

public class IllegalStateException extends Exception {

	private static final long serialVersionUID = 1L;

	public IllegalStateException(String message) {
        super(message);
    }
}
