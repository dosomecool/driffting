package com.drifft.service.neuralnetwork.activation;

import com.drifft.service.neuralnetwork.network.Types.ActivationType;

public class Linear implements Function {

	@Override
	public double getFunctionValue(double x) {
		return x;
	}

	@Override
	public double getFunctionDerivativeValue(double x) {
		return 1.0;
	}

	@Override
	public ActivationType getActivation() {
		return ActivationType.LINEAR;
	}
}
