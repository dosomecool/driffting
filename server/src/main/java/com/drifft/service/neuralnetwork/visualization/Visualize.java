package com.drifft.service.neuralnetwork.visualization;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import com.drifft.service.neuralnetwork.network.FeedForward;
import com.drifft.service.neuralnetwork.network.Layer;
import com.drifft.service.neuralnetwork.network.Node;

public class Visualize {
	
	// Layers
	public Layer inputLayer;
	public ArrayList<Layer> hiddenLayersList = new ArrayList<Layer>();
	public Layer outputLayer;
	
	public Visualize(FeedForward feedForward) {
		this.inputLayer = feedForward.getInputLayer();
		this.hiddenLayersList = feedForward.getHiddenLayersList();
		this.outputLayer = feedForward.getOutputLayer();
	}
	
	// Support for vis.org network graph
	private NetworkNodesAndEdges visualization; 

	public NetworkNodesAndEdges setView() {
		List<VisNode> nodes = new ArrayList<VisNode>();
		List<VisEdge> edges = new ArrayList<VisEdge>();
		
		for(Node node : inputLayer.getNodeList()) {
			
			int id = node.getIndex();
			String nodeLabel = Double.toString(node.getValue());
			double y = 200 * id;
			nodes.add(new VisNode(id, nodeLabel, 0, 0, y)); 
		}
		
		for(int i=1; i<=hiddenLayersList.size(); i++) {
			
			for(int j=0; j<hiddenLayersList.get(i-1).getNodeList().size(); j++) {
				
				int id = hiddenLayersList.get(i-1).getNodeList().get(j).getIndex();
				String nodeLabel = Double.toString(hiddenLayersList.get(i-1).getNodeList().get(j).getActivatedValue());
				double x = 400 * i;
				double y = 200 * j;
				nodes.add(new VisNode(id, nodeLabel, 1, x, y)); 
				
				hiddenLayersList.get(i-1).getNodeList().get(j).getConnectionList().forEach( c -> {
					
					int from = c.getFromNodeIndex();
					int to = c.getToNodeIndex();
					String edgeLabel = new DecimalFormat("#.##").format(c.getWeight()); 
					edges.add(new VisEdge(from, to, edgeLabel));
				});
			}
			
			int last = hiddenLayersList.get(i-1).getNodeList().size() - 1;
			int id = hiddenLayersList.get(i-1).getNodeList().get(last).getIndex() + 1;
			String nodeLabel = Double.toString(1.0);
			double x = 200 + 400 * (i-1);
			double y = 200 * hiddenLayersList.get(i-1).getNodeList().size();
			nodes.add(new VisNode(id, nodeLabel, 2, x, y)); 
		}
		
		for(int i=0; i<outputLayer.getNodeList().size(); i++) {
			
			int id = outputLayer.getNodeList().get(i).getIndex();
			String nodeLabel = new DecimalFormat("#.##").format(inputLayer.getNodeList().get(i).getActivatedValue());
			double x = 400 * (hiddenLayersList.size() + 1);
			double y = 200 * i;
			nodes.add(new VisNode(id, nodeLabel, 3, x, y)); 
		}
		
		int last = outputLayer.getNodeList().size() - 1;
		int id = outputLayer.getNodeList().get(last).getIndex() + 1;

		String nodeLabel = Double.toString(1.0);
		double x = 200 + 400 * hiddenLayersList.size();
		double y = 200 * outputLayer.getNodeList().size();
		nodes.add(new VisNode(id, nodeLabel, 2, x, y)); 
		
		return (visualization = new NetworkNodesAndEdges(nodes, edges));
	}
	
	public NetworkNodesAndEdges updateView() {
		
        // Update input layer
		for(Node node : inputLayer.getNodeList()) {
			
			int id = node.getIndex();
			String nodeLabel = Double.toString(node.getValue());
			visualization.getNodes().get(id).setLabel(nodeLabel);
		}
		
		visualization.getEdges().clear();
		
		// Update hidden layers
		for(int i=1; i<=hiddenLayersList.size(); i++) {
			
			for(int j=0; j<hiddenLayersList.get(i-1).getNodeList().size(); j++) {
				
				int id = hiddenLayersList.get(i-1).getNodeList().get(j).getIndex();
				String nodeLabel = Double.toString(hiddenLayersList.get(i-1).getNodeList().get(j).getActivatedValue());			
				visualization.getNodes().get(id).setLabel(nodeLabel);
		
				hiddenLayersList.get(i-1).getNodeList().get(j).getConnectionList().forEach( c -> {
										
					int from = c.getFromNodeIndex();
					int to = c.getToNodeIndex();
					String edgeLabel = new DecimalFormat("#.##").format(c.getWeight()); 
					visualization.getEdges().add(new VisEdge(from, to, edgeLabel));
				});
			}
			
			// Update bias node
			int last = hiddenLayersList.get(i-1).getNodeList().size() - 1;
			int id = hiddenLayersList.get(i-1).getNodeList().get(last).getIndex() + 1;
			String nodeLabel = Double.toString(1.0);
			visualization.getNodes().get(id).setLabel(nodeLabel);
		}
		
		// Update output layer
		for(int i=0; i<outputLayer.getNodeList().size(); i++) {
			
			int id = outputLayer.getNodeList().get(i).getIndex();
			String nodeLabel = new DecimalFormat("#.##").format(inputLayer.getNodeList().get(i).getActivatedValue());
			visualization.getNodes().get(id).setLabel(nodeLabel);
		}
		
		// Update output bias node
		int last = outputLayer.getNodeList().size() - 1;
		int id = outputLayer.getNodeList().get(last).getIndex() + 1;

		String nodeLabel = Double.toString(1.0);
		visualization.getNodes().get(id).setLabel(nodeLabel);
		
		return visualization; 
	}
	
}
