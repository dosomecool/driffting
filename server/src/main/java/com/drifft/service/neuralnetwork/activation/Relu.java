package com.drifft.service.neuralnetwork.activation;

import com.drifft.service.neuralnetwork.network.Types.ActivationType;

public class Relu implements Function {

	public Relu() {
	}
	
	@Override
	public double getFunctionValue(double x) {
		return Math.log( 1.0 + Math.exp(x) ); // RELU approximation
	}

	@Override
	public double getFunctionDerivativeValue(double x) {
		return ( 1 / (1 + Math.exp(-x)) );
	}
	
	public ActivationType getActivation() {
		return ActivationType.RELU;
	}
}
