package com.drifft.service.neuralnetwork.utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.drifft.constant.Constants.Request;
import com.drifft.service.utilities.Data;

public class Action {

	private Request request; 
	// List is working as a database
	List<Data<?>> dataset;

	public Action(Request request) {
		this.request = request;
		dataset = new ArrayList<Data<?>>();	
	}

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public <T> Action addData(Data<?> data) {
		
		if (data.getType() == null) throw new NullPointerException("Type is null");
		dataset.add(data);	
		return this;
	}

	@SuppressWarnings("unchecked")
	public <T> T getData(UUID uuid, String name) {

		for(Data<?> d : dataset) {

			if(d.getUuid() == uuid && d.getName().equals(name)) return (T) d.cast();
		}
		return null;
	}

	public void delete(UUID uuid, String name) {

		for(Data<?> d : dataset) {

			if(d.getUuid() == uuid && d.getName().equals(name)) dataset.remove(d);
		}
	}

	//Retrieve list of data from the database
	public List<Data<?>> getAllData() {
		return dataset;
	}

	// Not used for now  
	//	public <T> Action putData(Data<T> type, T instance) {
	//		dataSet.putData(type, instance);
	//		return this;
	//	}
	//	
	//	public <T> T getData(Data<T> type) {
	//		return dataSet.getData(type);
	//	}
}
