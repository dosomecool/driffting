package com.drifft.service.neuralnetwork.activation;

import com.drifft.service.neuralnetwork.network.Types.ActivationType;

public class Elu implements Function {

	// https://github.com/aleju/papers/blob/master/neural-nets/ELUs.md
	
	double alpha = 0.5;
	
	public Elu() {
	}
	
	@Override
	public double getFunctionValue(double x) {
		if( x > 0 ) return x;
		return alpha * ( Math.exp(x) - 1 );
	}

	@Override
	public double getFunctionDerivativeValue(double x) {
		if( x > 0 ) return 1.0;
		return getFunctionValue(x) + alpha;
	}

	@Override
	public ActivationType getActivation() {
		return ActivationType.ELU;
	}
}
