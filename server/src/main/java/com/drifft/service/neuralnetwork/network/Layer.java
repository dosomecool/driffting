package com.drifft.service.neuralnetwork.network;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.drifft.service.neuralnetwork.activation.ActivationFunctionFactory;
import com.drifft.service.neuralnetwork.activation.Function;
import com.drifft.service.neuralnetwork.network.Types.ActivationType;
import com.drifft.service.neuralnetwork.network.Types.NodeType;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Layer {

	private String id;
	private int index;
	private NodeType nodeType;

	// Hidden layer or Output layer
	private Function activation;

	private List<Node> nodeList = new ArrayList<>();
	
	public Layer() {
	}
	
	// For input layer only
	public Layer(NodeType nodeType) {
		this.nodeType = nodeType;
	}
	
	// For hidden and output layer
	public Layer(NodeType nodeType, ActivationType activationType) {
		this.nodeType = nodeType;
		this.activation = new ActivationFunctionFactory().getActivation(activationType);
	}
	
	// For importing an existing model
	public Layer(String id, int index, NodeType nodeType, Function activation, List<Node> nodeList) {
		this.id = id;
		this.index = index;
		this.nodeType = nodeType;
		this.activation = activation;
		this.nodeList = nodeList;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}
	
	public NodeType getNodeType() {
		return nodeType;
	}
	
	public void setNodeType(NodeType nodeType) {
		this.nodeType = nodeType;
	}
	
	public Function getActivation() {
		return activation;
	}
	
	public void setActivation(Function activation) {
		this.activation = activation;
	}
	
	public List<Node> getNodeList() {
		return nodeList;
	}
	
	public void setNodeList(List<Node> nodeList) {
		this.nodeList = nodeList;
	}

	@Override 
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("id: ").append(id);
		sb.append("\nindex: ").append(index);
		sb.append("\nnodeType: ").append(nodeType);
		sb.append("\nactivation: ").append(activation);
		sb.append("\nnodeList: ").append(nodeList);
		
		String jsonStr = null; 
		ObjectMapper mapper = new ObjectMapper();
		
        try {
            jsonStr = mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
		return jsonStr;
	}
}
