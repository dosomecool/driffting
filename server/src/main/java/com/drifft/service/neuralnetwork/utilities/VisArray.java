package com.drifft.service.neuralnetwork.utilities;

import java.util.ArrayList;
import java.util.List;

import com.drifft.domain.Graph3DData;
import com.drifft.domain.Graph3DValue;

public class VisArray {

	int maxPoints = 500000;

	double[] time;
	double[] state;
	Double[][] data; 
	
	List<Graph3DValue> input3d = new ArrayList<>();
	
	public VisArray(double[] time, double[] state, Double[][] data) {
		this.time = time;
		this.state = state;
		this.data = data;
	} 
	
	public Graph3DData getData() {
		
		// time 
		for(int j=0; j<data.length; j = j + 10) {
			
			// state
			for(int i=0; i<data[0].length; i = i + 10) {
				
				double t = time[j];
				double x = state[i] + state[i+1] / 2; 
				double value = data[j][i];
				
				if(value >= 0.01 ) value = 0.01;
				input3d.add(new Graph3DValue(t, x, value));
			}
		}
		return new Graph3DData("input-3d", input3d);
	}
}
