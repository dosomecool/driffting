package com.drifft.service.neuralnetwork.network;

import java.io.IOException;
import java.util.UUID;

import com.drifft.service.neuralnetwork.network.Types.ConnectionType;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Connection {
	
	private String id = UUID.randomUUID().toString();
	
	private ConnectionType type;
	private int fromNodeIndex;     // Index of originating node
	private int toNodeIndex;       // Index of destination node
	private double weight;         // Value of the weight
	private double gradient;       // Change of the weight 
	
	public Connection() {
	}
	
	public Connection(ConnectionType type, int fromNodeIndex, int toNodeIndex, double weight) {
		this.type = type;
		this.fromNodeIndex = fromNodeIndex;
		this.toNodeIndex = toNodeIndex;
		this.weight = weight;
	}
	
	public Connection(String id, ConnectionType type, int fromNodeIndex, int toNodeIndex, double weight) {
		this.id = id;
		this.type = type;
		this.fromNodeIndex = fromNodeIndex;
		this.toNodeIndex = toNodeIndex;
		this.weight = weight;
	}
		
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
		
	public ConnectionType getType() {
		return type;
	}
	
	public void setType(ConnectionType type) {
		this.type = type;
	}
		
	public int getFromNodeIndex() {
		return fromNodeIndex;
	}
	
	public void setFromNodeIndex(int fromNodeIndex) {
		this.fromNodeIndex = fromNodeIndex;
	}

	public int getToNodeIndex() {
		return toNodeIndex;
	}
	
	public void setToNodeIndex(int toNodeIndex) {
		this.toNodeIndex = toNodeIndex;
	}
	
	public double getWeight() {
		return weight;
	}
	
	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getGradient() {
		return gradient;
	}

	public void setGradient(double gradient) {
		this.gradient = gradient;
	}
	
	@Override 
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("id: ").append(id);
		sb.append("\ntype: ").append(type);
		sb.append("\nfromNodeIndex: ").append(fromNodeIndex);
		sb.append("\ntoNodeIndex: ").append(toNodeIndex);
		sb.append("\nweight: ").append(weight);
		sb.append("\ngradient: ").append(gradient);
		
		String jsonStr = null; 
		ObjectMapper mapper = new ObjectMapper();
		
        try {
            jsonStr = mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
		return jsonStr;
	}
}
