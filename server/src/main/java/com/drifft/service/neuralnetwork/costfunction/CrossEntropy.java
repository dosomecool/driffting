package com.drifft.service.neuralnetwork.costfunction;

import com.drifft.service.neuralnetwork.network.Types.CostFunctionType;

public class CrossEntropy implements CostFunction {
	
	// https://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence#Symmetrised_divergence
	
	public CrossEntropy() {
	}
	
	@Override
	public double getErrorValue(double yhat, double y) {
		
		double m = 0.5 * (y + yhat);
		
//		System.out.println("y: " + y + " yhat: " + yhat + " error: " + ( 0.5 * (y * Math.log(y/m)) + 0.5 * (yhat * Math.log(yhat/m))) );
		
		if(yhat == 0.0 || y == 0.0) {
			return 0.0;
		}
		return ( 0.5 * (y * Math.log(y/m)) + 0.5 * (yhat * Math.log(yhat/m)) );
	}

	@Override
	public double getErrorDerivativeWrtActivatedValue(double yhat, double y) {
//		return -1.0 * ( y / yhat); 
		return 0.5 * Math.log( (2 * yhat) / (y + yhat) );
	}

	@Override
	public CostFunctionType getCostFunction() {
		return CostFunctionType.CROSS_ENTROPY;
	}
}
