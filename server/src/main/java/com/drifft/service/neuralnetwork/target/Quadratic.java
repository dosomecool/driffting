package com.drifft.service.neuralnetwork.target;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.drifft.service.neuralnetwork.network.Types.TargetFunctionType;

public class Quadratic implements TargetFunction {

	// y = lambda * x * x
	private final TargetFunctionType type = TargetFunctionType.QUADRATIC;
	
	private double lambda = 10.0;
	
	private final Map<String, Double> params = new HashMap<>();
	
	public Quadratic() {
		params.put("lambda", lambda);
	}
	
	@Override
	public double getFunctionValue(double x) {
		return params.get("lambda") * Math.pow(x, 2);
	}
	
	@Override
	public double getFunctionDerivativeValue(double x, String param) {
		return Math.pow(x, 2);
	}
		
	@Override
	public Map<String, Double> getParams() {
		return params;
	}

	@Override
	public List<String> getParameterNames() {
		return new ArrayList<String>(params.keySet());
	}

	@Override
	public TargetFunctionType getType() {
		return this.type;
	}
}
