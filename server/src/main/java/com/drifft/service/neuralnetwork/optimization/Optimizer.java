package com.drifft.service.neuralnetwork.optimization;

public interface Optimizer {

	public double updateParameter(String id, double weight, double gradient);
	public void update(boolean update);
}
