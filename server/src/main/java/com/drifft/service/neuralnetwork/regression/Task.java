package com.drifft.service.neuralnetwork.regression;

import java.util.concurrent.atomic.AtomicBoolean;

public abstract class Task implements Runnable {

	private AtomicBoolean running = new AtomicBoolean(false);
	private AtomicBoolean stopped = new AtomicBoolean(false);
	private AtomicBoolean success = new AtomicBoolean(false);
	
	public abstract boolean calculate();
	public abstract void onSuccess();
	public abstract void onFail();
	public abstract void onStop();

    public void stop() {
    	stopped.set(false);
    }
    
    public boolean isStopped() {
    	return stopped.get();
    }
   
	boolean isRunning() {
		return running.get();
	}
	
	boolean succeeded() {
		return success.get();
	}

    @Override
    public void run() {
    	running.set(true);

    	try {
    		success.set(calculate());
    	} catch (Exception e) {
    		success.set(false);
    	}
    	
    	if(success.get()) {
    		onSuccess();
    	} else {
    		onFail();
    	}
    	running.set(false);
    }
}
