package com.drifft.service.neuralnetwork.target;

import java.util.List;
import java.util.Map;

import com.drifft.service.neuralnetwork.network.Types.TargetFunctionType;

public interface TargetFunction {

	public double getFunctionValue(double x);
	public double getFunctionDerivativeValue(double x, String param);
	public Map<String, Double> getParams();
	public List<String> getParameterNames();
	public TargetFunctionType getType();
}
