package com.drifft.service.neuralnetwork.troubleshoot;

import java.io.IOException;

import com.drifft.service.neuralnetwork.network.FeedForward;

public class DevOnlyTroubleshoot {

	// *** Debug ***

	private FeedForward feedForward;

	public DevOnlyTroubleshoot(FeedForward feedForward) {
		this.feedForward = feedForward;
	}

	public void debug(int iteration) {

		System.out.printf("%n %-15S \n", "********** ITERATION " + iteration + " **********");
		System.out.printf("%n %-15S \n", "* INPUT LAYER *");

		// Input layer
		System.out.printf("%n %-15S  %-15S", "NODE", "VALUE"); 
		feedForward.getInputLayer().getNodeList().forEach( inputNode ->{

			System.out.printf("%n %-15d %-15.3f", inputNode.getIndex(), inputNode.getValue());
		});

		System.out.printf("\n"); 

		// Hidden layer		
		feedForward.getHiddenLayersList().forEach( hiddenLayer ->{

			System.out.printf("%n %-15S %n", "** HIDDEN LAYER " + hiddenLayer.getIndex() + " **");

			hiddenLayer.getNodeList().forEach( hiddenNode ->{

				System.out.printf("%n %-15S %-15S %-15S", "NODE", "VALUE", "ACT. VALUE");
				System.out.printf("%n %-15d %-15.3f %-15.3f %n", hiddenNode.getIndex(), hiddenNode.getValue(), hiddenNode.getActivatedValue());

				System.out.printf("%n %-15S %n", "--- CONNECTIONS ---");
				System.out.printf("%n %-15S %-15S %-15S", "FROM", "TO", "WEIGHT");
				hiddenNode.getConnectionList().forEach( connection ->{

					System.out.printf("%n %-15d %-15d %-15.5f", connection.getFromNodeIndex(), connection.getToNodeIndex(), connection.getWeight());
				});
				System.out.printf("\n"); 
			});
		});

		// Output layer	
		System.out.printf("%n %-15S %n", "*** OUTPUT LAYER ***");

		feedForward.getOutputLayer().getNodeList().forEach(outputNode ->{

			System.out.printf("%n %-15S %-15S %-15S", "NODE", "VALUE", "ACT. VALUE");
			System.out.printf("%n %-15d %-15.3f %-15.3f %n", outputNode.getIndex(), outputNode.getValue(), outputNode.getActivatedValue());

			System.out.printf("%n %-15S %n", "--- CONNECTIONS ---");
			System.out.printf("%n %-15S %-15S %-15S", "FROM", "TO", "WEIGHT");
			outputNode.getConnectionList().forEach( connection ->{

				System.out.printf("%n %-15d %-15d %-15.3f", connection.getFromNodeIndex(), connection.getToNodeIndex(), connection.getWeight());
			});
			System.out.printf("\n");
		});

		System.out.printf("%n %-15S %-15.3f %n%n", "TOTAL ERROR: ", feedForward.getError()); 

		pressAnyKeyToContinue();
	}

	private void pressAnyKeyToContinue() {

		System.out.println("Press \"ENTER\" key to continue...");
		try {
			System.in.read(new byte[2]);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
