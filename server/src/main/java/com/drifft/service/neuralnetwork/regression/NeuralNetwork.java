package com.drifft.service.neuralnetwork.regression;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.drifft.constant.Constants.Queue;
import com.drifft.constant.Constants.Request;
import com.drifft.domain.Graph2DData;
import com.drifft.domain.Graph2DValue;
import com.drifft.domain.Status;
import com.drifft.domain.UserInput;
import com.drifft.service.neuralnetwork.backpropagation.MultiThreadBackpropagation;
import com.drifft.service.neuralnetwork.network.FeedForward;
import com.drifft.service.neuralnetwork.network.Types.ActivationType;
import com.drifft.service.neuralnetwork.network.Types.CostFunctionType;
import com.drifft.service.neuralnetwork.network.Types.OptimizationAlgorithmType;
import com.drifft.service.neuralnetwork.utilities.Action;
import com.drifft.service.neuralnetwork.utilities.DataUtility;
import com.drifft.websockets.WebSocketController;

@Service
public class NeuralNetwork implements Runnable {

	private UserInput userInput;

	private BlockingQueue<Action> netQueue;

	@Autowired
	private WebSocketController webSocketController;
	
	private DataUtility data;
	private FeedForward feedForward;

	private Action action;
	private double elapsedTime;

	private boolean initialized = false;

	private Task trainingTask;

	public NeuralNetwork() {
	}

	public NeuralNetwork setInput(UserInput userInput) {
		this.userInput = userInput;
		return this;
	}

	public NeuralNetwork setNetQueue(BlockingQueue<Action> netQueue) {
		this.netQueue = netQueue;
		return this;
	}

	private ExecutorService exec = Executors.newSingleThreadExecutor( r -> {

		Thread t = new Thread(r, "controlled-thread");
		t.setDaemon(true);
		return t;
	});

	@Override
	public void run() {

		try{
			while((action = netQueue.take()).getRequest() != Request.EXIT) {

				webSocketController.sendMessage(new Status("success", "Received action in NN"));

				switch (action.getRequest()) {
				case TRAIN: 
					if(!initialized) initialize(action);
					//					train(action);
					break;

				case VALIDATE: 
					validate(action); 
					break;

				case PREDICT: 
					predict(action); 
					break;

				case STOP: stop(action);
				break;

				case CHECK_GRADIENT:
					break;

				case DOWNLOAD:
					break;

				case EXIT:
					break;

				default:
					break;
				}
			}

		} catch(InterruptedException e) {
			e.printStackTrace(); 
		} finally {

		}	
	}

	private void initialize(Action action) {

		DataUtility data = null;
		
		Double[] time = action.get(uuid, Da); 
		Double[] state = action.get(uuid, name);
		Double[][] input = action.getData(uuid, name)
		
		try {
			data = new DataUtility(timeData, stateData, inputData, outputData)
					.setShuffle(userInput.isShuffle(), userInput.getNetworkSeed())
					.apply();
			
		} catch (Exception e) {
			e.printStackTrace();
		}


		// TODO: update 
		feedForward = new FeedForward()
				.createInputLayer(data.getInputSize())
				.createHiddenLayer(1000, ActivationType.SIGMOID)
				.createOutputLayer(data.getOutputSize(), ActivationType.LINEAR)
				.setCostFunction(CostFunctionType.MAD)
				.setOptimizer(OptimizationAlgorithmType.VANILLA)
				.setVariance(1.0) 
				.setBatch(5)
				.createConnections();

		webSocketController.sendMessage(new Status("success", "Neural Network initialized", "Press the button to start training!"));



		System.out.println("neural network is initializedd");
	}

	//	https://stackoverflow.com/questions/29458676/how-to-avoid-instanceof-when-implementing-factory-design-pattern

	private void train(Action action) {

		trainingTask = new Task() {

			MultiThreadBackpropagation backpropagation = new MultiThreadBackpropagation(feedForward);
			List<Integer> trainSequence = data.getTrainingSequence();

			int epoch = 25; 
			int batchIndex = 0;
			int batch = feedForward.getBatch();

			int id = 1;

			@Override
			public boolean calculate() {

				long startTime = System.nanoTime();

				for(int i=0; i<epoch && super.isRunning(); i++) {

					for(int index : trainSequence) {

						feedForward.feedForward(data.getInputRow(index), data.getOutputRow(index));
						backpropagation.backpropagate();

						backpropagation.update(false);
						batchIndex = batchIndex + 1;

						if(batchIndex == batch) {
							backpropagation.update(true);
							batchIndex = 0;
						}

						if(!super.isRunning()) {
							break; 
						}
					}
					id = id + 1;
				}
				// Stop measuring time	
				long estimatedTime = System.nanoTime() - startTime;
				elapsedTime = TimeUnit.SECONDS.convert(estimatedTime, TimeUnit.NANOSECONDS);

				return true; // tmp
			}

			@Override
			public void onSuccess() {
				webSocketController.sendMessage(new Status("success", "Training finished", 
						"The Neural Network finished training in " + elapsedTime + " seconds"));
			}

			@Override
			public void onStop() {
				webSocketController.sendMessage(new Status("info", "Training stopped"));	
			}

			@Override
			public void onFail() {
				webSocketController.sendMessage(new Status("danger", "Training failed"));
			}
		};
		exec.submit(trainingTask);
	}

	private void validate(Action action) {
		List<Integer> sequence = data.getSequence();
		List<Graph2DValue> validate = new ArrayList<>();
		//        Collections.sort(sequence);

		sequence.forEach( index -> {
			double t = data.getTime()[index];
			System.out.println(" data: " + data.getInputRow(index));
			double k = feedForward.feedForward(data.getInputRow(index)).get(0); 
			System.out.println(t + ", " + k + ", " + data.getOutputRow(index).get(0));
			validate.add(new Graph2DValue(t, k));
		});
		webSocketController.sendMessage(new Graph2DData("output", validate), Queue.VISUALIZE_OUTPUT);
	}

	private void predict(Action action2) {
		// TODO Auto-generated method stub

	}

	private void stop(Action action) {

		if(trainingTask.isRunning() && trainingTask != null) {
			trainingTask.stop();
		}
	}

	// TODO: Fix me
	public void gradientChecking() {

		double epsilon = 0.0001;

		List<Double> originalWeightList = feedForward.getWeights(false);
		List<Double> weightListPlusEpsilon = new ArrayList<>();
		//		List<Double> weightListMinusEpsilon = new ArrayList<>();

		originalWeightList.forEach( weight -> {
			weightListPlusEpsilon.add(weight + epsilon);
			weightListPlusEpsilon.add(weight - epsilon);
		});

		feedForward.setWeights(weightListPlusEpsilon); 
	}

	// TODO: where do I go?


	// TODO: Example
	//	webSocketController.sendMessage(new Graph2DData("desired-output", showOutput), Queue.VISUALIZE_OUTPUT);


	// TODO: Reuse thse



	//
	//
	//
	//	private int counter = 0; 
	//	private int max = 100;

	//	private void notify(int iteration) { 

	//		for(int j=0; j<feedForward.getHiddenLayersList().get(0).getNodeList().size(); j++) {
	//
	//			for(int i=0; i<feedForward.getHiddenLayersList().get(0).getNodeList().get(j).getConnectionList().size(); i++) {
	//				
	//				System.out.println("Grad: " + feedForward.getHiddenLayersList().get(0).getNodeList().get(j).getConnectionList().get(i).getGradient());
	//			}
	//		}
	//		System.out.println("\n *** \n");

	//		feedForward.getWeights().forEach( weight -> {
	//			System.out.println("Grad: " + weight);
	//		});
	//		validate();
	//
	//		double e = Math.round(feedForward.getError() * 10000.0) / 10000.0;
	//
	//		//		System.out.println("Epoch: " + iteration + " e: "  + e);
	//		Graph2DValue error = new Graph2DValue(iteration, e);
	//
	//		values.add(error);
	//		webSocketController.sendMessage(new Graph2DData("approximationError", values)); 
	//		counter = counter + 1;
	//		//		if(counter == max) values.clear();
	//	}


	public void setWebsocketController(WebSocketController webSocketController) {
		this.webSocketController = webSocketController;
	}

	public FeedForward getFeedForward() {
		return feedForward;
	}

	public void setFeedForward(FeedForward feedForward) {
		this.feedForward = feedForward;
	}

	//	public void setFeedForward(FeedForward feedForward) {
	//		this.feedForward = feedForward;
	//	}


}
