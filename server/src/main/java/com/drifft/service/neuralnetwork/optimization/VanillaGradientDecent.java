package com.drifft.service.neuralnetwork.optimization;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VanillaGradientDecent implements Optimizer {
 
	// Learning rate
	private double eta = 0.0005;   
	private boolean update = false;
	
	Map<String, List<Double>> gradients = new HashMap<>();
	
	public VanillaGradientDecent() {
	}
	
	@Override
	public double updateParameter(String id, double weight, double gradient) {	
		
//		if(Math.abs(gradient) > 2.0) gradient = 2.0 * (gradient / Math.abs(gradient)); 
		
		if(!gradients.containsKey(id)) {
			gradients.put(id, new ArrayList<>());
			gradients.get(id).add(gradient);
		} else {
			gradients.get(id).add(gradient);
		}
		
		if(update) {
			double sum = 0.0; 
			
			for(double g : gradients.get(id)) {
				sum = sum + g;
			}
			weight = weight - ( (this.eta / gradients.get(id).size()) * sum);
			gradients.get(id).clear();
		}
		return weight;
	}
	
	public VanillaGradientDecent setLearningRate(Double eta) {
		if(eta != null) this.eta = eta;
		return this;
	}

	@Override
	public void update(boolean update) {
		this.update = update;
	}
}
