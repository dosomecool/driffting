package com.drifft.service.neuralnetwork.network;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.drifft.service.neuralnetwork.activation.Function;
import com.drifft.service.neuralnetwork.costfunction.CostFunction;
import com.drifft.service.neuralnetwork.network.Types.NodeType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Node {
	
	// Attributes
	private String id;
	private int index;
	
	@JsonIgnore
	private double value = 0;
	
	@JsonIgnore
	private double activatedValue = 0;
	
	@JsonIgnore
	private double desiredValue;
	
	@JsonIgnore
	private double error = 0;
	
	// Derivatives
	@JsonIgnore
	private double errorDerivativeWrtOutputNode = 0.0;
	
	@JsonIgnore
	private double activationDerivativeWrtNode  = 0.0;
	
	@JsonIgnore
	private List<Double> nodeDerivativeWrtActivatedNode = new ArrayList<Double>();
	
	@JsonIgnore
	private List<Double> nodeDerivativeWrtWeight = new ArrayList<Double>();
	
	// For hidden and output nodes
	private List<Connection> connectionList = new ArrayList<Connection>();
	private Connection biasConnection;
	private double biasNodeValue = 1.0; 
	
	@JsonIgnore
	private CostFunction costFunction;
	
	public Node() {
	}
	
	public Node(int index) {
		this.index = index;
	}
	
	public Node(String id, int index, List<Connection> connectionList, Connection biasConnection) {
		this.id = id;
		this.index = index;
		this.connectionList = connectionList;
		this.biasConnection = biasConnection;
	}
		
	// For hidden and output nodes	
	public void calculateValue(Layer layer) {
		
		value = 0.0;
		
		for(int i=0; i<connectionList.size(); i++) {

			double nodeValue;

			if (layer.getNodeType() == NodeType.INPUT){
				nodeValue = layer.getNodeList().get(i).getValue();
			} else {
				nodeValue = layer.getNodeList().get(i).getActivatedValue();
			}
			value = value + nodeValue * connectionList.get(i).getWeight();
		}
		value = value + biasNodeValue * biasConnection.getWeight(); 
	}
			
	// ********** DERIVATIVES **********
	
	// For output nodes only
	// dE/dOA
    public void calculateErrorDerivativeWrtOutputNode(double batch) {	
    	errorDerivativeWrtOutputNode = costFunction.getErrorDerivativeWrtActivatedValue(activatedValue, desiredValue) / batch;
	}
    	
    public double getErrorDerivativeWrtOutputNode() {
    	return errorDerivativeWrtOutputNode;
    }
        
    // For Hidden and output nodes
    // dHA_i / dH_i OR dOA_i / dO_i 
	public void calculateActivationDerivativeWrtNode(Function activation) {
		activationDerivativeWrtNode = activation.getFunctionDerivativeValue(value);
	}
	
	public double getActivationDerivativeWrtNode() {
		return activationDerivativeWrtNode;
	}
    
	// For Hidden and output nodes
    // dH/dHA OR dO/dHA
	public void calculateNodeDerivativeWrtActivatedNode() {
		
		// Clear previous derivatives
		nodeDerivativeWrtActivatedNode.clear();
		
		connectionList.forEach( connection -> {
			nodeDerivativeWrtActivatedNode.add(connection.getWeight());
		});
		
		// Bias derivative not required since they  
		// are not part of the derivative chain
	}
	
	public List<Double> getNodeDerivativeWrtActivatedNode() {
		return nodeDerivativeWrtActivatedNode;
	}
	
	// For Hidden nodes only
    // dH/dW
	public void calculateNodeDerivativeWrtWeight(Layer previousLayer) {
		
		nodeDerivativeWrtWeight.clear();
		
		// For input nodes
		if(previousLayer.getNodeType() == NodeType.INPUT) {

			previousLayer.getNodeList().forEach( inputNode -> {
				nodeDerivativeWrtWeight.add(inputNode.getValue());
			});
			
		// For hidden nodes
		} else {
			previousLayer.getNodeList().forEach( hiddenNode -> {
				nodeDerivativeWrtWeight.add(hiddenNode.getActivatedValue());
			});
		}
		
		// For bias nodes
		nodeDerivativeWrtWeight.add(biasNodeValue);
	}
	
	public List<Double> getNodeDerivativeWrtWeight() {
		return nodeDerivativeWrtWeight;
	}
	
	public void setCostFunction(CostFunction costFunction) {
		this.costFunction = costFunction; 
	}
	
	// Returns value of the node
	public double getValue() {
		return value;
	}
	
	// For input nodes only
	public void setValue(double value) {
		this.value = value;
	}

	// Returns activated value of the node
	public double getActivatedValue() {
		return activatedValue;
	}
	
	public void setActivatedValue(double activatedValue){
		this.activatedValue = activatedValue;
	}
	
	// Only for output nodes
	public void setDesiredValue(double desiredValue) {
		this.desiredValue = desiredValue;
	}
	
	// For output nodes only
	public double getError() {
		return error;
	}
	
	// For output nodes only
	public void calculateError() {
		error = costFunction.getErrorValue(activatedValue, desiredValue);
	}
		
	public int getIndex() {
		return index;
	}
	
	public void setIndex(int index) {
		this.index = index;
	}
	
	public String getId() {
		return id;
	}
	
	public String setId() {
		return id;
	}
	
	public List<Connection> getConnectionList() {
		return connectionList;
	}
	
	public void setConnectionList(List<Connection> connectionList) {
		this.connectionList = connectionList;
	}

	public Connection getBiasConnection() {
		return biasConnection;
	}

	public void setBiasConnection(Connection biasConnection) {
		this.biasConnection = biasConnection;
	}
	
	@Override 
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("id: ").append(id);
		sb.append("\nindex: ").append(index);
		sb.append("\nconnectionList: ").append(connectionList);
		sb.append("\nbiasConnection: ").append(biasConnection);
		
		String jsonStr = null; 
		ObjectMapper mapper = new ObjectMapper();
		
        try {
            jsonStr = mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
		return jsonStr;
	}
}
