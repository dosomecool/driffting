package com.drifft.service.neuralnetwork.target;

import com.drifft.service.neuralnetwork.network.Types.TargetFunctionType;

public class TargetFunctionFactory {

	public TargetFunction get(TargetFunctionType type) {
		
		if(type == null) {
			return null;
		}
		
		if(type.equals(TargetFunctionType.LINE)) {
			return new Line();
		} else if (type.equals(TargetFunctionType.QUADRATIC)) {
			return new Quadratic();
		}
		return null;
	}
	
}
