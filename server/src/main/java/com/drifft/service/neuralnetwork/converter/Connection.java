package com.drifft.service.neuralnetwork.converter;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Connection {

	private int id;
	private double weight;
	
	public Connection() {
	}
	
	public Connection(int id, int weight) {
		this.id = id;
		this.weight = weight;
	}
	
	@Override 
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("id: ").append(id);
		sb.append("\nweight: ").append(weight);
		
		String jsonStr = null; 
		ObjectMapper mapper = new ObjectMapper();
		
        try {
            jsonStr = mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
		return jsonStr;
	}
}
