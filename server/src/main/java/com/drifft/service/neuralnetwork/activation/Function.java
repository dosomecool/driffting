package com.drifft.service.neuralnetwork.activation;

import com.drifft.service.neuralnetwork.network.Types.ActivationType;

public interface Function {

	public double getFunctionValue(double x);
	public double getFunctionDerivativeValue(double x);
	public ActivationType getActivation();
}
