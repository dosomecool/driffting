package com.drifft.service.neuralnetwork.activation;

import com.drifft.service.neuralnetwork.network.Types.ActivationType;

public class Tanh implements Function {

	// http://mochajl.readthedocs.io/en/latest/user-guide/neuron.html
	
	public Tanh() {
	}
	
	@Override
	public double getFunctionValue(double x) {
		return ( 2 / (1 + Math.exp(-2 * x)) ) - 1;
	}

	@Override
	public double getFunctionDerivativeValue(double x) {
		return ( 1 - Math.pow(getFunctionValue(x), 2) );	// (4 * Math.exp(2*x)) / Math.pow((Math.exp(2*x) + 1), 2)
	}

	@Override
	public ActivationType getActivation() {
		return ActivationType.TANH;
	}
}
