package com.drifft.service.neuralnetwork.backpropagation;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.drifft.service.neuralnetwork.network.FeedForward;
import com.drifft.service.neuralnetwork.network.Layer;
import com.drifft.service.neuralnetwork.network.Types.OptimizationAlgorithmType;
import com.drifft.service.neuralnetwork.optimization.OptimizationAlgorithmFactory;
import com.drifft.service.neuralnetwork.optimization.Optimizer;

public class MultiThreadBackpropagation {

	private double batch = 1.0;
	
	// Layers
	public Layer inputLayer;
	public ArrayList<Layer> hiddenLayersList = new ArrayList<Layer>();
	public Layer outputLayer;

	// Optimization algorithm
	public Optimizer optimizer;

	public MultiThreadBackpropagation(FeedForward feedForward) {
		this.batch = feedForward.getBatch();
		this.inputLayer = feedForward.getInputLayer();
		this.hiddenLayersList = feedForward.getHiddenLayersList();
		this.outputLayer = feedForward.getOutputLayer();
		this.optimizer = feedForward.getOptimizer();
	}

	// This method calculates all the individual partial differential equations
	public MultiThreadBackpropagation calculatePartialDerivatives() {

		// Hidden layer 
		for (int m=0; m<hiddenLayersList.size(); m++) {

			for(int n=0; n<hiddenLayersList.get(m).getNodeList().size(); n++) {

				// dHA/dH
				hiddenLayersList.get(m).getNodeList().get(n).calculateActivationDerivativeWrtNode(hiddenLayersList.get(m).getActivation());
				//dH/DHA
				hiddenLayersList.get(m).getNodeList().get(n).calculateNodeDerivativeWrtActivatedNode();	
				// dH/dW - Pass previous layer
				if (m==0){
					hiddenLayersList.get(m).getNodeList().get(n).calculateNodeDerivativeWrtWeight(inputLayer);
				} else {
					hiddenLayersList.get(m).getNodeList().get(n).calculateNodeDerivativeWrtWeight(hiddenLayersList.get(m-1));
				}
			}
		}	

		// Output layer
		for (int k=0; k<outputLayer.getNodeList().size(); k++) {

			// de/dOA
			outputLayer.getNodeList().get(k).calculateErrorDerivativeWrtOutputNode(batch); // removed: outputLayer.getNodeList().size()
			// dOA/dO
			outputLayer.getNodeList().get(k).calculateActivationDerivativeWrtNode(outputLayer.getActivation());
			// dO/dHA
			outputLayer.getNodeList().get(k).calculateNodeDerivativeWrtActivatedNode();
			// dO/dW
			outputLayer.getNodeList().get(k).calculateNodeDerivativeWrtWeight(hiddenLayersList.get(hiddenLayersList.size()-1)); // Last hidden layer
		}
		return this;
	}

	// Calculate input layer connection gradients
	public MultiThreadBackpropagation calculateWeightGradients() {

//		ExecutorService executorService = Executors.newFixedThreadPool(10);

		ExecutorService executorService = Executors.newFixedThreadPool(10, r -> {
			Thread t = new Thread(r, "backpropagation-thread");
			t.setDaemon(true);
			return t;
		});
		
		List<Callable<Object>> gradientCalculations = new ArrayList<>();

		// Hidden layers
		for(int k=0; k<hiddenLayersList.size(); k++){

			// Nodes
			for(int j=0; j<hiddenLayersList.get(k).getNodeList().size(); j++){

				final int layer = k;
				final int node = j;

				gradientCalculations.add(Executors.callable(new Runnable() {

					@Override
					public void run() {

						// Connections
						for(int i=0; i<hiddenLayersList.get(layer).getNodeList().get(node).getConnectionList().size(); i++) {

							// Passing values: { layer, node, connection, isOutput}
							double gradientValue = getGradient(layer, node, i, false);
							hiddenLayersList.get(layer).getNodeList().get(node).getConnectionList().get(i).setGradient( gradientValue );
						}
					}
				}));

				gradientCalculations.add(Executors.callable(new Runnable() {

					@Override
					public void run() {

						// Bias
						int biasConnection = hiddenLayersList.get(layer).getNodeList().get(node).getConnectionList().size();
						// Call gradient for bias weight
						hiddenLayersList.get(layer).getNodeList().get(node).getBiasConnection().setGradient( getGradient(layer, node, biasConnection, false) );

					}	
				}));
			}
		}

		// Output Nodes
		for(int j=0; j<outputLayer.getNodeList().size(); j++) {

			final int node = j;

			gradientCalculations.add(Executors.callable(new Runnable() {

				@Override
				public void run() {

					// Connections
					for(int i=0; i<outputLayer.getNodeList().get(node).getConnectionList().size(); i++){

						// Call get gradient
						outputLayer.getNodeList().get(node).getConnectionList().get(i).setGradient(getGradient(0, node, i, true));
					}
				}
			}));

			gradientCalculations.add(Executors.callable(new Runnable() {

				@Override
				public void run() {

					int biasConnection = outputLayer.getNodeList().get(node).getConnectionList().size();
					// Call gradient for bias weight
					outputLayer.getNodeList().get(node).getBiasConnection().setGradient( getGradient(0, node, biasConnection, true) );
				}
			}));
		}
		
		try {
			executorService.invokeAll(gradientCalculations);
		} catch (InterruptedException e) {
			System.out.println("\n Some bad happened!!!!! \n");
			e.printStackTrace();
		} finally {
			executorService.shutdown();
		}
		return this;
	}

	@SuppressWarnings("null")
	private double getGradient(int layer, int node, int connection, boolean isOutput){

		double product = 1.0;

		// Intermediate hidden layers - including first hidden layer go here
		if (layer < hiddenLayersList.size()-1 && isOutput == false) {

			// dHA/dH
			product = product * hiddenLayersList.get(layer).getNodeList().get(node).getActivationDerivativeWrtNode();
			// dH/dW
			product = product * hiddenLayersList.get(layer).getNodeList().get(node).getNodeDerivativeWrtWeight().get(connection); 
			// Get sum of derivatives from next layer
			product = product * getIntermidiateHiddenLayerDerivativeSumWrtNode(layer, node); // pass current layer

			return product;

			// Last hidden layer
		} else if (layer == hiddenLayersList.size()-1 && isOutput == false) {

			// dHA/dH
			product = product * hiddenLayersList.get(layer).getNodeList().get(node).getActivationDerivativeWrtNode();
			// dH/dW
			product = product * hiddenLayersList.get(layer).getNodeList().get(node).getNodeDerivativeWrtWeight().get(connection); 
			// Get sum of derivatives from output layer
			product = product * getOutputLayerDerivativeSumWrtNode(node);

			return product;

			// Output layer
		} else if ( isOutput == true ) { 

			// de/dOA
			product = product * outputLayer.getNodeList().get(node).getErrorDerivativeWrtOutputNode();
			// dOA/dO
			product = product * outputLayer.getNodeList().get(node).getActivationDerivativeWrtNode();
			// dO/dHA
			product = product * outputLayer.getNodeList().get(node).getNodeDerivativeWrtWeight().get(connection);

			return product;
		}
		return (Double) null;
	}

	private double getIntermidiateHiddenLayerDerivativeSumWrtNode(int layer, int node) {

		double sum = 0;

		// Hidden Nodes
		for(int j=0; j<hiddenLayersList.get(layer+1).getNodeList().size(); j++) {

			double product = 1.0;

			// Not last hidden layer
			if( layer == (hiddenLayersList.size()-2) ) { 

				// dH/dHA
				product = product * hiddenLayersList.get(layer+1).getNodeList().get(j).getNodeDerivativeWrtActivatedNode().get(node); 
				// dHA/dH
				product = product * hiddenLayersList.get(layer+1).getNodeList().get(j).getActivationDerivativeWrtNode();

				product = product * getOutputLayerDerivativeSumWrtNode(j);

				sum = sum + product;

			} else { 

				// dH/dHA
				product = product * hiddenLayersList.get(layer+1).getNodeList().get(j).getNodeDerivativeWrtActivatedNode().get(node); 
				// dHA/dH
				product = product * hiddenLayersList.get(layer+1).getNodeList().get(j).getActivationDerivativeWrtNode();
				// Call "this" method again
				product = product * getIntermidiateHiddenLayerDerivativeSumWrtNode(layer+1, j);

				sum = sum + product;
			} 
		}
		return sum;
	}

	// This returns the total derivative of the output layer w.r.t to a specific node in the last hidden layer
	private double getOutputLayerDerivativeSumWrtNode(int node) {

		double sum = 0;

		// Node
		for(int m=0; m<outputLayer.getNodeList().size(); m++) {

			double product = 1.0;

			// de/dOA
			product = product * outputLayer.getNodeList().get(m).getErrorDerivativeWrtOutputNode();
			// dOA/dO
			product = product * outputLayer.getNodeList().get(m).getActivationDerivativeWrtNode();
			// dO/dHA
			product = product * outputLayer.getNodeList().get(m).getNodeDerivativeWrtActivatedNode().get(node); // Node from previous layer

			sum = sum + product;
		}
		return sum;
	}

	// Update connections to hidden layer
	public MultiThreadBackpropagation updateHiddenLayerConnectionsWeights() {

		// Hidden layers
		for(int k=0; k<hiddenLayersList.size() ;k++) {

			// Nodes
			for(int j=0; j<hiddenLayersList.get(k).getNodeList().size(); j++) {

				// Connections
				for(int i=0; i<hiddenLayersList.get(k).getNodeList().get(j).getConnectionList().size(); i++) {

					String id       = hiddenLayersList.get(k).getNodeList().get(j).getConnectionList().get(i).getId();
					double weight   = hiddenLayersList.get(k).getNodeList().get(j).getConnectionList().get(i).getWeight();
					double gradient = hiddenLayersList.get(k).getNodeList().get(j).getConnectionList().get(i).getGradient();

					hiddenLayersList.get(k).getNodeList().get(j).getConnectionList().get(i).setWeight( optimizer.updateParameter(id, weight, gradient) );
				}

				String id       = hiddenLayersList.get(k).getNodeList().get(j).getBiasConnection().getId();
				double weight   = hiddenLayersList.get(k).getNodeList().get(j).getBiasConnection().getWeight();
				double gradient = hiddenLayersList.get(k).getNodeList().get(j).getBiasConnection().getGradient();

				hiddenLayersList.get(k).getNodeList().get(j).getBiasConnection().setWeight( optimizer.updateParameter(id, weight, gradient) );
			}
		}
		return this;
	}

	// Update connections to output layer
	public MultiThreadBackpropagation updateOutputLayerConnectionsWeights() {

		// Nodes
		for(int j=0; j<outputLayer.getNodeList().size(); j++){

			// Connections
			for(int i=0; i<outputLayer.getNodeList().get(j).getConnectionList().size(); i++) {

				String id       = outputLayer.getNodeList().get(j).getConnectionList().get(i).getId();
				double weight   = outputLayer.getNodeList().get(j).getConnectionList().get(i).getWeight();
				double gradient = outputLayer.getNodeList().get(j).getConnectionList().get(i).getGradient();

				outputLayer.getNodeList().get(j).getConnectionList().get(i).setWeight( optimizer.updateParameter(id, weight, gradient) );
			}

			String id       = outputLayer.getNodeList().get(j).getBiasConnection().getId();
			double weight   = outputLayer.getNodeList().get(j).getBiasConnection().getWeight();
			double gradient = outputLayer.getNodeList().get(j).getBiasConnection().getGradient();

			outputLayer.getNodeList().get(j).getBiasConnection().setWeight( optimizer.updateParameter(id, weight, gradient) );
		}
		return this;
	}

	public void backpropagate() {
		calculatePartialDerivatives()
		.calculateWeightGradients()
		.updateHiddenLayerConnectionsWeights()
		.updateOutputLayerConnectionsWeights();
	}

	public void update(boolean update) {
		optimizer.update(update);
	}
}
