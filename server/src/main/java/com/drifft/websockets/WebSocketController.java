package com.drifft.websockets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import com.drifft.constant.Constants.Queue;
import com.drifft.domain.Graph2DData;
import com.drifft.domain.Graph3DData;
import com.drifft.domain.Message;
import com.drifft.domain.Status;
import com.drifft.service.neuralnetwork.visualization.NetworkNodesAndEdges;

@Controller
public class WebSocketController {
	
	@Autowired
    private final SimpMessagingTemplate template;

    @Autowired
    WebSocketController(SimpMessagingTemplate template) {
        this.template = template;
    }
           
    @MessageMapping("/inbox")
    public void onMessage(MessageHeaders messageHeaders) throws Exception {
        	
//    	this.template.convertAndSend("/notification/message", new Car("red", "testarossa"));
    }
    
	@MessageExceptionHandler
    @SendTo("/notification/errors")
    public String handleException(Throwable exception) {
		exception.getStackTrace();
        return exception.getMessage(); 
    }
	
    public void sendMessage(String type, Object data) {
    	Message message = new Message(type, data);
    	template.convertAndSend("/notification/image", message);
    }
    
    public void sendMessage(NetworkNodesAndEdges networkNodesAndEdges) {
    	template.convertAndSend("/notification/visualize/network", networkNodesAndEdges);
    }

	public void sendMessage(Graph2DData graph2DData) {
		template.convertAndSend("/notification/visualize/error", graph2DData);
	}
	
	public void sendMessage(Graph2DData graph2DData, Queue queue) {
		String destination = null;

		switch(queue) {
		case VISUALIZE_OUTPUT: destination = Queue.VISUALIZE_OUTPUT.queue();
		break;
		case VISUALIZE_ERROR: destination = Queue.VISUALIZE_ERROR.queue();
		break;
		}
		template.convertAndSend(destination, graph2DData);
	}
	
//	public void sendMessage(List<Graph2DData> graph2DDataList) {
//		template.convertAndSend("/notification/visualize/output", graph2DDataList);
//	}
	
	public synchronized void sendMessage(Status status) {
		template.convertAndSend("/notification/server/status", status);
	}
	
	public void sendMessage(Graph3DData graph3DData) {
		template.convertAndSend("/notification/visualize/3d-input", graph3DData);
	}
}