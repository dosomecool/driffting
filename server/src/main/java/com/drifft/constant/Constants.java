package com.drifft.constant;

public class Constants {
	
	public static final String FXML_RESOURCE_PATH = "/fxml/";
	public static final String CSS_RESOURCE_PATH  = "/css/";
	public static final String PLOT_RESOURCE_PATH  = "/plots/";
	
	public static final String ACCESS_KEY_ID = "";
	public static final String SECRET_ACESS_KEY = "";
	
	public static final String PRIMARY_BUCKET = "drifftspace.com";
	public static final String DATA_FOLDER = "data/";
	
	// CURRENT FUNCTIONALITY
	public static enum Functionality {
		
		RANDOM, 
		
		TIME, 
		
		KAPLAIN_MEIER,
		
		DIFFUSION,

		KILLING,
		
		SURVIVAL,
		
		DENSITY, 
		
		HAZARD, 
		 
		SURVIVAL_PDE, 
		
		CONDITIONED_DIFFUSION,
	}
	
	public static enum Request {
		
		INITIALIZE("initialize"),
		
		GENERATE("generate"),
		
		TRAIN("train"),
		
		VALIDATE("validate"), 
		
		PREDICT("predict"), 
		
		CHECK_GRADIENT("check-gradient"), 
		
		DOWNLOAD("download"), 
		
		STOP("stop"),
		
		EXIT("exit");
		
	    private String request;

		Request (String request) {
	        this.request = request;
	    }

	    public String request() {
	        return request;
	    }
	}
	
	public static enum LocalService {
		
		NEURAL_NETWORK, PLOTTER
	}
	
	public static enum Queue {
		
		VISUALIZE_OUTPUT("/notification/visualize/output"),
		
		VISUALIZE_ERROR("/notification/visualize/error");
		
	    private String queue;

		Queue (String queue) {
	        this.queue = queue;
	    }

	    public String queue() {
	        return queue;
	    }
	}
	
	public static enum Data {
		
		TIME("time"),
		
		STATE("state"),
		
		WIENER("wiener"),
		
		DIFFUSION("diffusion"),
		
		KILLING("killing"),
		
		SURVIVAL("survival"),	
		
		DIFFUSION_HISTOGRAM("diffusionHistogram");
		
	    private String name;

		Data (String name) {
	        this.name = name;
	    }

	    public String getName() {
	        return name;
	    }
	}
	
	public static enum Service {
		
		NEURAL_NETWORK("neuralNetwork"),
		
		SIMULATOR("simulator");
		
	    private String service;

	    Service (String service) {
	        this.service = service;
	    }

	    public String getServiceType() {
	        return service;
	    }
	}
}
