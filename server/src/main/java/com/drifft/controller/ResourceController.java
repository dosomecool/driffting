package com.drifft.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.drifft.domain.UserInput;
import com.drifft.domain.validation.Validator;
import com.drifft.service.ServiceManager;
import com.drifft.service.UserRequest;

// Annotations
// http://websystique.com/spring-boot/spring-boot-rest-api-example/

@CrossOrigin(origins = { "http://localhost:3000", 
		                 "http://192.168.1.15:3000", 
		                 "http://127.0.0.1:3000", 
		                 "http://drifftspace.com.s3-website.us-east-2.amazonaws.com", 
		                 "http://drifftspace.com.s3-website.us-east-2.amazonaws.com:80" }, maxAge = 3000)
@RestController
public class ResourceController {
 
	@Autowired
	private ServiceManager manager;  
	
    @RequestMapping(value = "/{num1}/{num2}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String add(@PathVariable int num1, @PathVariable int num2) {
        return Integer.toString( num1 + num2 );
    }
    
    @PostMapping("/user-input")
    public ResponseEntity<String> submitRequest(@RequestBody UserInput input) {
    	   	    	
    	Validator validator = new Validator(input).check();
    	
    	if(!validator.isFailed()) manager.setInput(input).process();
    	
		return new ResponseEntity<>(validator.getErrors(), HttpStatus.OK);
    }
    
    @PostMapping("/user-action")
    public ResponseEntity<String> actionRequest(@RequestBody UserRequest action) {
    	
//    	System.out.println("user has requested: " + action.getAction());
//    	
//    	ResponseEntity response = null;
//    	
//    	if(action.getAction().equals("download")) {
//    		System.out.println("***\n");
//    		System.out.println(manager.getModel().toString());
//    		System.out.println("\n***");
//    		response = new ResponseEntity<>(manager.getModel(), HttpStatus.OK);
//    	}
    	
    	
		return new ResponseEntity<>("test", HttpStatus.OK);
    }
}


