package com.drifft.service.neuralnetwork.utilities;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

// source: https://gerardnico.com/code/design_pattern/typesafe_heterogeneous_container 

public class DataSet {

    private Map<Data<?>, Object> set = new HashMap<Data<?>, Object>();
 
    public <T> void putData(Data<T> type, T instance) {
    	
        if (type == null) throw new NullPointerException("Type is null");
        set.put(type, instance.getClass().cast(instance));
    }
 
    public <T> T getData(Data<T> type) {
        return type.cast(set.get(type));
    }
    
    public Set<Data<?>> getKeys() {
    	return set.keySet();
    }
}
