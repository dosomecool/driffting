package com.drifft.service.neuralnetwork.utilities;

// source: https://gerardnico.com/code/design_pattern/typesafe_heterogeneous_container

public class Data<T> {

	private final Class<T> type;
	
	public Data(Class<T> type) {
		this.type = type;
	}
	
	public T cast(Object object) {
		return object == null ? null : type.cast(object);
	}
}
